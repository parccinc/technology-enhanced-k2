CKEDITOR.plugins.add( 'glossaryTerm',
{
    init: function( editor )
    {
        var iconPath = this.path + 'images/dictionary.png';

        editor.addContentsCss( this.path + 'styles/glossaryTerm.css' );

        editor.addCommand( 'setGlosseryTerm',
            {
                exec : function( editor )
                {
                    if (editor.glossaryTerms
                            && editor.getSelection().getSelectedText()
                            && editor.glossaryTerms.map(function(e) { return e.term.toLowerCase(); }).indexOf(editor.getSelection().getSelectedText().toLowerCase()) === -1) {
                        alert("The text you have selected does not exist in the glossary.");
                    } else {
                        var style = new CKEDITOR.style( { element: 'span', attributes: { 'class': 'glossaryTerm'} } );

                        editor.applyStyle( style );
                    }
                }
            });

        editor.addCommand( 'removeGlosseryTerm',
            {
                exec : function( editor )
                {
                    var style = new CKEDITOR.style( { element: 'span', attributes: { 'class': 'glossaryTerm'} } );

                    editor.removeStyle( style );
                }
            });

        editor.ui.addButton( 'GlossaryTerm',
            {
                label: 'Set Glossary Term',
                command: 'setGlosseryTerm',
                icon: iconPath
            } );


        if ( editor.contextMenu )
        {
            editor.addMenuGroup( 'glossary' );
            editor.addMenuItem( 'abbrTermItem',
                {
                    label : 'Remove set as glossary term',
                    icon : iconPath,
                    command : 'removeGlosseryTerm',
                    group : 'glossary'
                });
            editor.contextMenu.addListener( function( element )
            {
                if ( element )
                    element = element.getAscendant( 'span', true );
                if ( element && !element.isReadOnly() && !element.data( 'cke-realelement' ) )
                    return { abbrTermItem : CKEDITOR.TRISTATE_OFF };
                return null;
            });
        }
    }
});