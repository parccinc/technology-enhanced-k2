Copyright © <2015> <Parcc, Inc.>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Meteor Starter Kit (SERAPH GROUP EDITION)

## Packages Included

* [accounts-base](http://docs.meteor.com/#accountsui)
* [accounts-password](http://docs.meteor.com/#accountsui)
* [iron-router](https://atmospherejs.com/package/iron-router)
* [less](http://docs.meteor.com/#less)
* [email](http://docs.meteor.com/#email)
* [spiderable](http://docs.meteor.com/#spiderable)
* [font-awesome](https://atmospherejs.com/package/font-awesome)
* [bootstrap3-less](https://atmospherejs.com/package/bootstrap3-less)
* [auto-nprogress](https://atmospherejs.com/package/auto-nprogress)
* [accounts-entry](https://atmospherejs.com/package/accounts-entry)
* [underscore](http://docs.meteor.com/#underscore)
* [jquery](http://docs.meteor.com/#jquery)
* [Bootstrap-Validator](https://atmospherejs.com/package/Bootstrap-Validator)


## File Structure
```
.meteor/ (You can guess what this is for)

client/
  accounts/ (Accounts package configuration)
    accounts.js
  compatibility/ (Extra libraries)
    animate.css
    jasny-bootstrap.min.css
    jasny-bootstrap.min.js
  styles/ (Global Styles)
    mixins.import.less
    style.less
    variables.import.less
  views/ (All template-specific html, less, and javascript files)
    contact/
      contact.html
      contact.js
    home/
      home.html
    includes/ (Reusable templates for overall structure)
      footer.html
      header.html
      header.js
      loading.html
      notFound.html
    base.html
    base.import.less
  subscriptions.js (Define what data you are going to subscribe to from the server)

collections/ (Model Definitions)

packages/ (Directory for installed packages)

public/
  favicon.ico (Browser Favicon)

server/
  views/ (Server-side functionality for templates on the client)
    contact/
      contactForm.js (Email submission and validation)
  publications.js (Define what data you going to publish to the client)

routes.js (Iron-router routes)
```

## Functionality & Components Included
* Basic User Accounts
* Contact Form (with [validation](http://bootstrapvalidator.com/))
* Bootstrap LESS along with Jasny Bootstrap extension
* File structure and LESS structure to work off
* Basic Fixed Navigation Menu
* Mobile Navigation Menu Fix for Bootstrap (when changing routes)
* Small test route (/profile) so you can see how logged-in only pages work
* [CSS Animations](http://daneden.github.io/animate.css/) which can come in handy for [customized page transitions](http://www.manuel-schoebel.com/blog/simple-page-transitions-with-iron-router-hooks)


## LESS Structure
The way I have this structured is that all LESS files get imported into the ```style.less``` file.
So, all of the files are named ```filename.import.less``` and then imported in the order it should go in.
This way, you are more organized with the order and usage of your LESS styles.


## Installation

You should have [Meteor](http://meteor.com) and [Meteorite](https://atmospherejs.com/docs/installing) installed then follow these steps:

Note: Only works on *nix or Mac machines.

1. Clone the repository locally
2. run ```mrt```
