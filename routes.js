//cache subscriptions
var subs = new SubsManager();

// Hard coded admin users
var adminUsernames = ["teacherk@yahoo.com",
    "teacherk@k.com",
    "teacherg1@yahoo.com",
    "teacherg2@yahoo.com",
    "edccm@yahoo.com",
    "centercm@yahoo.com",
    "parcccm@yahoo.com",
    "k2contentadmin1@yahoo.com",
    "k2contentadmin2@yahoo.com"];

// Map the routes
Router.map(function() {
    // Homepage
    this.route('home', {
        path: '/',
        onAfterAction: function () {
            Session.set("selected_group", null);
            Session.set('group_list_ordered', null);
        }
    });

    // Dashboard
    this.route('dashboard', {
       onAfterAction: function () {
            Session.set("selected_subject", "ela");
            Session.set('display_help', false);

            if (Meteor.user()
                && Meteor.user().profile
                && Meteor.user().profile.groups
                && Meteor.user().profile.groups.length > 0
                && Session.get("selected_group") == null) {

                Session.set("selected_group", Meteor.user().profile.groups[0]);
                Session.set("selected_grade", Meteor.user().profile.groups[0].grade);

            } else if (Meteor.user()
                && Meteor.user().profile
                && Meteor.user().profile.classrooms
                && Meteor.user().profile.classrooms.length > 0
                && Session.get("selected_group") == null) {

                Session.set("selected_group", Meteor.user().profile.classrooms[0]);
                Session.set("selected_grade", Meteor.user().profile.classrooms[0].grade);

            } else if (Session.get("selected_group") == null) {
                Session.set('display_help', true);
            }
        },
        waitOn: function () {
            if (Session !== undefined && Session.get('selected_grade')) {
                return [subs.subscribe('performanceTasksByGrade', Session.get('selected_grade')),
                        subs.subscribe('commonCoreStandardsByGrade', Session.get('selected_grade'))];
            }
        }
    });

    // Roster Classroom Admin
    this.route('classroom', {
        waitOn: function () {
            if (Session.get('selected_classroom')) {
                return subs.subscribe('classroomStudents', Session.get("selected_classroom")._id);
            }
        }
    });

    // Roster Grouping Admin
    this.route('groups', {
        waitOn: function () {
            if (Meteor.user()) {
                //return subs.subscribe('usersStudents');
                return [subs.subscribe('reactiveUsersStudents')];
            }
        }
    });

    // User Journal
    this.route('journal', {
        onAfterAction: function () {
        },
        waitOn: function () {
            if (Session.get('selected_grade')) {
                return [subs.subscribe('performanceTasksByGrade', Session.get('selected_grade'))];
            }
        }
    });

    // Standards Admin
    this.route('standards', {
        onAfterAction: function () {
            Session.set("selected_standard", null);
        },
        onBeforeAction: function(){
            if(adminUsernames.indexOf(Meteor.user().emails[0].address) != -1 || Roles.userIsInRole(Meteor.user(), "Content Administrator (Curator)")){
                this.next();
            } else {
                this.stop();
            }
        },
        waitOn: function () {
            return [
                subs.subscribe('commonCoreStandards'),
                subs.subscribe('assessments')
            ]
        }
    });

    // Assessment Admin
    this.route('assessment', {
        onAfterAction: function () {
            Session.set("selected_admin_assessment", null);
        },
        onBeforeAction: function(){
            if(adminUsernames.indexOf(Meteor.user().emails[0].address) != -1 || Roles.userIsInRole(Meteor.user(), "Content Administrator (Curator)")){
                this.next();
            } else {
                this.stop();
            }
        },
        waitOn: function () {
            return [
                subs.subscribe('commonCoreStandards'),
                subs.subscribe('assessments')
            ]
        }
    });

    // Performance Tasks Admin
    this.route('performancetask', {
        onAfterAction: function () {
            Session.set("selected_admin_performance_task", null);
            Session.set("selected_performancetask", null);
        },
        onBeforeAction: function(){
            if(adminUsernames.indexOf(Meteor.user().emails[0].address) != -1 || Roles.userIsInRole(Meteor.user(), "Content Administrator (Curator)")){
                this.next();
            } else {
                this.stop();
            }
        },
        waitOn: function () {
            return [
                subs.subscribe('glossary'),
                subs.subscribe('performanceTasks'),
                subs.subscribe('commonCoreStandards'),
                subs.subscribe('assessments'),
                subs.subscribe('currentUserExtendedData')
            ]
        }
    });

    // Glossary Admin
    this.route('glossary', {
        onAfterAction: function () {
            Session.set("selected_glossary_term", null);
        },
        onBeforeAction: function(){
            if(adminUsernames.indexOf(Meteor.user().emails[0].address) != -1 || Roles.userIsInRole(Meteor.user(), "Content Administrator (Curator)")){
                this.next();
            } else {
                this.stop();
            }
        },
        waitOn: function () {
            return subs.subscribe('glossary');
        }
    });

    // Student Performance Task Assessment
    this.route('studentassessment', {
        path: '/studentassessment/:id/:subject/:pt',
        template: 'studentassessment',

        action: function () {
            // Workaround for issue with annotations causing page to reload when add/edit them
            if (Session.get("selected_performancetask") === null
                ||  (Session.get("selected_performancetask") !== null && Session.get("selected_performancetask")._id !== this.params.id)) {
                Session.set("selected_performancetask", null);
                Session.set("selected_day", null);
                Session.set("selected_assessment", null);
            }

            this.render('studentassessment');
        },
        onAfterAction: function () {
            Session.set('selected_students', null);
            Session.set('group_list_ordered', null);

            // Workaround for issue with annotations causing page to reload when add/edit them
            if (Session.get("selected_performancetask") === null
                ||  (Session.get("selected_performancetask") !== null && Session.get("selected_performancetask")._id !== this.params.id)) {
                Meteor.call('getPerformanceTask', this.params.id, function(err, task) {

                    if (task.subject === 'math') {
                        Session.set('main_menu_item_selected', { _id: "overview",
                            name: "Overview",
                            type: "Instruction",
                            content: task.instructions.overview});
                    } else {
                        Session.set('main_menu_item_selected', { _id: "plan",
                            name: "Plan",
                            type: "Instruction",
                            content: task.instructions.plan});
                    }

                    Session.set("selected_performancetask", task);

                    if (task !== null && task.assessmentSchedule.length > 0) {
                        Session.set("selected_day", task.assessmentSchedule[0]);

                        if (task.subject === 'math' && task.assessmentSchedule[0].assessments.length > 0) {
                            Session.set('selected_assessment', task.assessmentSchedule[0].assessments[0]);
                        } else {
                            Session.set('selected_assessment', null);
                        }
                    }
                });
            }
        },
        waitOn: function () {
            if (Session.get('selected_group')) {
                return [subs.subscribe('commonCoreStandardsByGrade', Session.get('selected_grade')),
                        subs.subscribe('assessmentsByGrade', Session.get('selected_grade')),
                        subs.subscribe('glossary'),
                        subs.subscribe('reactiveStudentsMedia', Session.get('selected_group')._id)];
            } else {
                return subs.subscribe('glossary');
            }
    }
    });

    this.route('uploadFile', {
        path: '/fileupload',
        action: function() {
            console.dir(this.params); //Contains params
            console.dir(this.request);
            console.dir(this.response);
        }
    });


    this.route('invalidUser', {
        path: '/invalidUser'
    });

    // Student Details Page
    this.route('studentDetails', {
        path: '/studentDetails',
        waitOn: function () {
            if (Session.get('selected_group')) {
                return [
                    subs.subscribe('performanceTasksByGrade', Session.get('selected_grade')),
                    subs.subscribe('reactiveStudentsMedia', Session.get('selected_group')._id),
                    subs.subscribe('assessmentsByGrade', Session.get('selected_grade')),
                    subs.subscribe('commonCoreStandardsByGrade', Session.get('selected_grade'))
                ]
            }
        },
        onAfterAction: function () {
            Session.set("selected_subject", null);
            Session.set("selected_performance_task", null);
            Session.set("selected_standard", null);
            Session.set("selected_student", null);
            Session.set('details_page', null);
            Session.set('details_num_pages', null);
        }
    });

    // Class Details Page
    this.route('classDetails', {
        path: '/classDetails',
        waitOn: function () {
            if (Session.get('selected_group')) {
                return [
                    subs.subscribe('performanceTasksByGradeNames', Session.get('selected_grade')),
                    subs.subscribe('reactiveStudentsMedia', Session.get('selected_group')._id),
                    subs.subscribe('assessmentsByGrade', Session.get('selected_grade'))
                ]
            }
        },
        onAfterAction: function () {
            Session.set("selected_subject", null);
            Session.set("selected_performance_task", null);
            Session.set("selected_standard", null);
            Session.set("selected_student", null);
            Session.set('details_page', 1);
            Session.set('details_num_pages', null);
        }
    });

    // 404
    this.route('notFound', {
        path: '*'
    });
});

// Restrict Access to all pages except public pages
Router.onBeforeAction(function() {
    // Use appropriate login based on environment
    if (Meteor.settings.public.environment === "local") {
        AccountsEntry.signInRequired(this);
    } else {
        Accounts.saml.signInRequired(this);
    }
}, {except: ['invalidUser','entrySignUp','entrySignIn','entryForgotPassword','uploadFile']});

Router.onAfterAction(function() {
    // Actions based on environment
    if (Meteor.settings.public.environment === "local") {
        if (Router.current().route.getName() === 'entrySignIn' && Meteor.user()) {
            Router.go('dashboard');
        } else if (Router.current().route.getName() === 'home' && !Meteor.user()) {
            Router.go('entrySignIn');
        } else if (Router.current().route.getName() === 'entrySignOut') {
            Session.set("selected_group", null);
        }
    } else {
        if (Router.current().route.getName() === 'home' && Meteor.user()) {
            Router.go('dashboard');
        } else if (!Meteor.user() && Router.current().route.getName() !== 'invalidUser') {
            Router.go('home');
        }
    }
});


