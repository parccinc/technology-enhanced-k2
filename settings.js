// Path for domain/parcc
Meteor.startup(function() {
    Meteor.absoluteUrl('parcc');
    /**
     * Forbid account config on the client
     */
    Accounts.config({
        forbidClientAccountCreation: true
    });
});
