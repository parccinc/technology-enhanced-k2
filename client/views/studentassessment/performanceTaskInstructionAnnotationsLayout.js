/*** Template: performanceTaskInstructionAnnotationsLayout ***/
Template.annotationTextContainer.rendered = function() {
    // Add glossary definition to any glossary terms defined in the instructions.
    // Do a case insensitive db search
    $('.glossaryTerm').each(function(index) {
        var glossaryTerm = $(this).text(),
            glossary = Glossary.findOne({'term': new RegExp(["^",glossaryTerm,"$"].join(""),"i")});

        console.log(glossary);
        if (glossary) {
            $(this).attr('title', glossary.definition);
            $(this).attr('data-toggle', 'tooltip');
            $(this).attr('data-html', 'true');
            $(this).tooltip({
                content: glossary.definition,
                placement: 'right',
                container: 'body'
            });
        }
    });
};

Template.annotationTextContainer.events({
    'click .glossaryTerm': function (e) {
        e.stopPropagation();
        $(e.target).tooltip('show');
    }
});

Template.annotationTextContainer.onDestroyed(function () {
    //When annotations container gets destroyed, change the session variable to force the container to re-render
    Session.set('switched', false);
});

////////// UI Display //////////

Template.performanceTaskInstructionAnnotationsLayout.helpers({
    // Get the list of annotation entries
    ptInstructionAnnotations: function () {
        if (Session.get("selected_performancetask") != null &&  Session.get("main_menu_item_selected") != null) {
            var allUserAnnotations = Meteor.user().profile.annotations;

            if (allUserAnnotations && allUserAnnotations.length > 0) {
                if (Session.get("main_menu_item_selected").type === MENU_TYPE_ASSESMENT) {
                    if (Session.get('selected_day') !== null) {
                        if (Session.get('selected_assessment') === null) {
                            return $.grep(allUserAnnotations, function(e){ return e.performance_task_id === Session.get("selected_performancetask")._id && e.day === Session.get('selected_day').day; });
                        } else {
                            return $.grep(allUserAnnotations, function(e){ return e.performance_task_id === Session.get("selected_performancetask")._id && e.assessment_id === Session.get('selected_assessment')._id; });
                        }
                    }
                } else {
                    return $.grep(allUserAnnotations, function(e){ return e.performance_task_id === Session.get("selected_performancetask")._id && e.instruction === Session.get("main_menu_item_selected")._id; });
                }
            }
        }

        return [];
    },

    // Format note date/time
    getFormattedAnnotationDate: function (annotationDate) {
        if (annotationDate) {
            var monthNames = new Array("Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                    "Oct", "Nov", "Dec"),
                d = annotationDate,
                hours = d.getHours(),
                minutes = d.getMinutes(),
                ampm = hours >= 12 ? 'PM' : 'AM';

            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;

            var strTime = hours + ':' + minutes + ' ' + ampm;

            return monthNames[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear() + " " + strTime;
        }

        return "";
    },

    // Determine if user is editing an annotation
    isEditAnnotation: function ()  {
        return Session.get('edit_annotation') != null && Session.get('edit_annotation')._id == this._id;
    },

    switched: function () {
        return Session.get('switched');
    }
});

////////// Events //////////

Template.performanceTaskInstructionAnnotationsLayout.events({
    // Save a new annotation entry
    'submit #add-annotation-form': function (evt, template) {
        evt.preventDefault();

        var annotationTextArea = template.find('textarea[name=textareaInstructionAnnotation]'),
            annotationsEntryData = {
                _id: new Meteor.Collection.ObjectID()._str,
                performance_task_id: Session.get("selected_performancetask")._id,
                annotation_date: new Date(),
                annotation: annotationTextArea.value
            };

        if (Session.get("main_menu_item_selected").type === MENU_TYPE_ASSESMENT) {
            if (Session.get('selected_assessment') === null) {
                annotationsEntryData.day = Session.get('selected_day').day;
            } else {
                annotationsEntryData.assessment_id = Session.get('selected_assessment')._id;
            }
        } else {
            annotationsEntryData.instruction = Session.get("main_menu_item_selected")._id;
        }

        // LS PAR-126: Adding validation on annotation input on both the client and server
        if (annotationsEntryData.annotation && annotationsEntryData.annotation.trim() != "") {
            // Add new annotations entry to the database
            Meteor.call('addUserAnnotationEntry', annotationsEntryData, function (err, annotation) {
                if (!annotation) {
                    showBootstrapAlert("danger", "You cannot enter a blank annotation.");
                } else {
                    annotationTextArea.value = '';
                }
            });
        } else {
            showBootstrapAlert("danger", "You cannot enter a blank annotation.");
        }
        // END OF PAR-126

        console.log("Add User Annotation Entry");

        return false;
    },
    // Edit an annotation entry (display in edit mode)
    'click #edit-annotation-entry': function () {
        Session.set('edit_annotation', this);
    },
    // Save an edited annotation entry
    'submit #edit-annotation-entry-form': function (evt, template) {
        evt.preventDefault();

        var annotationTextArea = template.find('textarea[name=textareaEditInstructionAnnotation]'),
            annotationsEntryData = Session.get('edit_annotation');

        annotationsEntryData.annotation = annotationTextArea.value;

        // LS PAR-126: Adding validation on annotation input on both the client and server
        if (annotationsEntryData.annotation && annotationsEntryData.annotation.trim() != "") {
            // Edit annotations entry in the database
            Meteor.call('editUserAnnotationEntry', annotationsEntryData, function (err, annotation) {
                Session.set('edit_annotation', null);
                if (!annotation) {
                    showBootstrapAlert("danger", "You cannot enter a blank annotation.");
                } else {
                    annotationTextArea.value = '';
                }
            });
        } else {
            showBootstrapAlert("danger", "You cannot enter a blank annotation.");
        }
        // END OF PAR-126

        console.log("Edit User Annotation Entry");

        return false;
    },
    // Cancel edit annotation display mode
    'click #cancel-edit-annotation-entry': function () {
        Session.set('edit_annotation', null);
    },
    // Delete an annotation entry
    'click #delete-annotation-entry': function () {
        var deleteAnnotationEntry = this;

        bootbox.confirm("Are you sure you want to delete this annotation entry?", function(result) {
            if (result === true) {
                // Remove user annotations entry
                Meteor.call('removeUserAnnotationEntry', deleteAnnotationEntry, function(err, annotationEntry) {
                });

                console.log("Remove User Annotation Entry");
            }

            return;
        });
    }
});

Session.set('switched', false);


Template.performanceTaskInstructionAnnotationsLayout.onCreated(function () {
    Session.set('switched', false);
});
