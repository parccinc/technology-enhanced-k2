/*** Template: performanceTaskStudentAssessmentLayout ***/

Session.setDefault("selected_pt_standards_evidence", null);

// If more than one student is selected and at if all student's selected data
// for the given performance task + day + assessment does not match then
// we need to alert them that the "active" selections will not be shown
// for that reason
alertDataCollision = function () {
    if (Session.get('selected_students') !== null
        && Session.get('selected_students').length > 1
        && Session.get('selected_day') !== null
        && Session.get("selected_assessment") !== null) {

        var students = Session.get('selected_students'),
            performanceTaskId = Session.get("selected_performancetask")._id,
            day = Session.get('selected_day').day,
            assessmentId = Session.get("selected_assessment")._id;

        // LS PAR-287: selected_assessment is an object. Needed to look at the 'type' property.
        if (Session.get("selected_assessment").type !== 'yesnolist') {
            var distinctValues = students.map(function(e) {
                    if (e.assessmentdata
                        && e.assessmentdata[performanceTaskId]
                        && e.assessmentdata[performanceTaskId][day]
                        && e.assessmentdata[performanceTaskId][day][assessmentId]
                        && e.assessmentdata[performanceTaskId][day][assessmentId].value) {
                        return e.assessmentdata[performanceTaskId][day][assessmentId].value.join();
                    }

                    return "";
                }
            );
        } else {
            // LS PAR-287: e is not defined here and it was throwing errors in the console.
            //console.log(e.assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation + '_' + e.isobserved;}));
            var distinctValues = students.map(function(e) {
                        if (e.assessmentdata
                            && e.assessmentdata[performanceTaskId]
                            && e.assessmentdata[performanceTaskId][day]
                            && e.assessmentdata[performanceTaskId][day][assessmentId]
                            && e.assessmentdata[performanceTaskId][day][assessmentId].value) {
                            return e.assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation + '_' + e.isobserved;}).join();
                        }

                        return "";
                    }
            );
        }

        var lastValue = distinctValues[0];

        for (var i = 1; i < distinctValues.length; i++) {
            if (distinctValues[i] != lastValue) {
                return true;
            }
        }

        return false;
    }
};

// Save Assessment Data
saveAssessmentDataCallback = function (ids) {
    var updatedStudents = Students.find({_id: {$in: ids}}).fetch();
    Session.set('selected_students', updatedStudents);
    Session.set('view_value', null);

    var groupOrderList = Session.get('group_list_ordered'),
        currentGroup = groupOrderList[Session.get("selected_group")._id];

    for (var i = 0; i < updatedStudents.length; i++) {
        for (var j = 0; j < currentGroup.length; j++) {
            if (updatedStudents[i]._id == currentGroup[j]._id) {
                currentGroup[j] = updatedStudents[i];
            }
        }
    }
    Session.set('group_list_ordered', groupOrderList);

    if (alertDataCollision()) {
        if ($('.bootstrap-growl')) {
            $('.bootstrap-growl').removeClass('bootstrap-growl');
        }

        $.bootstrapGrowl("Well done! You have collected information.", {
            type: 'info',
            align: 'center',
            width: 'auto',
            allow_dismiss: true,
            delay: 5000
        });
    }
};

saveAssessmentData = function (type, assessmentValue, yesno) {
    if(Session.get("selected_students") != null) {
        // If there is more than one student selected, ask them to confirm they want to update for ALL students
        if (Session.get("selected_students").length > 1) {
            bootbox.confirm("Are you sure you want to modify the value for ALL selected students?", function(result) {
                if (result === true) {
                    saveAssessmentDataConfirm(type, assessmentValue, yesno);
                }
            });
        } else {
            saveAssessmentDataConfirm(type, assessmentValue, yesno);
        }
    }
};

saveAssessmentDataConfirm = function (type, assessmentValue, yesno) {
    var selectedStudents = Session.get("selected_students"),
        performanceTaskId = Session.get("selected_performancetask")._id,
        day = Session.get('selected_day').day,
        assessmentId = Session.get("selected_assessment")._id,
        ids = selectedStudents.map(function(e) { return e._id;}),
        setVal = {},
        setTimestamp = {},
        pullVal = {},
        pullListVal = {};

    // run this block to change session info for latency compensation
    var selected_students = Session.get('selected_students').map(
        function (aStudent) {
            console.log(aStudent);
            if (aStudent.assessmentdata === undefined){
                aStudent.assessmentdata = {};
            }
            if (aStudent.assessmentdata[performanceTaskId] === undefined){
                aStudent.assessmentdata[performanceTaskId] = {};
            }
            if (aStudent.assessmentdata[performanceTaskId][day] === undefined){
                aStudent.assessmentdata[performanceTaskId][day] = {};
            }
            if (aStudent.assessmentdata[performanceTaskId][day][assessmentId]  === undefined || aStudent.assessmentdata[performanceTaskId][day][assessmentId].value === undefined) {
                aStudent.assessmentdata[performanceTaskId][day][assessmentId] = {value : [{observation:assessmentValue, isobserved: ''}]}
            }
            var b =  aStudent.assessmentdata[performanceTaskId][day][assessmentId];
            for (var i = 0; i< b.value.length; i++){
                if (b.value[i].observation === assessmentValue) {
                    if (b.value[i].isobserved === yesno){
                        aStudent.assessmentdata[performanceTaskId][day][assessmentId].value[i].isobserved = '';
                    } else {
                        aStudent.assessmentdata[performanceTaskId][day][assessmentId].value[i].isobserved = yesno;
                    }
                }
            }
            return (aStudent);
        }
    );
    Session.set('selected_students', selected_students);

    if (type === 'yesnolist') {
        setVal['assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.value'] = {"observation": assessmentValue, "isobserved": yesno};
        pullListVal['assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.value'] = {"observation": assessmentValue};

    } else {
        setVal['assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.value'] = type === 'checklist' ? assessmentValue : [assessmentValue];
    }

    pullVal['assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.value'] = FLAG_VAL;

    // deselect checklist item
    if ((selectedStudents[0].assessmentdata
        && selectedStudents[0].assessmentdata[performanceTaskId]
        && selectedStudents[0].assessmentdata[performanceTaskId][day]
        && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId]
        && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value)
        && ((type == 'checklist' && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.indexOf(assessmentValue) !== -1)
        || (type == 'rubric' && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.length > 0 && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value[0] === assessmentValue)
        || (type == 'yesnolist'
        && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation;}).indexOf(assessmentValue) !== -1
        && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value[selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation;}).indexOf(assessmentValue)].isobserved === yesno))) {
        if (type == 'checklist') {
            Meteor.call('removeChecklistStudentAssessments', ids, setVal, function (err, updateCount) {
                saveAssessmentDataCallback(ids);
            });
        } else if (type == 'yesnolist') {
            Meteor.call('removeChecklistStudentAssessments', ids, setVal, function (err, updateCount) {
                saveAssessmentDataCallback(ids);
            });
        } else {
            Meteor.call('removeRubricStudentAssessments', ids, setVal, function (err, updateCount) {
                saveAssessmentDataCallback(ids);
            });
        }
        // Update selected rubric/checklist value(s) for selected student(s)
    } else {
        if (type == 'checklist') {
            var removeSet = "";

            setTimestamp['assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.timestamp'] = new Date();

            if (assessmentValue === FLAG_VAL) {
                removeSet = 'assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.value';
            }

            Meteor.call('updateChecklistStudentAssessments', ids, setVal, setTimestamp, pullVal, removeSet, function (err, updateCount) {
                saveAssessmentDataCallback(ids);
            });
        } else if (type == 'yesnolist') {
            var removeSet = "";

            setTimestamp['assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.timestamp'] = new Date();

            if (assessmentValue === FLAG_VAL) {
                removeSet = 'assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.value';
            }

            Meteor.call('updateYesnolistStudentAssessments', ids, setVal, setTimestamp, pullListVal, pullVal, removeSet, function (err, updateCount) {
                saveAssessmentDataCallback(ids);
            });
        } else {
            setVal['assessmentdata.' + performanceTaskId + '.' + day + '.' + assessmentId + '.timestamp'] = new Date();

            Meteor.call('updateRubricStudentAssessments', ids, setVal, function (err, updateCount) {
                saveAssessmentDataCallback(ids);
            });
        }
    }
}

////////// UI Display //////////

//initialize all tooltips in this template
Template.performanceTaskStudentAssessmentLayout.rendered = function() {
    $('.assessment-standard-tip').tooltip();
};

// used to shade the task boxes
shadeColor = function (color, percent) {
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

Template.performanceTaskStudentAssessmentLayout.helpers({
    // Checks to see if a day is the user selected day
    isSelectedDay: function (day) {
        return  Session.get('selected_day') !== null && Session.get('selected_day').day == day;
    },

    // Indicates if a day is the user selected day and returns "active" if it is for css styling purposes
    isActiveDay: function (day) {
        return  Session.get('selected_day') !== null && Session.get('selected_day').day == day ? "active" : "";
    },

    // Indicates if the selected performance task is an ela task
    isSubjectEla: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").subject == 'ela';
        }

        return false;
    },

    // Get the background color based on the performance task subject
    getPTBackgroundColor: function () {
        return Session.get("selected_performancetask").subject === 'ela' ? ELA_BACKGROUND_COLOR : MATH_BACKGROUND_COLOR;
    },

    // Gets the number of assessment steps for the selected day
    getAssessmentCount: function (assessments) {
        if (assessments) {
            if (assessments.length == 1) {
                return assessments.length + " item";
            }

            return assessments.length + " items";
        }

        return "0 items";
    },

    // Indicates if a assessement step is the user selected step and returns "active" if it is for css styling purposes
    isActiveDayAssessment: function (id) {
        return  Session.get('selected_assessment') != null && Session.get('selected_assessment')._id == id ? "active" : "";
    },

    // Checks to see if a step is the user selected step
    isSelectedDayAssessment: function (id) {
        return  Session.get('selected_assessment') != null && Session.get('selected_assessment')._id == id;
    },

    // Get the assessment data
    assessment: function () {
        var assess = Session.get("selected_assessment");

        if (assess) {
            assess.assessValues = _.map(Session.get("selected_assessment").assessValues, function (val, idx) {
                return _.extend(val, {
                    type: assess.type
                });
            });

            return assess;
        }

        return null;
    },

    // If there is a display name for the assessment, use that otherwise use the name value
    getAssessmentName: function () {
        return this.displayName && this.displayName != null && this.displayName.trim().length > 0 ? this.displayName : this.name;
    },

    // Append Step CI or Observations info based upon PT type
    getStepDescription: function () {
        return Session.get("selected_performancetask").subject === 'ela' ? "CI" : "Observations of Students";
    },

    // Determines if rubric or checklist assessment being displayed for
    // layout and selection purposes
    isChecklist: function (type) {
        if (type == 'checklist') {
            return true;
        }

        return false;
    },

    // Determines if yes/no list assessment being displayed for layout and selection
    //purposes
    isYesNoList: function (type) {
        if (type == 'yesnolist') {
            return true;
        }

        return false;
    },

    // Gets the description details of the ccss
    joinWithCommonCoreStandards: function () {
        var commonCoreStandard = this;
        var standards = CommonCoreStateStandards.findOne({_id: commonCoreStandard._id});
        return _.extend(commonCoreStandard, _.omit(standards, '_id'));
    },

    // Determines if assessment value should be displayed in the active color state
    // Only show active if one student is selected.
    isActiveStudentAssessment: function () {
        if (Session.get("selected_assessment") != null) {
            var selectedStudents = Session.get("selected_students"),
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get("selected_day").day,
                assessmentId = Session.get("selected_assessment")._id;

            if (selectedStudents != null
                && (selectedStudents.length == 1
                || (selectedStudents.length > 1 && !alertDataCollision()))) {
                if (selectedStudents[0].assessmentdata
                    && selectedStudents[0].assessmentdata[performanceTaskId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.indexOf(this.value) != -1) {
                    return "active";
                }
            }

            var selectedView = Session.get('view_value');

            if ( selectedView != null && selectedView == this.value) {
                return "active";
            }
        }

        return "";
    },

    // Determines if the css active state should be applied to a yesnolist value
    isActiveStudentYesNoAssessment: function (yesno) {
        if (Session.get("selected_assessment") != null) {
            var selectedStudents = Session.get("selected_students"),
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get("selected_day").day,
                assessmentId = Session.get("selected_assessment")._id;

            if (selectedStudents != null
                && (selectedStudents.length == 1
                || (selectedStudents.length > 1 && !alertDataCollision()))) {
                if (selectedStudents[0].assessmentdata
                    && selectedStudents[0].assessmentdata[performanceTaskId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value) {
                    var observationIndex = selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation;}).indexOf(this.value);

                    if (selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value[observationIndex] !== undefined && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value[observationIndex].isobserved === yesno) {
                        return "active";
                    }
                }
            }
        }

        return "";
    },

    // Determines if assessment value background color should be displayed in performance task gradient
    // Only show active if one student is selected so otherwise performance task gradient color should be displayed.
    isStudentAssessment: function () {
        if (Session.get("selected_assessment") != null) {
            var selectedStudents = Session.get("selected_students"),
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get("selected_day").day,
                assessmentId = Session.get("selected_assessment")._id;

            if (Session.get('view_value') != null && Session.get('view_value') == this.value) {
                return true;
            } else if (selectedStudents != null
                && Session.get('view_value') == null
                && (selectedStudents.length == 1
                || (selectedStudents.length > 1 && !alertDataCollision()))) {
                if (selectedStudents[0].assessmentdata
                    && selectedStudents[0].assessmentdata[performanceTaskId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.indexOf(this.value) != -1) {
                    return true;
                }
            }
        }

        return false;
    },

    // Determines if assessment value background color should be displayed in performance task gradient
    // Only show active if one student is selected so otherwise performance task gradient color should be displayed.
    isStudentYesNoAssessment: function (yesno) {
        if (Session.get("selected_assessment") != null) {
            var selectedStudents = Session.get("selected_students"),
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get("selected_day").day,
                assessmentId = Session.get("selected_assessment")._id;

            if (Session.get('view_value') != null && Session.get('view_value') == this.value) {
                return true;
            } else if (selectedStudents != null
                && Session.get('view_value') == null
                && (selectedStudents.length == 1
                || (selectedStudents.length > 1 && !alertDataCollision()))) {
                if (selectedStudents[0].assessmentdata
                    && selectedStudents[0].assessmentdata[performanceTaskId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId]
                    && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value) {
                    var observationIndex = selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation;}).indexOf(this.value);

                    if (selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value[observationIndex] && selectedStudents[0].assessmentdata[performanceTaskId][day][assessmentId].value[observationIndex].isobserved === yesno) {
                        return true;
                    }
                }
            }
        }

        return false;
    },

    // Return whether assessment values possible response value is an image
    isResponseImage: function (values) {
        var assessValue = this,
            responseIdx = values.map(function(e) { return e._id;}).indexOf(this._id);

        if (responseIdx != -1) {
            return values[responseIdx].isPossibleResponseImage;
        }
        return false;
    },

    // Return assessment values possible response value
    getResponse: function (values) {
        var assessValue = this,
            responseIdx = values.map(function(e) { return e._id;}).indexOf(this._id);

        if (responseIdx != -1) {
            return values[responseIdx].possibleResponse;
        }
        return "";
    },

    // Only display student work product if there is a value
    displayStudentWorkProduct: function () {
        return this.studentWorkProduct && this.studentWorkProduct.trim().length > 0;
    }
});


////////// Events //////////

Template.performanceTaskStudentAssessmentLayout.events({
    // Save a selected assessment value for the selected students
    'click #select-student-assessment': function (evt, template) {
        evt.stopPropagation();

        if (Session.get("selected_students") != null
            && Session.get("selected_performancetask") != null
            && Session.get('selected_day') != null
            && Session.get("selected_assessment") != null) {
            saveAssessmentData(this.type, this.value);
        }
    },
    // Save a selected assessment value for the selected students on a chart matrix type
    'click .select-student-assessment-yesnolist': function (evt, template) {
        evt.stopPropagation();
        var yesno = $(evt.currentTarget).data('assessselection');
        if (Session.get("selected_students") != null
            && Session.get("selected_performancetask") != null
            && Session.get('selected_day') != null
            && Session.get("selected_assessment") != null) {
            saveAssessmentData(this.type, this.value, yesno);
        }
    },
    // When click on eye icon for an assessment value, show all students that have that value selected
    'click .view-students-assessed': function (evt, template) {
        evt.stopPropagation();
        var selectedStudents = [],
            groupOrderList = Session.get('group_list_ordered'),
            currentGroup = groupOrderList[Session.get("selected_group")._id],
            performanceTaskId = Session.get("selected_performancetask")._id,
            day = Session.get("selected_day").day,
            assessmentId = Session.get("selected_assessment")._id;

        for (var i = 0; i < currentGroup.length; i++) {
            if (currentGroup[i].assessmentdata
                && currentGroup[i].assessmentdata[performanceTaskId]
                && currentGroup[i].assessmentdata[performanceTaskId][day]
                && currentGroup[i].assessmentdata[performanceTaskId][day][assessmentId]
                && currentGroup[i].assessmentdata[performanceTaskId][day][assessmentId].value
                && currentGroup[i].assessmentdata[performanceTaskId][day][assessmentId].value.indexOf(this.value) != -1) {
                selectedStudents.push(currentGroup[i]);
            }
        }

        Session.set('selected_students', selectedStudents);
        Session.set('view_value', this.value);
    },
    // Display pt standards and evidence
    'click #standards-evidence-link': function () {
        Session.set("selected_pt_standards_evidence", this.standardsEvidence);

        $('#ptStandardsEvidenceModal').modal("show");
    }
});


/*** Template: performanceTaskArtifacts ***/

////////// UI Display //////////

Template.performanceTaskArtifacts.helpers({
    // Get media for selected student, but only show media if one student is selected
    performanceTaskMedia: function () {
        if (Session.get("selected_students") != null && Session.get("selected_students").length == 1 && Session.get("selected_performancetask") != null) {
            var selectedStudentIds = Session.get("selected_students").map(function(e) { return e._id;});

            return Media.find({performance_task_id: Session.get("selected_performancetask")._id, students: { $in: selectedStudentIds }}, {sort: {captured_date: -1}});
        }
    },

    // Format note date/time
    getFormattedDate: function (noteDate) {
        if (noteDate) {
            var monthNames = new Array("Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                    "Oct", "Nov", "Dec"),
                d = noteDate,
                hours = d.getHours(),
                minutes = d.getMinutes(),
                ampm = hours >= 12 ? 'PM' : 'AM';

            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;

            var strTime = hours + ':' + minutes + ' ' + ampm;

            return monthNames[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear() + " " + strTime;
        }

        return "";
    },

    // Determine if the media item is a note
    isNote: function () {
        return this.type == 'note';
    },

    // Get note student details
    joinWithStudents: function () {
        var student = {_id: this.toString()};

        var students = Students.findOne({_id: student._id});
        return  _.extend(student, _.omit(students, '_id'));
    },

    // Get the actual media file
    fileInfo: function () {
        if (this) {
            return MediaData.findOne({_id: this._id});
        }

        return '';
    }
});

////////// Events //////////

Template.performanceTaskArtifacts.events({
    // Save a selected assessment value for the selected students
    'click .edit-note': function (evt, template) {
        Session.set('selected_performancetask_artifact', this);

        $('#artifactModal').modal("show");
    },

    'click #view-artifact-image': function (evt, template) {
        $('#artifact-display-img').attr('src', $(evt.currentTarget).data('url'));
        $('#artifactDisplayImageModal').modal("show");
    },
    // Delete an annotation entry
    'click .delete-note': function () {
        var deleteNote = this;

        bootbox.confirm("Are you sure you want to delete this note?", function(result) {
            if (result === true) {
                console.log(deleteNote);
                // Remove student note
                Meteor.call('removeMedia', deleteNote._id, function(err) {
                    if (err) {
                        console.log(err);
                    }
                });

                console.log("Remove Student Note");
            }

            return;
        });
    }
});


/*** Template: ptStandardsEvidenceModal ***/

////////// UI Display //////////

Template.ptStandardsEvidenceModal.helpers({
    // Get performance task step standards and evidence to display
    getStandardsEvidence: function () {
        return Session.get("selected_pt_standards_evidence");
    }
});