// Parcc group performance task assessments -- client

////////// Variables //////////

// Main Menu Constants
var PLAN_MENU_ITEM = "plan",
    OVERVIEW_MENU_ITEM = "overview",
    OUTLINE_MENU_ITEM = "outline",
    READY_SET_MENU_ITEM = "readySetGo",
    STEPS_MENU_ITEM = "steps",
    STANDARDS_MENU_ITEM = "ccssEvidence",
    IF_THEN_MENU_ITEM = "ifThen";

MENU_TYPE_INSTRUCTION = "Instruction",
    MENU_TYPE_ASSESMENT = "Assessment";

// Subject Background Colors
ELA_BACKGROUND_COLOR = "#009900",
    MATH_BACKGROUND_COLOR = "#72547D";

// Default session of ordered group lists via drag and drop ordering
Session.setDefault('main_menu_item_selected', null);

// Default session of ordered group lists via drag and drop ordering
Session.setDefault('group_list_ordered', null);

// Default currently selected students
Session.setDefault('selected_students', null);

// Default currently selected performance task
Session.setDefault('selected_performancetask', null);

// Default currently selected performance task day
Session.setDefault('selected_day', null);

// Default currently selected performance task assessment
Session.setDefault('selected_assessment', null);

// Default currently selected artifact
Session.setDefault('selected_performancetask_artifact', null);

// Default of assessment view (eye) icon value
Session.setDefault('view_value', null);

// Default of edit annotation entry
Session.setDefault('edit_annotation', null);

// Default of nav display
Session.set('isNavDisplayed', true);

/*** Template: studentassessment ***/

////////// UI Display //////////


//Template.studentassessment.rendered = function () {
//    var xStart, yStart = 0;
//
//    document.addEventListener('touchstart',function(e) {
//        xStart = e.touches[0].screenX;
//        yStart = e.touches[0].screenY;
//    });
//
//    document.addEventListener('touchmove',function(e) {
//        var xMovement = Math.abs(e.touches[0].screenX - xStart);
//        var yMovement = Math.abs(e.touches[0].screenY - yStart);
//        if((yMovement * 3) > xMovement) {
//            e.preventDefault();
//        }
//    });
//}

/*var addGlossaryToolTips = function (contentText) {
    var div = document.createElement('div');
    div.innerHTML = contentText;

    var arr = div.getElementsByClassName("glossaryTerm");

    for(var i = 0; i < arr.length; i++) {
        var glossaryTerm = arr[i].innerHTML,
            temp = document.createElement("div");
            temp.innerHTML = glossaryTerm;

        var sanitized = temp.textContent || temp.innerText,
            glossary = Glossary.findOne({'term': new RegExp(["^",sanitized,"$"].join(""),"i")});

        if (glossary) {
            arr[i].setAttribute('data-title', glossary.definition);
            arr[i].setAttribute('data-toggle', 'tooltip');
            arr[i].setAttribute('data-html', 'true');
            arr[i].setAttribute('data-placement', 'right');
        }
    }

    return div.innerHTML;
};   */

Template.studentassessment.helpers({
    // Check to see if an image exists for the PT
    isImageFound: function () {
        return this.visual.trim().length > 0;
    },

    // Get the performance task info
    performanceTask: function () {
    return Session.get("selected_performancetask");
    },

    // Get the background color based on the performance task subject
    getPTBackgroundColor: function () {
        return Session.get("selected_performancetask").subject === 'ela' ? ELA_BACKGROUND_COLOR : MATH_BACKGROUND_COLOR;
    },

    // Create performance task main menu
    mainMenuItems: function () {
        if (Session.get("selected_performancetask") != null) {
            var menuItems = [];

            if (Session.get("selected_performancetask").subject === 'ela') {
                var plan = { _id: PLAN_MENU_ITEM,
                    name: "Task<br> At-A-Glance",
                    type: MENU_TYPE_INSTRUCTION,
                    content: Session.get("selected_performancetask").instructions.plan};

                menuItems.push(plan);
            }

            var overview = { _id: OVERVIEW_MENU_ITEM,
                name: "Overview",
                type: MENU_TYPE_INSTRUCTION,
                content: Session.get("selected_performancetask").instructions.overview};

            menuItems.push(overview);

            if (Session.get("selected_performancetask").subject === 'math') {
                var standards = { _id: STANDARDS_MENU_ITEM,
                    name: "Standards",
                    type: MENU_TYPE_INSTRUCTION,
                    content: Session.get("selected_performancetask").instructions.ccssEvidence};

                menuItems.push(standards);
            } else {
                var outline = { _id: OUTLINE_MENU_ITEM,
                    name: "Days<br>At-A-Glance",
                    type: MENU_TYPE_INSTRUCTION,
                    content: Session.get("selected_performancetask").instructions.outline};

                menuItems.push(outline);

                var standards = { _id: STANDARDS_MENU_ITEM,
                    name: "CCSS<br>Alignment",
                    type: MENU_TYPE_INSTRUCTION,
                    content: Session.get("selected_performancetask").instructions.ccssEvidence};

                menuItems.push(standards);
            }

            var readySet = { _id: READY_SET_MENU_ITEM,
                name: "Get Ready,<br> Get Set, Go!",
                type: MENU_TYPE_INSTRUCTION,
                content: Session.get("selected_performancetask").instructions.readySetGo};

            menuItems.push(readySet);

            var steps = { _id: STEPS_MENU_ITEM,
                name: "Step-By-Step<br>Directions",
                type: MENU_TYPE_ASSESMENT };

            menuItems.push(steps);

            if (Session.get("selected_performancetask").subject === 'ela') {
                var ifThen = { _id: IF_THEN_MENU_ITEM,
                    name: "If...Then",
                    type: MENU_TYPE_INSTRUCTION,
                    content: Session.get("selected_performancetask").instructions.ifThen};

                menuItems.push(ifThen);
            }

            return menuItems;
        }

        return [];
    },

    // Determines if a menu item is the currently selected menu item
    isActiveMenuItem: function () {
        return Session.get('main_menu_item_selected') != null && Session.get('main_menu_item_selected')._id == this._id ? "active": "";
    },

    // Determines if a menu item is the currently selected menu item
    isMenuItemSelected: function () {
        return Session.get('main_menu_item_selected') != null && Session.get('main_menu_item_selected')._id == this._id;
    },

    // Get the student resources file for the selected peformance task
    getStudentResources: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").studentResource;
        }

        return "";
    },

    // Get the color assigned to the selected peformance task
    getTaskColor: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").color;
        }

        return "";
    },

    // Determine if the PT is an ELA PT
    isSubjectEla: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").subject == 'ela';
        }

        return false;
    },

    // Determine if the menu item selected is an instruction or an assessment
    isStudentAssessment: function () {
        return Session.get("main_menu_item_selected") != null && Session.get("main_menu_item_selected").type == MENU_TYPE_ASSESMENT;
    },

    // Determine if the assessment step menu item selected is an instruction or an assessment
    isStudentAssessmentLayout: function () {
        return Session.get("main_menu_item_selected") !== null
            && Session.get("main_menu_item_selected").type === MENU_TYPE_ASSESMENT
            && Session.get('selected_assessment') !== null
            && Session.get('selected_assessment').stepType !== "instruction";
    },

    // Determines if the nav caret direction should be up or down based on display of nav
    getNavCareDirection: function () {
        return Session.get('isNavDisplayed') ? "up" : "down";
    },

    // Determine if a step is an instruction type of step
    isInstructionStep: function () {
        return this.stepType === "instruction";
    },

    // Gets the assessments for the selected day
    dayAssessments: function () {
        var assessments = [];

        if ( Session.get('selected_day') !== null) {
            var dayIdx = Session.get("selected_performancetask").assessmentSchedule.map(function(e) { return e.day; }).indexOf(Session.get('selected_day').day);

            if (dayIdx != -1) {
                return Session.get("selected_performancetask").assessmentSchedule[dayIdx].assessments;
            }
        }

        return assessments;
    },

    // Merges assessment data (admin->assessment rubric/checklist/yesnolist) with the performance task assessment data
    joinWithDayAssessments: function () {
        var assessment = this;
        var assessments = Assessments.findOne({_id: assessment.assessment_id});
        return _.extend(assessment, _.omit(assessments, '_id'));
    },

    // Determines if the day label should be day or part based on whether the performance
    // task selected is ela or math
    subjectDayLabel: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").subject == 'ela' ? "Day" : "Part";
        }

        return "";
    },

    // Return the day selected value
    getSelectedDay: function () {
        if (Session.get('selected_day') !== null) {
            return Session.get('selected_day').day;
        }

        return "";
    },

    // Return the day selected step
    getSelectedStep: function () {
        if (Session.get('selected_assessment') != null) {
            return Session.get('selected_assessment').step;
        } else if (Session.get("selected_performancetask").subject === 'ela') {
            return "Time and Materials";
        }

        return "";
    },

    // Determine if selected step is an instruction
    isSelectedStepInstruction: function () {
        return Session.get('selected_assessment') !== null && Session.get('selected_assessment').stepType !== 'instruction' ? false : true;
    },

    // Determine if the previous step button should be disabled
    previousBtnDisabled: function () {
        if (Session.get('selected_day') != null) {
            var selectedPT = Session.get("selected_performancetask"),
                dayIdx = selectedPT.assessmentSchedule.map(function(e) { return e.day; }).indexOf(Session.get('selected_day').day);

            if (Session.get('selected_assessment') === null) {
                if ((dayIdx - 1) >=  0
                    && selectedPT.assessmentSchedule[dayIdx - 1].assessments !== null
                    && selectedPT.assessmentSchedule[dayIdx - 1].assessments.length > 0) {
                    return "";
                }
            } else {
                var assessmentIdx = selectedPT.assessmentSchedule[dayIdx].assessments.map(function(e) { return e._id; }).indexOf(Session.get('selected_assessment')._id);

                if ((assessmentIdx - 1) >=  -1 && Session.get("selected_performancetask").subject === 'ela') {
                    return "";
                } else if ((assessmentIdx - 1) >=  0 && Session.get("selected_performancetask").subject === 'math') {
                    return "";
                } else if ((dayIdx - 1) >=  0) {
                    return "";
                }
            }
        }

        return "disabled";
    },

    // Determine if the next step button should be disabled
    nextBtnDisabled: function () {
        if (Session.get('selected_day') !== null) {
            var selectedPT = Session.get("selected_performancetask"),
                dayIdx = selectedPT.assessmentSchedule.map(function(e) { return e.day; }).indexOf(Session.get('selected_day').day);

            if (Session.get('selected_assessment') === null) {
                if (selectedPT.assessmentSchedule[dayIdx].assessments !== null
                    && selectedPT.assessmentSchedule[dayIdx].assessments.length > 0) {
                    return "";
                }
            } else {
                var assessmentIdx = selectedPT.assessmentSchedule[dayIdx].assessments.map(function(e) { return e._id; }).indexOf(Session.get('selected_assessment')._id);

                if ((assessmentIdx + 1) <  selectedPT.assessmentSchedule[dayIdx].assessments.length) {
                    return "";
                } else if ((dayIdx + 1) < selectedPT.assessmentSchedule.length) {
                    return "";
                }
            }
        }

        return "disabled";
    },

    // Set instruction content based on main menu instruction or step menu instruction selected
    instructionAnnotationText: function () {
        //Change session variable to force re-render "annotationTextContainer" template
        Session.set('switched', true);

        if (Session.get("main_menu_item_selected") !== null
            && Session.get("main_menu_item_selected").type === MENU_TYPE_ASSESMENT
            && Session.get('selected_assessment') !== null
            && Session.get('selected_assessment').stepType === "instruction") {
            return Session.get("selected_assessment").stepInstruction;
        } else if (Session.get("main_menu_item_selected") !== null
                && Session.get("main_menu_item_selected").type === MENU_TYPE_ASSESMENT
                && Session.get('selected_assessment') === null
                && Session.get('selected_day') !== null) {
                return Session.get('selected_day').dayInstructions;
        } else if (Session.get("main_menu_item_selected") !== null
            && Session.get("main_menu_item_selected").type === MENU_TYPE_INSTRUCTION) {
            return Session.get("main_menu_item_selected").content;
        }

        return "";
    }
});

////////// Events //////////

Template.studentassessment.events({
    'click': function(){
        $('.glossaryTerm').tooltip('hide');
    },
    // User clicks a selected menu item and it gets set in a Session variable
    'click #menu-instruction': function (evt, template) {
        Session.set('main_menu_item_selected', this);
        $("#instruction-text").scrollTop(0);
    },
    // User clicks the step navigation caret and it sets the navigation area to display or not
    'click #nav-collapse-link': function (evt, template) {
        var isNavDisplayed = Session.get('isNavDisplayed');

        Session.set('isNavDisplayed', !isNavDisplayed);
    },
    // User selects a performance task day
    'click .select-day': function (e) {
        e.preventDefault();
        var selectedPT = Session.get("selected_performancetask"),
            dayIdx = selectedPT.assessmentSchedule.map(function(e) { return e.day; }).indexOf(this.day);

        // Set the day being selected
        Session.set('selected_day', this);

        // Reset the assessments to pick from for the day
        // in the navigation submenu
        if (selectedPT.subject === 'math' && selectedPT.assessmentSchedule[dayIdx].assessments.length > 0) {
            Session.set('selected_assessment', selectedPT.assessmentSchedule[dayIdx].assessments[0]);
        } else {
            Session.set('selected_assessment', null);
        }

        // Reset selected students back to none selected
        Session.set('selected_students', null);
        Session.set('view_value', null);
    },
    // User selects a peformance task step
    'click .select-step': function (e) {
        e.preventDefault();
        var selectedAssessment = this;

        if (!this.stepType) {
            Session.set('selected_assessment', null);
        } else {
            if (this.stepType === "instruction") {
                Session.set('selected_assessment', this);
            } else {
                Meteor.call('getAssessment', selectedAssessment.assessment_id, function (err, assessment) {
                    if (assessment) {
                        // Set the assessment
                        Session.set('selected_assessment', _.extend(selectedAssessment, _.omit(assessment, '_id')));

                        // Reset selected students back to none selected
                        Session.set('selected_students', null);
                        Session.set('view_value', null);
                    }
                });
            }
        }
    },
    // User clicks the next button to see the next step
    'click #btn-next': function () {
        var selectedPT = Session.get("selected_performancetask"),
            dayIdx = selectedPT.assessmentSchedule.map(function(e) { return e.day; }).indexOf(Session.get('selected_day').day);

        if (Session.get('selected_assessment') === null) {
            if (selectedPT.assessmentSchedule[dayIdx].assessments !== null
                    && selectedPT.assessmentSchedule[dayIdx].assessments.length > 0) {
                var selectedAssessment = selectedPT.assessmentSchedule[dayIdx].assessments[0];

                if (selectedAssessment.stepType === "instruction") {
                    Session.set('selected_assessment', selectedAssessment);
                } else {
                    Meteor.call('getAssessment', selectedAssessment.assessment_id, function (err, assessment) {
                        if (assessment) {
                            // Set the assessment
                            Session.set('selected_assessment', _.extend(selectedAssessment, _.omit(assessment, '_id')));
                        }
                    });
                }
            }

        } else {
            var assessmentIdx = selectedPT.assessmentSchedule[dayIdx].assessments.map(function(e) { return e._id; }).indexOf(Session.get('selected_assessment')._id);

            if ((assessmentIdx + 1) <  selectedPT.assessmentSchedule[dayIdx].assessments.length) {
                var selectedAssessment = selectedPT.assessmentSchedule[dayIdx].assessments[assessmentIdx + 1];

                if (selectedAssessment.stepType === "instruction") {
                    Session.set('selected_assessment', selectedAssessment);
                } else {
                    Meteor.call('getAssessment', selectedAssessment.assessment_id, function (err, assessment) {
                        if (assessment) {
                            // Set the assessment
                            Session.set('selected_assessment', _.extend(selectedAssessment, _.omit(assessment, '_id')));
                        }
                    });
                }
            } else if ((dayIdx + 1) < selectedPT.assessmentSchedule.length) {
                if ((assessmentIdx + 1) ===  selectedPT.assessmentSchedule[dayIdx].assessments.length && Session.get("selected_performancetask").subject === 'ela') {
                    Session.set('selected_day', selectedPT.assessmentSchedule[dayIdx + 1]);
                    Session.set('selected_assessment', null);
                } else {
                    var selectedAssessment = selectedPT.assessmentSchedule[dayIdx + 1].assessments[0];

                    Session.set('selected_day', selectedPT.assessmentSchedule[dayIdx + 1]);

                    if (selectedAssessment.stepType === "instruction") {
                        Session.set('selected_assessment', selectedAssessment);
                    } else {
                        Meteor.call('getAssessment', selectedAssessment.assessment_id, function (err, assessment) {
                            if (assessment) {
                                // Set the assessment
                                Session.set('selected_assessment', _.extend(selectedAssessment, _.omit(assessment, '_id')));
                            }
                        });
                    }
                }
            }
        }
        $("#instruction-text").scrollTop(0);
    },
    // User clicks the previous button to see the previous step
    'click #btn-previous': function () {
        var selectedPT = Session.get("selected_performancetask"),
            dayIdx = selectedPT.assessmentSchedule.map(function(e) { return e.day; }).indexOf(Session.get('selected_day').day);

        if (Session.get('selected_assessment') === null) {
            if ((dayIdx - 1) >=  0
                && selectedPT.assessmentSchedule[dayIdx - 1].assessments !== null
                && selectedPT.assessmentSchedule[dayIdx - 1].assessments.length > 0) {
                var selectedAssessment = selectedPT.assessmentSchedule[dayIdx - 1].assessments[selectedPT.assessmentSchedule[dayIdx - 1].assessments.length-1];

                Session.set('selected_day', selectedPT.assessmentSchedule[dayIdx - 1]);

                if (selectedAssessment.stepType === "instruction") {
                    Session.set('selected_assessment', selectedAssessment);
                } else {
                    Meteor.call('getAssessment', selectedAssessment.assessment_id, function (err, assessment) {
                        if (assessment) {
                            // Set the assessment
                            Session.set('selected_assessment', _.extend(selectedAssessment, _.omit(assessment, '_id')));
                        }
                    });
                }
            }
        } else {
            var assessmentIdx = selectedPT.assessmentSchedule[dayIdx].assessments.map(function(e) { return e._id; }).indexOf(Session.get('selected_assessment')._id);

            if ((assessmentIdx - 1) >=  0) {
                var selectedAssessment = selectedPT.assessmentSchedule[dayIdx].assessments[assessmentIdx - 1];

                if (selectedAssessment.stepType === "instruction") {
                    Session.set('selected_assessment', selectedAssessment);
                } else {
                    Meteor.call('getAssessment', selectedAssessment.assessment_id, function (err, assessment) {
                        if (assessment) {
                            // Set the assessment
                            Session.set('selected_assessment', _.extend(selectedAssessment, _.omit(assessment, '_id')));
                        }
                    });
                }
            } else if ((assessmentIdx - 1) ===  -1 && Session.get("selected_performancetask").subject === 'ela') {
                    Session.set('selected_assessment', null);
            } else if ((dayIdx - 1) >=  0) {
                var selectedAssessment = selectedPT.assessmentSchedule[dayIdx - 1].assessments[selectedPT.assessmentSchedule[dayIdx - 1].assessments.length-1];

                Session.set('selected_day', selectedPT.assessmentSchedule[dayIdx - 1]);

                if (selectedAssessment.stepType === "instruction") {
                    Session.set('selected_assessment', selectedAssessment);
                } else {
                    Meteor.call('getAssessment', selectedAssessment.assessment_id, function (err, assessment) {
                        if (assessment) {
                            // Set the assessment
                            Session.set('selected_assessment', _.extend(selectedAssessment, _.omit(assessment, '_id')));
                        }
                    });
                }
            }
        }
        $("#instruction-text").scrollTop(0);

    },
    // Add note/media to selected students
    'click #add-media': function (evt, template) {
        if (Session.get("selected_students") != null && Session.get("selected_students").length > 0){
            var selectedCCSS = [];

            if (Session.get("selected_assessment") != null) {
                selectedCCSS = Session.get("selected_assessment").commonCoreStateStandards;
            }

            var noteVal = {
                type: "note",
                performance_task_id: Session.get("selected_performancetask")._id,
                note: "",
                commonCoreStateStandards: selectedCCSS,
                captured_date: new Date(),
                students: Session.get("selected_students").map(function(e) { return e._id;})
            };

            Session.set('selected_performancetask_artifact', noteVal);

            $('#artifactModal').modal("show");
        } else {
            $.bootstrapGrowl("An artifact cannot be added. You must select a student to add an artifact.", {
                type: 'warning',
                align: 'center',
                width: 'auto',
                allow_dismiss: true,
                delay: 4000
            });
        }
    }
});


/*** Template: artifactModal ***/

////////// UI Display //////////

Template.artifactModal.helpers({
    // Returns selected media object data
    ptMedia: function () {
        if (Session.get('selected_performancetask_artifact') != null) {
            return Session.get('selected_performancetask_artifact');
        }
    },

    // Determines if note media type is selected
    isNote: function () {
        if (Session.get('selected_performancetask_artifact') != null
            && Session.get('selected_performancetask_artifact').type == "note") {
            return true;
        }

        return false;
    },

    // Defines active media type selected for upload
    isActiveMediaType: function (type) {
        return  Session.get('selected_performancetask_artifact') != null
            && Session.get('selected_performancetask_artifact').type == type ? "active" : "";
    },

    // Define file upload MIME types allowed based on media type selected
    getAcceptTypes: function () {
        if (Session.get('selected_performancetask_artifact') != null) {
            if (Session.get('selected_performancetask_artifact').type == 'audio') {
                return "audio/*";
            } else if (Session.get('selected_performancetask_artifact').type == 'image') {
                return "image/*";
            } else if (Session.get('selected_performancetask_artifact').type == 'video') {
                return "video/*";
            }
        }

        return "";
    },

    // Determines is we are editing an artifact or creating a new one
    isEditMode: function () {
        return Session.get('selected_performancetask_artifact') != null && Session.get('selected_performancetask_artifact')._id;
    }
});

////////// Events //////////

Template.artifactModal.events({
    // User selects to add an image artifact to the student for the performance task selected
    'click #select-image': function (evt, template) {
        var selectedCCSS = [];

        if (Session.get("selected_assessment") != null) {
            selectedCCSS = Session.get("selected_assessment").commonCoreStateStandards;
        }

        var imageVal = {
            type: "image",
            performance_task_id: Session.get("selected_performancetask")._id,
            file: "",
            commonCoreStateStandards: selectedCCSS,
            captured_date: new Date(),
            students: Session.get("selected_students").map(function(e) { return e._id;})
        };

        Session.set('selected_performancetask_artifact', imageVal);
    },

    // User selects to add a note artifact to the student for the perfomance task selected
    'click #select-note': function (evt, template) {
        var selectedCCSS = [];

        if (Session.get("selected_assessment") != null) {
            selectedCCSS = Session.get("selected_assessment").commonCoreStateStandards;
        }

        var noteVal = {
            type: "note",
            performance_task_id: Session.get("selected_performancetask")._id,
            note: "",
            commonCoreStateStandards: selectedCCSS,
            captured_date: new Date(),
            students: Session.get("selected_students").map(function(e) { return e._id;})
        };

        Session.set('selected_performancetask_artifact', noteVal);
    },

    // Clear note text area
    'click #clear-note': function (evt, template) {
        if (Session.get('selected_performancetask_artifact') != null) {
            var taskNote = Session.get('selected_performancetask_artifact');

            if (taskNote._id) {
                taskNote.note = "";
            } else {
                template.find('textarea[name=textareaNote]').value = "";
            }

            Session.set('selected_performancetask_artifact', taskNote);
        }
    },

    // Exit modal logic
    'click .exit-modal': function (evt, template) {
        template.find('textarea[name=textareaNote]').value = "";
    },

    // Save performance task media
    'click #save-media': function (evt, template) {
        var taskMedia = Session.get('selected_performancetask_artifact');

        // A performance task and at least one student must be selected
        if (taskMedia != null && taskMedia.students != null) {
            // Save note
            if (taskMedia.type == 'note') {
                taskMedia.note = template.find('textarea[name=textareaNote]').value;

                // edit note
                if (taskMedia._id) {
                    delete taskMedia._id;

                    // LS PAR-126: Adding input validation on notes to both client and server
                    if (taskMedia.note && taskMedia.note != "") {
                        Meteor.call('updateMedia', Session.get('selected_performancetask_artifact')._id, taskMedia, function (err, note) {
                            $('#artifactModal').modal("hide");
                            template.find('textarea[name=textareaNote]').value = "";
                            Session.set('selected_performancetask_artifact', null);

                            // Show alert if server does not add the note
                            if (!note) {
                                showBootstrapAlert("danger", "You cannot enter a blank note.");
                            }
                        });
                    } else {
                        $('#artifactModal').modal("hide");
                        template.find('textarea[name=textareaNote]').value = "";
                        Session.set('selected_performancetask_artifact', null);
                        showBootstrapAlert("danger", "You cannot enter a blank note.");
                    }
                    // END OF PAR-126
                // add note
                } else {
                    // LS PAR-126: Adding input validation on notes to both client and server
                    if (taskMedia.note && taskMedia.note.trim() != "") {
                        Meteor.call('addMedia', taskMedia, function (err, note) {
                            $('#artifactModal').modal("hide");
                            template.find('textarea[name=textareaNote]').value = "";
                            Session.set('selected_performancetask_artifact', null);

                            // Show alert if server does not add the note
                            if (!note) {
                                showBootstrapAlert("danger", "You cannot enter a blank note.");
                            }
                        });
                    } else {
                        $('#artifactModal').modal("hide");
                        template.find('textarea[name=textareaNote]').value = "";
                        Session.set('selected_performancetask_artifact', null);
                        showBootstrapAlert("danger", "You cannot enter a blank note.");
                    }
                    // END OF PAR-126
                }
            // Save audio, video or image
            } else {
                // For now only upload one file at a time
                // so get file to be uploaded.
                var files = $('#file-media').get(0).files;
                taskMedia.file = [];

                for (var i = 0; i < files.length; i++) {
                    // Upload media file
                    var fileObj = MediaData.insert(files[i]);
                    console.log('Upload result: ', fileObj);

                    taskMedia.file[i] = fileObj;
                }

                // Save media info to the mongodb
                Meteor.call('addMedia', taskMedia, function (err, media) {
                    $('#artifactModal').modal("hide");
                    Session.set('selected_performancetask_artifact', null);
                });
            }
        }
    }
});
