/*** Template: students ***/

// Student Assessment value Constants
// Student can be "scored" or if they
// are not "scored" they can be marked
// as:
//      COMPLETE = No value assigned because I did not collect
//                  enough info from you to do so.
//      FLAG = I need to collect more info no value assigned.
//                  It is a reminder to come back.
//      NOT COMPLETE = Nothing has been done with this student
//                  yet.
COMPLETE_VAL = "COMPLETE",
NOT_COMPLETE_VAL = "NOT COMPLETE",
FLAG_VAL = "FLAG";

////////// UI Display //////////

Template.students.rendered = function() {
    // Allow user to drag students around to sort them
    $('#student-list').sortable({
        stop: function(event, ui) {
            var newGroupOrderList = [],
                groupOrderList = Session.get('group_list_ordered'),
                currentGroupOrder = groupOrderList[Session.get("selected_group")._id];

            $("#student-list li").each(function(i, el){
                var id = $(el).data('id');
                newGroupOrderList.push(currentGroupOrder[currentGroupOrder.map(function(e) { return e._id; }).indexOf(id)]);
            });


            groupOrderList[Session.get("selected_group")._id] = newGroupOrderList;

            Session.set('group_list_ordered', groupOrderList);
        }
    });
};

Template.students.helpers({
    // Get list of students for the selected group
    students: function () {
        if (Session.get("selected_group") != null) {
            var groupOrderList = Session.get('group_list_ordered');

            // If the selected group has been ordered then let's just use that order
            // for display of the student list
            if (groupOrderList != null) {
                if (groupOrderList[Session.get("selected_group")._id]) {
                    return groupOrderList[Session.get("selected_group")._id];
                }
            }

            groupOrderList = {};

            // If the selected group is a group then list the group's students.
            if (Session.get("selected_group").students) {
                var studentList = Students.find({ _id: { $in: Session.get("selected_group").students } }).fetch();

                if (studentList.length > 0) {
                    var sortingArr = Session.get("selected_group").students;

                    groupOrderList[Session.get("selected_group")._id] = sortArrayByArray(sortingArr, studentList, "_id");

                    Session.set('group_list_ordered', groupOrderList);

                    return studentList;
                }
            // If the selected group is a classroom then find that classroom's student's and list them
            } else {
                var studentList = Students.find({classroom_id: Session.get("selected_group")._id}, {sort: {lastname: 1}}).fetch();

                if (studentList.length > 0) {
                    groupOrderList[Session.get("selected_group")._id] = studentList;
                    Session.set('group_list_ordered', groupOrderList);

                    return studentList;
                }
            }
        }

        return [];
    },

    // Determine if a student is currently selected and therefore should use the active css styling
    isActiveStudent: function (id) {
        return Session.get("selected_students") != null && Session.get("selected_students").map(function(e) { return e._id; }).indexOf(id) != -1 ? "active" : "";
    },

    // Determines if the student has been assessed for the assessment (rubric, checklist, yesnolist) the user is looking at
    isStudentAssessed: function () {
        if (Session.get("selected_performancetask") != null
            && Session.get('selected_day') != null
            && Session.get("selected_assessment") != null) {

            var currentStudent = this,
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get('selected_day').day,
                assessmentId = Session.get("selected_assessment")._id;

            if (currentStudent.assessmentdata
                && currentStudent.assessmentdata[performanceTaskId]
                && currentStudent.assessmentdata[performanceTaskId][day]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value != undefined
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value != null
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value.length > 0) {
                return true;
            }
        }

        return false;
    },

    //  Determines if the student has been flagged for the assessment (rubric, checklist, yesnolist the user is looking at
    isStudentFlagged: function () {
        if (Session.get("selected_performancetask") != null
            && Session.get('selected_day') != null
            && Session.get("selected_assessment") != null) {

            var currentStudent = this,
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get('selected_day').day,
                assessmentId = Session.get("selected_assessment")._id,
                assessmentType = Session.get("selected_assessment").type;

            if (currentStudent.assessmentdata
                && currentStudent.assessmentdata[performanceTaskId]
                && currentStudent.assessmentdata[performanceTaskId][day]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId] != null
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value != null
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value.length > 0
                && ((assessmentType !== 'yesnolist' && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value.indexOf(FLAG_VAL) != -1)
                    || (assessmentType === 'yesnolist' && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation;}).indexOf(FLAG_VAL) != -1))) {
                return true;
            }
        }

        return false;
    },

    // Determines if the currently select PT assessment is a rubric type
    isRubric: function () {
        if (Session.get("selected_assessment") != null) {
            return Session.get("selected_assessment").type === 'rubric';
        }

        return false;
    },

    // Get the character value of the student's rubric assessment value
    getStudentRubricAssessmentValue: function () {
        if (Session.get("selected_performancetask") != null
            && Session.get('selected_day') != null
            && Session.get("selected_assessment") != null) {

            var currentStudent = this,
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get('selected_day').day,
                assessmentId = Session.get("selected_assessment")._id;

            if (currentStudent.assessmentdata
                && currentStudent.assessmentdata[performanceTaskId]
                && currentStudent.assessmentdata[performanceTaskId][day]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId] != null
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value != null
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value.length > 0) {

                if (currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value[0] === "Meeting") {
                    return "M";
                } else if (currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value[0] === "Developing") {
                    return "D";
                } else if (currentStudent.assessmentdata[performanceTaskId][day][assessmentId].value[0] === "Emerging") {
                    return "E";
                }
            }
        }

        return "";
    },

    // Used for bootstrap odd/even placement for a 2 column list of students
    getOddEvenColumn: function (id) {
        var groupOrderList = Session.get('group_list_ordered');

        if (groupOrderList != null) {
            var currentList = groupOrderList[Session.get("selected_group")._id],
                currentIdx = currentList.map(function(e) { return e._id; }).indexOf(id);

            if (currentIdx != -1 && currentIdx % 2) {
                return "odd";
            }

            return "even";
        }

        return "";
    },

    studentsSelected: function () {
        return Session.get("selected_students") && Session.get("selected_students").length > 0;
    }
});


////////// Events //////////

// Function to sort an array but a specified object key
function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

// Function to sort an array by the marked completed status
function sortByComplete(groupList, type) {
    if (Session.get("selected_performancetask") != null
        && Session.get('selected_day') != null
        && Session.get("selected_assessment") != null) {
        var asssessedList = [],
            notCompleteList = [],
            flaggedList = [],
            performanceTaskId = Session.get("selected_performancetask")._id,
            day = Session.get('selected_day').day,
            assessmentId = Session.get("selected_assessment")._id,
            assessmentType = Session.get("selected_assessment").type;

        for (var i = 0; i < groupList.length; i++) {
            if (groupList[i].assessmentdata
                && groupList[i].assessmentdata[performanceTaskId]
                && groupList[i].assessmentdata[performanceTaskId][day]
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId]
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId] != null
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value != null
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value.length > 0) {

                if ((assessmentType !== 'yesnolist' && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value.indexOf(FLAG_VAL) != -1)
                    || (assessmentType === 'yesnolist' && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation;}).indexOf(FLAG_VAL) != -1)) {
                    flaggedList.push(groupList[i]);
                } else {
                    asssessedList.push(groupList[i]);
                }
            } else {
                notCompleteList.push(groupList[i]);
            }
        }

        if (type == COMPLETE_VAL) {
            return asssessedList.concat(flaggedList.concat(notCompleteList));
        }
        return notCompleteList.concat(flaggedList.concat(asssessedList));
    }

    return groupList;
}

// Function to sort an array by the marked flagged status
sortByFlagged = function (groupList) {
    if (Session.get("selected_performancetask") != null
        && Session.get('selected_day') != null
        && Session.get("selected_assessment") != null) {
        var flaggedList = [],
            notFlaggedList = [],
            performanceTaskId = Session.get("selected_performancetask")._id,
            day = Session.get('selected_day').day,
            assessmentId = Session.get("selected_assessment")._id,
            assessmentType = Session.get("selected_assessment").type;

        for (var i = 0; i < groupList.length; i++) {
            if (groupList[i].assessmentdata
                && groupList[i].assessmentdata[performanceTaskId]
                && groupList[i].assessmentdata[performanceTaskId][day]
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId]
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId] != null
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value != null
                && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value.length > 0
                && ((assessmentType !== 'yesnolist' && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value.indexOf(FLAG_VAL) != -1)
                || (assessmentType === 'yesnolist' && groupList[i].assessmentdata[performanceTaskId][day][assessmentId].value.map(function(e) { return e.observation;}).indexOf(FLAG_VAL) != -1))) {
                flaggedList.push(groupList[i]);
            } else {
                notFlaggedList.push(groupList[i]);
            }
        }

        return flaggedList.concat(notFlaggedList);
    }

    return groupList;
}

// Function to sort an array based an the order of another array
sortArrayByArray = function (sortingArray, arrayToSort, key) {
    var newSortedArray = new Array();

    for (i = 0; i < sortingArray.length; i++) {
        var sortKey = sortingArray[i],
            pos = arrayToSort.map(function(e) { return e[key]; }).indexOf(sortKey);

        if (pos != -1) {
            newSortedArray.push(arrayToSort[pos]);
        }
    }

    return newSortedArray;
}

Template.students.events({
    // User clicks to sort the student list alphabetically
    'click #alpha-sort': function () {
        var groupOrderList = Session.get('group_list_ordered'),
            currentGroupOrder = groupOrderList[Session.get("selected_group")._id],
            alphaList = sortByKey(currentGroupOrder, "lastname");

        groupOrderList[Session.get("selected_group")._id] = alphaList;

        Session.set('group_list_ordered', groupOrderList);
    },
    // User clicks to sort the student list by those that are marked complete first
    'click #complete-sort': function () {
        var groupOrderList = Session.get('group_list_ordered'),
            currentGroupOrder = groupOrderList[Session.get("selected_group")._id],
            alphaList = sortByKey(currentGroupOrder, "lastname"),
            completeList = sortByComplete(alphaList, COMPLETE_VAL);

        groupOrderList[Session.get("selected_group")._id] = completeList;

        Session.set('group_list_ordered', groupOrderList);
    },
    // User clicks to sort the student list by those that are not marked complete first
    'click #not-complete-sort': function () {
        var groupOrderList = Session.get('group_list_ordered'),
            currentGroupOrder = groupOrderList[Session.get("selected_group")._id],
            alphaList = sortByKey(currentGroupOrder, "lastname"),
            completeList = sortByComplete(alphaList, NOT_COMPLETE_VAL);

        groupOrderList[Session.get("selected_group")._id] = completeList;

        Session.set('group_list_ordered', groupOrderList);
    },
    // User clicks to sort the student list by those that are marked flagged first
    'click #flag-sort': function () {
        var groupOrderList = Session.get('group_list_ordered'),
            currentGroupOrder = groupOrderList[Session.get("selected_group")._id],
            alphaList = sortByKey(currentGroupOrder, "lastname"),
            flaggedList = sortByFlagged(alphaList);

        groupOrderList[Session.get("selected_group")._id] = flaggedList;

        Session.set('group_list_ordered', groupOrderList);
    },
    // User clicks to sort the student list by the user's custom sort for the group
    // Note: Only groups not classrooms have a custom sort that is set-up on the
    // Admin->Groups area
    'click #my-custom-sort': function () {
        var  groupOrderList = Session.get('group_list_ordered'),
            studentList =  Students.find({ _id: { $in: Session.get("selected_group").students } }).fetch(),
            sortingArr = Session.get("selected_group").students;

        groupOrderList[Session.get("selected_group")._id] = sortArrayByArray(sortingArr, studentList, "_id");

        Session.set('group_list_ordered', groupOrderList);
    },
    // User clicks to deselect any selected students in the student list
    'click #deselect-all': function () {
        // Deselect all selected students
        Session.set('selected_students', null);
        Session.set('view_value', null);
    },
    // User clicks to select all students in the student list
    'click #select-all': function () {
        // Select all students
        Session.set('selected_students', Session.get('group_list_ordered')[Session.get("selected_group")._id]);
        Session.set('view_value', null);
    },
    // Mark selected students flagged (data needs to be collected)
    'click #flag': function (evt, template)  {
        if (Session.get("selected_students") !== null
            && Session.get("selected_performancetask") !== null
            && Session.get('selected_day') !== null
            && Session.get("selected_assessment") !== null) {
            saveAssessmentData(Session.get("selected_assessment").type, FLAG_VAL, "yes");
        }
    },
    // User clicks on a selected student.  It will select
    // the student if they are not yet selected or deselect them
    // if they are already selected.  A growl message appears
    // if more than one student is selected and all selected
    // students do not have matching assessment data values
    'click #student-item': function (evt, template) {
        var selectedStudents = Session.get('selected_students');

        if (selectedStudents == null) {
            selectedStudents = [];
        }

        var studentIdx = selectedStudents.map(function(e) { return e._id; }).indexOf(this._id);

        if (studentIdx == -1) {
            selectedStudents.push(this);
        } else {
            selectedStudents.splice(studentIdx, 1);
        }

        // Set student being selected when a student is clicked on
        Session.set('selected_students', selectedStudents);
        Session.set('view_value', null);

        if (alertDataCollision()) {
            if ($('.bootstrap-growl')) {
                $('.bootstrap-growl').removeClass('bootstrap-growl');
            }

            $.bootstrapGrowl("<b>Student information cannot be displayed.</b><br/>You have selected more than one student whose information is not the same.", {
                type: 'warning',
                align: 'center',
                width: 'auto',
                allow_dismiss: true,
                delay: 5000
            });
        }
    }
});