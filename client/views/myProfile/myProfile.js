
////////// Variables //////////
Session.set('editing_field', null);



/** userView template **/

////////// Helpers //////////

Template.userInfo.helpers({
    // Returns record for the currently logged in user
    userData: function () {
        return Meteor.users.findOne({_id: Meteor.userId()});
    },
    // Returns the user's email address
    userEmail: function () {
        return getUserEmail();
    },
    // Returns whether or not first name is currently being edited
    editingFirstName: function () {
        return Session.get('editing_field') === "firstName";
    },
    // Returns whether or not last name is currently being edited
    editingLastName: function () {
        return Session.get('editing_field') === "lastName";
    },
    // Returns whether or not email is currently being edited
    editingEmail: function () {
        return Session.get('editing_field') === "email";
    },
    // Returns the number of classroom groups shared with the user
    numberOfSharedClassroomGroups: function () {
        var currentUser = Meteor.user();

        if (currentUser.profile.sharedgroups) {
            return currentUser.profile.sharedgroups.length || 0;
        }

        return 0;
    }
});

////////// Events //////////

Template.userInfo.events({
    // User clicks button to edit their first name
    'click #edit-user-details-firstname': function () {
        Session.set('editing_field', "firstName");
    },
    // User clicks button to cancel editing their first name
    'click #cancel-user-details-firstname': function () {
        Session.set('editing_field', null);
    },
    // Update user's first name
    'click #update-user-details-firstname': function (evt, template) {

        // Check if user changed email address
        if (template.find('input[name=txtEditFirstName]').value.trim() != getUserFirstName()) {

            // Validate email updates server side
            Meteor.call("updateFirstName", Meteor.user(), template.find('input[name=txtEditFirstName]').value.trim(), function (error, result) {

                // There was a Meteor error
                if (error) {
                    showBootstrapAlert("danger", error.reason);
                } else {

                    // Email format is invalid
                    if (result == "invalidLastNameFormat") {
                        showBootstrapAlert("danger", "The first name you entered is invalid.");
                    } else if (result == "blankInput") {
                        showBootstrapAlert("danger", "Your first name cannot be blank.");
                    } else if (result === "invalidChar") {
                        showBootstrapAlert("danger", "Your first name should only contain letters.");
                    } else {
                        Session.set('editing_field', null);
                        showBootstrapAlert("success", "Your first name was updated successfully.");
                    }
                }
            });
        } else {
            Session.set('editing_field', null);
        }
    },
    // User clicks button to edit their last name
    'click #edit-user-details-lastname': function () {
        Session.set('editing_field', "lastName");
    },
    // User clicks button to cancel editing their last name
    'click #cancel-user-details-lastname': function () {
        Session.set('editing_field', null);
    },
    // Update user's last name
    'click #update-user-details-lastname': function (evt, template) {

        // Check if user changed email address
        if (template.find('input[name=txtEditLastName]').value.trim() != getUserLastName()) {

            // Validate email updates server side
            Meteor.call("updateLastName", Meteor.user(), template.find('input[name=txtEditLastName]').value.trim(), function (error, result) {

                // There was a Meteor error
                if (error) {
                    showBootstrapAlert("danger", error.reason);
                } else {

                    // Email format is invalid
                    if (result == "invalidLastNameFormat") {
                        showBootstrapAlert("danger", "The last name you entered is invalid.");
                    } else if (result == "blankInput") {
                        showBootstrapAlert("danger", "Your last name cannot be blank.");
                    } else if (result === "invalidChar") {
                        showBootstrapAlert("danger", "Your last name should only contain letters.");
                    } else {
                        Session.set('editing_field', null);
                        showBootstrapAlert("success", "Your last name was updated successfully.");
                    }
                }
            });
        } else {
            Session.set('editing_field', null);
        }
    },
    // User clicks button to edit their email
    'click #edit-user-details-email': function () {
        Session.set('editing_field', "email");
    },
    // User clicks button to cancel editing their email
    'click #cancel-user-details-email': function () {
        Session.set('editing_field', null);
    },
    // Update user's email
    'click #update-user-details-email': function (evt, template) {

        // Check if user changed email address
        if (template.find('input[name=txtEditUserEmail]').value.trim() != getUserEmail()) {

            // Validate email updates server side
            Meteor.call("updateEmail", Meteor.user(), template.find('input[name=txtEditUserEmail]').value.trim(), function (error, result) {

                // There was a Meteor error
                if (error) {
                    showBootstrapAlert("danger", error.reason);
                } else {

                    // Email format is invalid
                    if (result == "invalidEmailFormat") {
                        showBootstrapAlert("danger", "The email address you entered is invalid.");
                    } else if (result == "emailAlreadyTaken") {
                        showBootstrapAlert("danger", "The email address you entered is already in use.");
                    } else if (result == "blankInput") {
                        showBootstrapAlert("danger", "Your email address cannot be blank.");
                    } else {
                        Session.set('editing_field', null);
                        showBootstrapAlert("success", "Your email address was updated successfully.");
                    }
                }
            });
        } else {
            Session.set('editing_field', null);
        }
    }
});



/** userDetailsResetPasswordModal Template **/

////////// Helpers //////////

Template.userDetailsResetPasswordModal.helpers({
    // Returns the user's email
    userEmail: function () {
        return getUserEmail();
    }
});

////////// Events //////////

Template.userDetailsResetPasswordModal.events({
    // User clicks the button to reset their password
    'submit #change-password-form': function (evt, template) {
        evt.preventDefault();

        var currentPass         = template.find('input[name=changePassCurrentPass]').value,
            newPass             = template.find('input[name=changePassNewPass]').value,
            confirmNewPass      = template.find('input[name=changePassConfirmNewPass]').value;

        // Call server function to update password
        Meteor.call('updatePassword', currentPass, newPass, confirmNewPass, function (error, result) {
            // Handle various error results returned from server
            if (error) {
                showBootstrapAlert("danger", error.reason);

            } else if (result == "emptyFields") {
                showBootstrapAlert("danger", "You must fill out every field.");

            } else if (result == "wrongPassword") {
                showBootstrapAlert("danger", "The old password you entered is incorrect.");
                clearErrorAlerts("old");
                $("#old-password-form-group").addClass("has-error");

            } else if (result == "passwordTooShort") {
                showBootstrapAlert("danger", "Your new password must be at least 7 characters in length.");
                clearErrorAlerts("all");
                $("#new-password-form-group").addClass("has-error");

            } else if (result == "passwordsDoNotMatch") {
                showBootstrapAlert("danger", "Your new passwords do not match.");
                clearErrorAlerts("all");
                $("#new-password-form-group").addClass("has-error");
                $("#confirm-new-password-form-group").addClass("has-error");

            } else if (result == "passwordMustContainLetter") {
                showBootstrapAlert("danger", "Your new password must contain at least one letter.");
                clearErrorAlerts("all");
                $("#new-password-form-group").addClass("has-error");
                $("#confirm-new-password-form-group").addClass("has-error");

            } else {
                clearErrorAlerts("all");
                showBootstrapAlert("success", "Your password has been changed.");
                $("#userDetailsPasswordResetModal").modal("hide");
                clearPasswordChangeFields();
            }
        });

    },
    // User clicks the close button in the password reset modal
    'click #close-password-reset-modal': function () {
        clearPasswordChangeFields();
        clearErrorAlerts("all");

    },
    // User types in the old password field
    'keypress #user-details-update-current-pass': function () {
        clearErrorAlerts("old");

    },
    // User types in the new password field
    'keypress #user-details-update-new-pass': function () {
        clearErrorAlerts("new");

    },
    // User types in the confirm new password field
    'keypress #user-details-update-confirm-new-pass': function () {
        clearErrorAlerts("confirm");

    }
});



/** updateAlertTemplate Template **/

////////// Events //////////

Template.updateAlertTemplate.events({
    // User dismisses the alert
    'click #bootstrap-alert-update-alert-dismiss': function () {
        $("#bootstrap-alert-update-alert").fadeOut();
    }
});



/** JS Functions **/

// Returns the email address of the current user
var getUserEmail = function () {
    return Meteor.user().emails[0].address;
}

// Returns the last name of the current user
var getUserLastName = function () {
    return Meteor.user().profile.lastName;
}

// Returns the first name of the current user
var getUserFirstName = function () {
    return Meteor.user().profile.firstName;
}

// Clears fields in the password change modal
var clearPasswordChangeFields = function () {
    $("#user-details-update-current-pass").val("");
    $("#user-details-update-new-pass").val("");
    $("#user-details-update-confirm-new-pass").val("");
}

// Clear the bootstrap error alert formatting from the password reset form fields
var clearErrorAlerts = function (field) {

    if (field == "all" || field == "old") {
        $("#old-password-form-group").removeClass("has-success");
        $("#old-password-form-group").removeClass("has-error");
    }

    if (field == "all" || field == "new") {
        $("#new-password-form-group").removeClass("has-success");
        $("#new-password-form-group").removeClass("has-error");
    }

    if (field == "all" || field == "confirm") {
        $("#confirm-new-password-form-group").removeClass("has-success");
        $("#confirm-new-password-form-group").removeClass("has-error");
    }
}


/** jQuery **/

// Password reset dismiss event
$(document).on('hide.bs.modal', '#userDetailsPasswordResetModal', function () {
    clearPasswordChangeFields();
    clearErrorAlerts("all");
});