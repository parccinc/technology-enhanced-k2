// Parcc classroom admin -- client

////////// Variables //////////

// Default when add new classroom
var defaultAdd = {
    _id: "",
    name: "",
    grade: "",
    students: []
};

// Default currently selected group
Session.setDefault('admin_selected_group', null);

// Default selected classroom student list
Session.setDefault('admin_selected_classroom', null);

// Default selected classroom students
Session.setDefault('admin_selected_students', null);

// Default selected group student
Session.setDefault('admin_selected_group_student', null);


////////// Helpers for in-place editing //////////

// Returns an event map that handles the "escape" and "return" keys and
// "blur" events on a text input (given by selector) and interprets them
// as "ok" or "cancel".
var okCancelEvents = function (selector, callbacks) {
    var ok = callbacks.ok || function () {};
    var cancel = callbacks.cancel || function () {};

    var events = {};
    events['keyup '+selector+', keydown '+selector+', focusout '+selector] =
        function (evt) {
            if (evt.type === "keydown" && evt.which === 27) {
                // escape = cancel
                cancel.call(this, evt);

            } else if (evt.type === "keyup" && evt.which === 13 ||
                evt.type === "focusout") {
                // blur/return/enter = ok/submit if non-empty
                var value = String(evt.target.value || "");
                if (value)
                    ok.call(this, value, evt);
                else
                    cancel.call(this, evt);
            }
        };

    return events;
};


/*** Template: groups ***/

////////// UI Display //////////

Template.groups.helpers({
    // Display the groups associated with the logged in teacher.
    groups: function () {
        if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.groups) {
            return Meteor.user().profile.groups;
        }

        return [];
    },

    // Sets active group
    isActiveGroup: function () {
        return Session.get("admin_selected_group") !== null && Session.get("admin_selected_group")._id === this._id ? "active" : "";
    },

    // Indicate if a group is selected
    groupIsSelected: function () {
        return Session.get("admin_selected_group") !== null;
    }
});

////////// Events //////////

Template.groups.events({
    // Add a new group
    'click #add-group': function (evt, template) {
        // Default group when adding a new one
        Session.set('admin_selected_group', defaultAdd);
    },
    // Sets the group the user has selected from the group menu list
    'click #select-group-item': function (evt, template) {
        evt.stopPropagation();

        // Set group being selected when a group is clicked on
        Session.set('admin_selected_group', this);

        // Clear out selected classroom in case there are any selected
        Session.set('admin_selected_classroom', null);
    },
    // Deletes a selected group
    'click #remove-group': function () {
        var deleteGroup = this,
            deletedId = this._id;

        bootbox.confirm("Are you sure you want to delete group, " + deleteGroup.name + "?", function(result) {
            if (result === true) {
                // Remove group
                Meteor.call('removeUserGroup', deleteGroup, function(err, group) {
                    Session.set("admin_selected_group", null);

                    if (deletedId === Session.get("selected_group")._id) {
                        Session.set("selected_group", null);
                    }
                });

                console.log("Remove User Group");
            }

            return;
        });
    }
});



/*** Template: groupedStudents ***/

////////// UI Display //////////

Template.groupedStudents.helpers({
    // Determine if form is in edit mode
    isGroupEditable: function () {
        return Session.get("admin_selected_group") !== null && Session.get("admin_selected_group")._id.trim().length > 0;
    },

    // Get the list of classrooms the user can see: their own + shared groups for the selected group's grade
    getClassrooms: function () {
        var classData = [];

        if (Session.get("admin_selected_group") !== null && Meteor.user() && Meteor.user().profile) {
            // Classrooms
            if (Meteor.user().profile.classrooms) {
                $.merge(classData, $.grep(Meteor.user().profile.classrooms, function(e){ return e.grade === Session.get('admin_selected_group').grade; }));
            }

            // Shared Groups
            if (Meteor.user().profile.sharedgroups) {
                for (var i = 0; i < Meteor.user().profile.sharedgroups.length; i++) {
                    var user = Meteor.users.findOne({ 'profile.groups': { $elemMatch: { _id: Meteor.user().profile.sharedgroups[i]._id } } });

                    if (user) {
                        var sharedGroup = $.grep(user.profile.groups, function(e){ return e._id === Meteor.user().profile.sharedgroups[i]._id && e.grade === Session.get('admin_selected_group').grade; });
                        $.merge(classData, sharedGroup);
                    }
                }
            }
        }

        return classData;
    },

    // Get selected classroom data
    selectedClassroomStudents: function () {
        return Session.get('admin_selected_classroom') !== null ? Session.get('admin_selected_classroom').students : [];
    },

    // Determine if student is a selected student
    isSelectedStudent: function () {
        return Session.get('admin_selected_students') != null && Session.get('admin_selected_students').map(function(e) { return e._id; }).indexOf(this._id) != -1;
    },


    // Get selected group data
    selectedGroup: function () {
        return Session.get("admin_selected_group");
    },

    // Get student data based on student id in the list
    joinWithStudents: function () {
        var groupStudent = this.toString(),
            student = Students.findOne({_id: groupStudent});

        return student;
    },

    // Determines if a student in the list has been selected
    isSelectedGroupStudent: function () {
        return Session.get('admin_selected_group_student') !== null && Session.get('admin_selected_group_student')._id === this._id;
    },

    // Get the grade of the selected group
    isGroupGradeSelected: function (grade) {
        return Session.get("admin_selected_group") !== null && Session.get("admin_selected_group").grade === grade ? 'selected' : '';
    },

    // Determine if the user can select a group grade
    isGroupGradeEnabled: function () {
        return Session.get("admin_selected_group") !== null && Session.get("admin_selected_group")._id.trim().length > 0 ? "disabled" : '';
    }
});

////////// Events //////////

Template.groupedStudents.events({
    // User selects a classroom from the classroom dropdown list and the selected classroom is saved in session
    'change #select-classroom': function (evt) {
        var classroomId = $(evt.target).val(),
            selectedClassroom = null;

        if (Meteor.user().profile.classrooms) {
            var classroomIdx = Meteor.user().profile.classrooms.map(function(e) { return e._id; }).indexOf(classroomId);

            if (classroomIdx !== -1) {
                selectedClassroom = Meteor.user().profile.classrooms[classroomIdx];

                var studentList = Students.find({classroom_id: classroomId}, {sort: {lastname: 1, firstname: 1}}).fetch();
                selectedClassroom.students = studentList;
            }
        }

        if (selectedClassroom === null && Meteor.user().profile.sharedgroups) {
            var groupIdx = Meteor.user().profile.sharedgroups.map(function(e) { return e._id; }).indexOf(classroomId);

            if (groupIdx !== -1) {
                var user = Meteor.users.findOne({ 'profile.groups': { $elemMatch: { _id: classroomId } } });

                if (user) {
                    selectedClassroom = $.grep(user.profile.groups, function(e){ return e._id === classroomId; })[0];

                    if (selectedClassroom.students) {
                        var studentsList = [];

                        for (var i = 0; i < selectedClassroom.students.length; i++) {
                            var findStudent = Students.findOne({_id: selectedClassroom.students[i].toString()});

                            if (findStudent) {
                                studentsList.push(findStudent);
                            }
                        }

                        selectedClassroom.students = studentsList;
                    }
                }
            }
        }

        Session.set('admin_selected_classroom', selectedClassroom);
    },

    // User selects a student from the classroom list and the selected students are stored in session
    'click #select-student': function () {
        var selectedStudents = Session.get('admin_selected_students') != null ? Session.get('admin_selected_students') : [],
            studentPos = selectedStudents.map(function(e) { return e._id; }).indexOf(this._id);

        if (studentPos != -1) {
            selectedStudents.splice(studentPos, 1);
        } else {
            selectedStudents.push(this);
        }

        Session.set('admin_selected_students', selectedStudents);
        Session.set('admin_selected_group_student', null);
    },

    // All students from the classroom list are to be saved in session as selected
    'click #select-all-students': function () {
        if (Session.get('admin_selected_classroom') != null) {
            Session.set('admin_selected_students', Session.get('admin_selected_classroom').students);
        }
    },

    // No students from the classroom list are to be saved in session as selected
    'click #select-no-students': function () {
        Session.set('admin_selected_students', null);
    },

    // User selects a student from the group list and the selected students are stored in session
    'click #select-group-student': function () {
        Session.set('admin_selected_group_student', this);
        Session.set('admin_selected_students', null);
    },

    // User clicks the add selected students button from the classroom list to move over any selected classroom
    // students to the selected group list
    'click #add-selected-students': function () {
        if (Session.get('admin_selected_group') != null && Session.get('admin_selected_students') != null) {
            var selectedStudents = Session.get('admin_selected_students'),
                group = Session.get('admin_selected_group'),
                studentsList = Session.get('admin_selected_group').students,
                duplicatesFound = false,
                addRecords = true;

            if (studentsList === null) {
                studentsList = [];
            } else {
                for (var i = 0; i < selectedStudents.length; i++) {
                    if ($.grep(studentsList, function(e){ return e === selectedStudents[i]._id; }).length > 0) {
                        duplicatesFound = true;
                        break;
                    }
                }

                if (duplicatesFound) {
                    addRecords = false;
                    //alert("You have selected a student that already exists in the group.  Please remove the duplicate student from your selection.");
                    bootbox.alert("You have selected a student that already exists in the group.  Please remove the duplicate student from your selection.", function() {});
                }
            }

            if (addRecords) {
                group.students = $.merge(studentsList, selectedStudents.map(function(e) { return e._id; }));

                Session.set('admin_selected_group', group);
                Session.set('admin_selected_students', null);
            }
        }
    },

    // Move selected group student down one array index position
    'click #move-selected-up': function () {
        if (Session.get('admin_selected_group') != null && Session.get('admin_selected_group_student') != null) {
            var selectedStudent = Session.get('admin_selected_group_student'),
                group = Session.get('admin_selected_group'),
                studentsList = Session.get('admin_selected_group').students,
                studentPos = studentsList.indexOf(selectedStudent._id);

            if (studentPos != 0) {
                studentsList.splice(studentPos-1, 0, studentsList.splice(studentPos, 1)[0] );
                group.students = studentsList;
                Session.set('admin_selected_group', group);
            }
        }
    },

    // Move selected group student up one array index position
    'click #move-selected-down': function () {
        if (Session.get('admin_selected_group') != null && Session.get('admin_selected_group_student') != null) {
            var selectedStudent = Session.get('admin_selected_group_student'),
                group = Session.get('admin_selected_group'),
                studentsList = Session.get('admin_selected_group').students,
                studentPos = studentsList.indexOf(selectedStudent._id);

            if (studentPos != studentsList.length-1) {
                studentsList.splice(studentPos+1, 0, studentsList.splice(studentPos, 1)[0] );
                group.students = studentsList;
                Session.set('admin_selected_group', group);
            }
        }
    },

    // Save any group changes including student lists and group name
    'click #save-group': function (evt, template) {
        evt.preventDefault();

        var groupData = Session.get('admin_selected_group');

        groupData.name = template.find('input[name=groupName]').value;
        groupData.grade = template.find('select[name=groupGrade]').value;

        if (groupData._id.trim().length === 0) {
            // Add group
            groupData._id = new Meteor.Collection.ObjectID()._str;

            Meteor.call('addUserGroup', groupData, function(err, group) {
                if (err) {
                    showBootstrapAlert("danger", err.reason);
                    return;
                }

                if (group === "blankValue") {
                    showBootstrapAlert("danger", "Please enter a group name and select a grade.");
                    return;
                } else if (group) {
                    Session.set('admin_selected_group', groupData);
                    Session.set('admin_selected_classroom', null);
                    return;
                } else {
                    showBootstrapAlert("danger", "\"" + groupData.name.trim() + "\" is already in use.");
                    return;
                }
            });
        } else {
            // Update group
            Meteor.call('editUserGroup', groupData, function(err, group) {
                Session.set('admin_selected_group', null);
                Session.set('admin_selected_group_student', null);
                Session.set('admin_selected_classroom', null);
                Session.set('admin_selected_students', null);
            });
        }
    },

    // Remove a selected student from the group list
    'click #remove-group-student': function (evt, template) {
        evt.stopPropagation();
        console.log(this._id);
        console.log(Session.get('admin_selected_group'));
        var deletedId = this._id;

        bootbox.confirm("Are you sure you want to remove "+ this.firstname + " "+ this.lastname +" from group?", function(result) {
            if (result === true) {
                // Remove group
                var group = Session.get('admin_selected_group'),
                    newGroup = [],
                    i = 0;
                //need to store the context of this since scope changes in .map
                //newGroup = group.students.filter(function(e) { return e._id !== this._id}.bind(this));
                for (i; i < group.students.length; i++) {
                    console.log(group.students[i]);
                    if (group.students[i] === deletedId){
                        console.log('equals');
                        group.students.splice(i, 1);
                    }
                }
                console.log(group.students);
                Session.set('admin_selected_group', group);
            }
        });
    },

    // User clicks the share group button and is taken to the share modal
    'click #share-group': function () {
        $('#shareGroupModal').modal("show");
    }
});

/*** Template: sharedInfo ***/

////////// UI Display //////////

Template.sharedInfo.helpers({
    // Get the names of all users the selected group is shared with
    sharedUsers: function () {
        if (Session.get("admin_selected_group") != null) {

            var _qry = {
                    'profile.sharedgroups': { $elemMatch: { _id: Session.get("admin_selected_group")._id } }
                },
                sharedGroups = Meteor.users.find(_qry).fetch();

            for (var i = 0; i < sharedGroups.length; i++) {
                if (sharedGroups[i]._id === Meteor.userId()) {
                    var user = Meteor.users.findOne({ 'profile.groups': { $elemMatch: { _id: Session.get("admin_selected_group")._id } } });

                    if (user) {
                        sharedGroups[i].sharedName = user.profile.firstName + " " + user.profile.lastName;
                        sharedGroups[i].allowDelete = false;
                    }
                } else {
                    sharedGroups[i].sharedName = sharedGroups[i].profile.firstName + " " + sharedGroups[i].profile.lastName;
                    sharedGroups[i].allowDelete = true;
                }
            }

            return sharedGroups;
        }

        return [];
    }
});


////////// Events //////////

Template.sharedInfo.events({
    // User clicks on the delete icon to delete a student
    'click #remove-group-share': function () {
        var deleteShare = this;

        bootbox.confirm("Are you sure you want to remove the ability for " + deleteShare.profile.firstName + " " + deleteShare.profile.lastName + " to view this group?", function(result) {
            if (result === true) {
                // Remove Group share
                Meteor.call('removeSharedGroup', deleteShare._id, Session.get("admin_selected_group")._id);

                console.log("Remove Shared Group");
            }

            return;
        });
    }
});

/*** Template: shareGroupModal ***/

////////// Events //////////

Template.shareGroupModal.events({
    // User clicks the share group save button and if a user with the entered email address is found then
    // the classroom is added to that users sharedgroup list otherwise a message appears letting the
    // user know that a user with the email address entered could not be found.
    'click #save-share-group': function (evt, template) {
        Meteor.call('getUserByEmail', template.find('input[name=userEmail]').value, function (err, user) {
            if (user != null) {
                var userId = user._id;

                Meteor.call('addSharedUserGroup', userId, Session.get("admin_selected_group")._id, function(err, group) {
                    if (err) {
                        showBootstrapAlert("danger", err.reason);
                        return;
                    }

                    if (group === "groupExists") {
                        showBootstrapAlert("danger", "The group is already shared with this user.");
                        $('#shareGroupModal').modal("hide");
                    } else if (group) {

                        template.find('input[name=userEmail]').value = "";
                        $('#shareGroupModal').modal("hide");
                    }
                });
            } else {
                $.bootstrapGrowl("You have not entered an email address belonging to another user in this application.  Please try again.", {
                    type: 'warning',
                    align: 'center',
                    width: 'auto',
                    allow_dismiss: true,
                    delay: 5000
                });
            }
        });
    }
});
