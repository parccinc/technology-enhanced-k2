// Parcc assessment admin -- client

////////// Variables //////////

// Default when add new assessment
var rubricDefaultAdd = {grade: "K",
    subject: "ela",
    type: "rubric",
    name: "",
    displayName: "",
    assessValues: [{_id: 1, value: "Meeting", description: ""},
        {_id: 3, value: "Developing", description: ""},
        {_id: 4, value: "Emerging", description: ""}],
    commonCoreStateStandards: []};


// Default currently selected admin assessment
Session.setDefault('selected_admin_assessment', null);

// Default current filters
Session.setDefault('assessment_filters', {grade: "K", subject: "ela"});


/*** Template: assessment ***/

////////// UI Display //////////

Template.assessment.helpers({
    // Get selected assessment data
    selectedAssessment: function () {
        return Session.get("selected_admin_assessment");
    },

    // Determine if should show assessment form
    showAssessmentForm: function () {
        return Session.get("selected_admin_assessment") != null;
    }

});


/*** Template: assessmentFilter ***/

////////// UI Display //////////

Template.assessmentFilter.helpers({
    isGradeFilterSelected: function (grade) {
        return Session.get("assessment_filters") != null && Session.get("assessment_filters").grade == grade;
    },

    isSubjectFilterChecked: function (subject) {
        return Session.get("assessment_filters") != null && Session.get("assessment_filters").subject == subject;
    }
});



////////// Events //////////

Template.assessmentFilter.events({
    'click #filter-assessments': function (evt, template) {
        var filterGrade = template.find('select[name=filterGrade]').value,
            filterSubject = template.find('input:radio[name=filterSubject]:checked').value;


        Session.set('assessment_filters', {grade: filterGrade, subject: filterSubject});
        Session.set('selected_admin_assessment', null);
    }
});



/*** Template: assessmentList ***/

////////// UI Display //////////

Template.assessmentList.helpers({
    // Get the list of assessments
    assessments: function () {
        return Assessments.find(Session.get("assessment_filters"), {sort: {name: 1}});
    },

    // Sets active assessment
    isActiveAssessment: function () {
        return Session.get("selected_admin_assessment") != null && Session.get("selected_admin_assessment")._id == this._id ? "active" : "";
    }
});


////////// Events //////////

Template.assessmentList.events({
    'click #select-assessment': function (evt, template) {
        evt.stopPropagation();

        // Set the assessment
        Session.set('selected_admin_assessment', this);
    },
    'click #add-assessment': function () {
        Session.set('selected_admin_assessment', rubricDefaultAdd);
    },
    'click #delete-assessment': function (e) {
        e.preventDefault();
        var assesssment = this;

        bootbox.confirm("Are you sure you want to delete collection tool, " + assesssment.name + "?", function(result) {
            if (result === true) {
                // Remove assessment
                Meteor.call('removeAssessment', assesssment._id);

                console.log("Remove Assessment");
            }
        });
    }
});


/*** Template: assessmentForm ***/

////////// UI Display //////////

Template.assessmentForm.helpers({
    // Set the form mode (add or edit)
    assessmentMode: function () {
        return this != null && this._id != null ? "Edit" : "Add";
    },

    isGradeSelected: function (grade) {
        return this != null && this.grade == grade;
    },

    isSubjectChecked: function (subject) {
        return this != null && this.subject == subject;
    },

    isTypeChecked: function (type) {
        return this != null && this.type == type;
    },

    // Get List of common core standards
    getCommonCoreStandards: function () {
        return CommonCoreStateStandards.find(Session.get("assessment_filters"), {sort: {name: 1}});
    },

    // Determine if the selected assessment type uses a description value and should be displayed
    isRubric: function (type) {
        return type && type === 'rubric';
    }
});

////////// Events //////////

Template.assessmentForm.events({
    'change #select-grade': function (evt) {
        // Update the common core filter with the grade selected
        var newValue = $(evt.target).val(),
            oldValue = Session.get("assessment_filters").grade;

        if (newValue != oldValue) {
            Session.set("assessment_filters", {grade: newValue, subject: Session.get("assessment_filters").subject});
        }
    },
    'change #radio-subject': function (evt) {
        // Update the common core filter with the subject selected
        var newValue = $(evt.target).val(),
            oldValue = Session.get("assessment_filters").subject;

        if (newValue != oldValue) {
            console.log(newValue);
            Session.set("assessment_filters", {grade: Session.get("assessment_filters").grade, subject: newValue});
        }
    },
    'click #radio-type-rubric': function (evt, template) {
        var assessment = Session.get("selected_admin_assessment");

        assessment.type = template.find('input:radio[name=type]:checked').value;
        assessment.assessValues = [
            {_id: new Meteor.Collection.ObjectID()._str, value: "Meeting", description: ""},
            {_id: new Meteor.Collection.ObjectID()._str, value: "Developing", description: ""},
            {_id: new Meteor.Collection.ObjectID()._str, value: "Emerging", description: ""}
        ];

        Session.set("selected_admin_assessment", assessment);
    },
    'click #radio-type-checklist': function (evt, template) {
        var assessment = Session.get("selected_admin_assessment");

        assessment.type = template.find('input:radio[name=type]:checked').value;
        assessment.assessValues = [
            {_id: new Meteor.Collection.ObjectID()._str, value: "", description: ""}
        ];

        Session.set("selected_admin_assessment", assessment);
    },
    'click #radio-type-yesnolist': function (evt, template) {
        var assessment = Session.get("selected_admin_assessment");

        assessment.type = template.find('input:radio[name=type]:checked').value;
        assessment.assessValues = [
            {_id: new Meteor.Collection.ObjectID()._str, value: "", description: ""}
        ];

        Session.set("selected_admin_assessment", assessment);
    },
    'click #add-standard': function (evt, template) {
        // Add a common core state standard to the assessment
        var standard = template.find('select[name=standard]').value,
            assessment = Session.get("selected_admin_assessment");

        if (standard) {
            if (!assessment.commonCoreStateStandards) {
                assessment.commonCoreStateStandards = [];
            }

            assessment.commonCoreStateStandards.push({_id: standard});
            Session.set('selected_admin_assessment', assessment);
        }
    },
    'click #delete-standard': function () {
        var deleteId = this._id
        bootbox.confirm("Are you sure you want to delete standard " + this._id + "?", function(result) {
            if (result === true) {
                //Remove a common core state standard from the assessment
                var assessment = Session.get("selected_admin_assessment"),
                    pos = assessment.commonCoreStateStandards.map(function(e) { return e._id; }).indexOf(deleteId);

                assessment.commonCoreStateStandards.splice(pos, 1);
                Session.set('selected_admin_assessment', assessment);
            }
        });
    },
    'click #add-assessmentValue': function () {
        var assessment = Session.get("selected_admin_assessment"),
            newId = new Meteor.Collection.ObjectID()._str;

        assessment.assessValues.push({_id: newId, value: "", description: ""});

        Session.set("selected_admin_assessment", assessment);
    },
    'click #delete-assessmentValue': function () {
        var assesssmentValue = this;

        bootbox.confirm("Are you sure you want to delete collection tool?", function(result) {
            if (result === true) {
                var assessment = Session.get("selected_admin_assessment"),
                    pos = assessment.assessValues.map(function (e) {
                        return e._id;
                    }).indexOf(assesssmentValue._id);

                assessment.assessValues.splice(pos, 1);

                Session.set("selected_admin_assessment", assessment);
            }

            return;
        });
    },
    'submit form': function (evt, template) {
        evt.preventDefault();

        var assessmentData = Session.get("selected_admin_assessment") != null ? Session.get("selected_admin_assessment"): {},
            allAssessmentValues = template.findAll('input[name=assessmentValue]'),
            allAssessmentDescriptions = template.findAll('input[name=assessmentValueDescription]');

        // Update data from form values 
        assessmentData.grade = template.find('select[name=grade]').value;
        assessmentData.subject = template.find('input:radio[name=subject]:checked').value;
        assessmentData.type = template.find('input:radio[name=type]:checked').value;
        assessmentData.name = template.find('input[name=assessmentName]').value;
        assessmentData.displayName = template.find('input[name=displayName]').value;

        // Update assessment values
        for (i = 0; i < assessmentData.assessValues.length; i++) {
            assessmentData.assessValues[i].value = allAssessmentValues[i].value;
            if (allAssessmentDescriptions[i]) {
                assessmentData.assessValues[i].description = allAssessmentDescriptions[i].value;
            } else {
                assessmentData.assessValues[i].description = "";
            }
        }

        if (Session.get("selected_admin_assessment") != null) {
            delete assessmentData._id;

            if (Session.get("selected_admin_assessment")._id != null) {
                Meteor.call('updateAssessment', Session.get("selected_admin_assessment")._id, assessmentData, function (err, assessment) {
                    // LS PAR-133: Add user input validation
                    if (err) {
                        showBootstrapAlert("danger", err.reason);

                    } else {
                        if (assessment == "missingAssessmentData") {
                            showBootstrapAlert("danger", "Please fill out all fields.");

                        } else if (assessment == "blankAssessmentoValues") {
                            showBootstrapAlert("danger", "Please fill out all assessment value fields.");

                        } else if (assessment == "noAssessments") {
                            showBootstrapAlert("danger", "Please add assessment values.");

                        } else if (assessment) {
                            showBootstrapAlert("success", assessmentData.name + " has been updated.");
                            Session.set("selected_admin_assessment", null);

                        } else {
                            showBootstrapAlert("danger", "This assessment could not be updated.");
                        }
                    }
                    // END OF PAR-133
                });

                console.log("Update Assessment");
            } else {
                Meteor.call('addAssessment', assessmentData, function (err, assessment) {
                    // LS PAR-133: Add user input validation
                    if (err) {
                        showBootstrapAlert("danger", err.reason);

                    } else {
                        if (assessment == "missingAssessmentData") {
                            showBootstrapAlert("danger", "Please fill out all fields.");

                        } else if (assessment == "blankAssessmentoValues") {
                            showBootstrapAlert("danger", "Please fill out all assessment value fields.");

                        } else if (assessment == "noAssessments") {
                            showBootstrapAlert("danger", "Please add assessment values.");

                        } else if (assessment) {
                            showBootstrapAlert("success", assessmentData.name + " has been added.");
                            Session.set("selected_admin_assessment", null);

                        } else {
                            showBootstrapAlert("danger", "This assessment could not be added.");
                        }
                    }
                    // END OF PAR-133
                });

                console.log("Add Assessment");
            }
        }
     }
});