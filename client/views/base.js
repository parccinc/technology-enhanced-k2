$(document).ready(function(){
    // Work around for Fast Click library breaking tooltips on hover
    // NOTE: Fixes all tooltips in the application as long as the tooltip sets
    // the data-toggle attribute to "tooltip"
    var current_toggle_active = null;
    $('[data-toggle="tooltip"]').tooltip().each(function(e) {
        $(this).click(function() {
            $(this).tooltip('show');
            if (current_toggle_active) $(current_toggle_active).tooltip('hide');
            current_toggle_active = this;
        });
    });
});