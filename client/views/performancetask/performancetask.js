// Parcc performance task admin -- client

////////// Variables //////////
// Default when add new performance task
var defaultAdd = {
    grade: "K",
    subject: "ela",
    name: "",
    textType: "",
    visual: "",
    books: [],
    performanceTaskPdfDoc: "",
    performanceTaskWordDoc: "",
    performanceTaskExcelDoc: "",
    studentResource: "",
    glossary: "",
    summaryGlance: '',
    commonCoreStateStandards: [],
    assessmentSchedule: [],
    instructions: {
        plan: "",
        overview: "",
        outline: "",
        readySetGo: "",
        steps: "",
        ccssEvidence: "",
        ifThen: ""
    }

};

//
Blaze._allowJavascriptUrls();

// Default selected performance task
Session.setDefault('selected_admin_performance_task', null);

// Default current filters for the session
Session.setDefault('performance_task_filters', {grade: "K", subject: "ela"});

// Default common core standards filter for the session
Session.setDefault('standards_filters', {grade: "K", subject: "ela"});

// Default displayed assessment
Session.setDefault('display_assessment', null);

/*** Template: performancetask ***/

////////// UI Display //////////

Template.performancetask.helpers({
    selectedPerformanceTask: function () {
        return Session.get("selected_admin_performance_task");
    },

    // Determine if should show performance task form
    showPerformanceTaskForm: function () {
        return Session.get("selected_admin_performance_task") != null;
    }
});

Template.performancetask.onDestroyed(function(){
   removeAllCKs();
});

/*** Template: performanceTaskFilter ***/

////////// UI Display //////////

Template.performanceTaskFilter.helpers({
    isGradeFilterSelected: function (grade) {
        return Session.get("performance_task_filters") != null && Session.get("performance_task_filters").grade == grade;
    },

    isSubjectFilterChecked: function (subject) {
        return Session.get("performance_task_filters") !== null && Session.get("performance_task_filters").subject == subject;
    }
});


////////// Events //////////

Template.performanceTaskFilter.events({
    'click #filter-performance-tasks': function (evt, template) {
        // Filter the performance tasks by grade and subject
        var filterGrade = template.find('select[name=filterGrade]').value,
            filterSubject = template.find('input:radio[name=filterSubject]:checked').value;

        Session.set('performance_task_filters', {grade: filterGrade, subject: filterSubject});
        Session.set('selected_admin_performance_task', null);
    }
});




/*** Template: performanceTaskList ***/

////////// UI Display //////////

Template.performanceTaskList.helpers({
    // Get the list of performance tasks
    performanceTasks: function () {
        return PerformanceTasks.find(Session.get("performance_task_filters"), {sort: {name: 1}});
    },

    // Sets active performance task
    isActivePerformanceTask: function () {
        return Session.get('selected_admin_performance_task') != null && Session.get('selected_admin_performance_task')._id == this._id  ? "active" : "";
    }

});


////////// Events //////////

Template.performanceTaskList.events({
    'click #select-performance-task': function (evt, template) {
        evt.preventDefault();
        removeAllCKs();
        $(".panel-collapse.in").collapse('hide');

        // Set the performance task
        Session.set('selected_admin_performance_task', this);

        // Default the filter for the standards form dropdown based
        // on the performance task data
        Session.set("standards_filters", {
            grade: this.grade,
            subject: this.subject
        });
    },
    'click #add-performance-task': function () {
        // Default performance task when adding a new one
        var pt = defaultAdd;
        removeAllCKs();

        if (Session.get("performance_task_filters") !== null) {
            pt.grade = Session.get("performance_task_filters").grade;
            pt.subject = Session.get("performance_task_filters").subject;
        }

        Session.set('selected_admin_performance_task', pt);

        // Default the filter for the standards form dropdown based
        // on the performance task defaults
        Session.set("standards_filters", {
            grade: Session.get('selected_admin_performance_task').grade,
            subject: Session.get('selected_admin_performance_task').subject
        });
    },
    'click #delete-performance-task': function () {
        var deletePT = this;

        bootbox.confirm("Are you sure you want to delete the performance task, " + deletePT.name + "?", function(result) {
            if (result === true) {
                // Remove performance task
                Meteor.call('removePerformanceTask', deletePT._id);
                Session.set('selected_admin_performance_task', null);
            }

            return;
        });
    }
});




/*** Template: performanceTaskForm ***/

Template.performanceTaskForm.rendered = function() {
    // Logic for sorting assessment data via drag and drop
    $('#assessment-group-accordion').collapse().sortable({
        stop: function(event, ui) {
           var newDaysOrderList = [],
                selectedTask = Session.get('selected_admin_performance_task'),
                daysOrderList = $.extend(true, [], selectedTask.assessmentSchedule);

            $(".assessment-day-item").each(function(i, el){
                var _day = $(el).data('day');
                newDaysOrderList.push(daysOrderList[daysOrderList.map(function(e) { return e.day; }).indexOf(_day.toString())]);
            });

            selectedTask.assessmentSchedule = newDaysOrderList;
            Session.set('selected_admin_performance_task', selectedTask);
        }
    });
};

////////// UI Display //////////

Template.performanceTaskForm.helpers({
    // Get the list of performance tasks
    performanceTasks: function () {
        return PerformanceTasks.find(Session.get("performance_task_filters"), {sort: {name: 1}});
    },

    // Sets active performance task
    isActivePerformanceTask: function () {
        return Session.get('selected_admin_performance_task') != null && Session.get('selected_admin_performance_task')._id == this._id  ? "active" : "";
    },

    // Set the form mode (add or edit)
    performanceTaskMode: function () {
        return this != null && this._id != null ? "Edit" : "Add";
    },

    isGradeSelected: function (grade) {
        return this != null && this.grade == grade;
    },

    isSubjectChecked: function (subject) {
        return this != null && this.subject == subject;
    },

    isTextTypeChecked: function (type) {
        return this != null && this.textType == type;
    },

    // Get List of common core standards
    getCommonCoreStandards: function () {
        return CommonCoreStateStandards.find(Session.get("standards_filters"), {sort: {name: 1}});
    },

    // Get List of assessments
    getAssessments: function () {
        return Assessments.find(Session.get("standards_filters"), {sort: {name: 1}});
    },

    getReferenceDays: function () {
        var days = [];

        for (var i = 1; i <= 10; i++) {
            if (Session.get('selected_admin_performance_task').assessmentSchedule.map(function(e) { return e.day; }).indexOf(i) == -1) {
                days.push({day: i})
            }
        }

        return days;
    },

    getSubjectDayLabel: function () {
        if (Session.get("standards_filters") != null) {
            return Session.get("standards_filters").subject == 'ela' ? "Day" : "Part";
        }

        return "";
    },

    joinWithAssessments: function () {
        var assessment = this;

        if (this.stepType == "instruction") {
            assessment.name = "Teacher Instruction";
            return assessment;
        } else {
            var assessments = Assessments.findOne({_id: assessment.assessment_id});
            return  _.extend(assessment, _.omit(assessments, '_id'));
        }
    },

    joinWithAssessmentValues: function (assessmentId) {
        var assessmentValue = this,
            assessments = Assessments.findOne({_id: assessmentId}),
            assessmentValueIdx = 0;

        if (assessments && assessments.assessValues) {
            assessmentValueIdx = assessments.assessValues.map(function(e) { return e._id; }).indexOf(assessmentValue._id);
            return  _.extend(assessmentValue, _.omit(assessments.assessValues[assessmentValueIdx], '_id'));
        }

        return assessmentValue;
    },

    isSubjectEla: function () {
        if (Session.get("standards_filters") != null) {
            return Session.get("standards_filters").subject == 'ela';
        }

        return false;
    },

    isStepTypeChecked: function (type) {
        return this != null && this.stepType == type;
    },

    isStepInstruction: function () {
        return this != null && this.stepType == "instruction";
    },

    isAssessmentMoveUpDisabled: function (day) {
        if (Session.get('selected_admin_performance_task') != null && Session.get('selected_admin_performance_task').assessmentSchedule != null) {
            var scheduleIdx = Session.get('selected_admin_performance_task').assessmentSchedule.map(function(e) { return e.day; }).indexOf(day);

            if (Session.get('selected_admin_performance_task').assessmentSchedule[scheduleIdx] != null
                    && Session.get('selected_admin_performance_task').assessmentSchedule[scheduleIdx].assessments != null) {
                var currentAssessmentIdx = Session.get('selected_admin_performance_task').assessmentSchedule[scheduleIdx].assessments.map(function(e) { return e._id; }).indexOf(this._id);

                return currentAssessmentIdx != null && currentAssessmentIdx > 0 ? "" : "disabled";
            }
        }

        return "disabled";
    },

    isAssessmentMoveDownDisabled: function (day) {
        if (Session.get('selected_admin_performance_task') != null && Session.get('selected_admin_performance_task').assessmentSchedule != null) {
            var scheduleIdx = Session.get('selected_admin_performance_task').assessmentSchedule.map(function(e) { return e.day; }).indexOf(day);

            if (Session.get('selected_admin_performance_task').assessmentSchedule[scheduleIdx] != null
                    && Session.get('selected_admin_performance_task').assessmentSchedule[scheduleIdx].assessments != null) {
                var currentAssessmentIdx = Session.get('selected_admin_performance_task').assessmentSchedule[scheduleIdx].assessments.map(function(e) { return e._id; }).indexOf(this._id);

                return currentAssessmentIdx != null && currentAssessmentIdx+1 < Session.get('selected_admin_performance_task').assessmentSchedule[scheduleIdx].assessments.length ? "" : "disabled";
            }
        }

        return "disabled";
    },

    // Logic to determine if a collapsed assessment should be displayed
    displayAssessment: function (day) {
        var displayedAssessment = Session.get('display_assessment');

        return displayedAssessment != null && displayedAssessment.day == day && displayedAssessment._id == this._id ? "in" : "";
    }
});


////////// Events //////////

addPerformanceTask = function (data, closePT) {
    Meteor.call('addPerformanceTask', data, function (err, performanceTask) {
        if (err) {
            console.log(err);
            showBootstrapAlert("danger", err.reason);
        } else {
            if (performanceTask) {
                showBootstrapAlert("success", data.name + " has been created.");

                Session.set("selected_day", null);
                Session.set("selected_assessment", null);

                if (closePT) {
                    removeAllCKs();
                    Session.set('selected_admin_performance_task', null);
                } else {
                    data._id = performanceTask;
                    Session.set('selected_admin_performance_task', data);
                }
            } else {
                showBootstrapAlert("danger", "Please fill out every field.");
            }
        }
    });
};

updatePerformanceTask = function (data, closePT) {
    Meteor.call('updatePerformanceTask', Session.get('selected_admin_performance_task')._id, data, function (err, performanceTask) {
        if (err) {
            showBootstrapAlert("danger", err.reason);
        } else {
            if (performanceTask) {
                showBootstrapAlert("success", data.name + " has been updated.");

                Session.set("selected_day", null);
                Session.set("selected_assessment", null);

                if (closePT) {
                    removeAllCKs();
                    Session.set('selected_admin_performance_task', null);
                } else {
                    data._id = Session.get('selected_admin_performance_task')._id;
                    Session.set('selected_admin_performance_task', data);
                }
            } else {
                showBootstrapAlert("danger", "Please fill out every field.");
            }
        }
    });
};

savePerformanceTask = function (evt, template, closePT) {
    evt.preventDefault();

    var performanceTaskData = Session.get('selected_admin_performance_task') != null ? Session.get('selected_admin_performance_task') : {},
        allBookNames = template.findAll('input[name=bookName]'),
        allBookUrls = template.findAll('input[name=bookUrl]');
        file = template.find('#input-visual').files[0];

    // Update data from form values 
    performanceTaskData.grade = template.find('select[name=grade]').value;
    performanceTaskData.subject = template.find('input:radio[name=subject]:checked').value;
    performanceTaskData.name = template.find('input[name=performanceTaskName]').value;

    if (performanceTaskData.subject === 'ela') {
        performanceTaskData.textType = template.find('input:radio[name=textType]:checked').value;

        // Update assessment values
        if (performanceTaskData.books) {
            for (i = 0; i < performanceTaskData.books.length; i++) {
                performanceTaskData.books[i].name = allBookNames[i].value;
                performanceTaskData.books[i].url = allBookUrls[i].value;
            }
        }
    } else {
        performanceTaskData.textType = "";
        performanceTaskData.books = [];
    }

    performanceTaskData.performanceTaskPdfDoc = template.find('input[name=performanceTaskPdfDoc]').value;
    performanceTaskData.studentResource = template.find('input[name=studentResource]').value;
    performanceTaskData.performanceTaskWordDoc = template.find('input[name=performanceTaskWordDoc]').value;

    if (performanceTaskData.subject == 'math') {
        performanceTaskData.performanceTaskExcelDoc = template.find('input[name=performanceTaskExcelDoc]').value;
        performanceTaskData.glossary = "";
    }  else {
        performanceTaskData.performanceTaskExcelDoc = "";
        performanceTaskData.glossary = template.find('input[name=glossary]').value;
    }

    performanceTaskData.summaryGlance = template.find('textarea[name=textareaSummaryGlance]').value;

    if (performanceTaskData.subject === 'ela') {
        performanceTaskData.instructions.plan = template.find('textarea[name=textareaPlan]').value;
        performanceTaskData.instructions.outline = template.find('textarea[name=textareaOutline]').value;
        performanceTaskData.instructions.ifThen = template.find('textarea[name=textareaIfThen]').value;
    }

    performanceTaskData.instructions.overview = template.find('textarea[name=textareaOverview]').value;
    performanceTaskData.instructions.readySetGo = template.find('textarea[name=textareaReadySetGo]').value;
    performanceTaskData.instructions.ccssEvidence = template.find('textarea[name=textareaCcss]').value;

    // Update assessement day step and response
    for (var i = 0; i < performanceTaskData.assessmentSchedule.length; i++) {
        var currentDay = performanceTaskData.assessmentSchedule[i].day,
            currentAssessment = performanceTaskData.assessmentSchedule[i].assessments;

        performanceTaskData.assessmentSchedule[i].dayInstructions = template.find('textarea[name=dayInstructions' + currentDay + ']').value;

        if (currentAssessment) {
            for (var j = 0; j < currentAssessment.length; j++) {
                var currentAssessmentId = performanceTaskData.assessmentSchedule[i].assessments[j]._id;

                performanceTaskData.assessmentSchedule[i].assessments[j].step = template.find('input[name=step' + currentDay + currentAssessmentId + ']').value;

                if (performanceTaskData.assessmentSchedule[i].assessments[j].stepType == "instruction")  {
                    performanceTaskData.assessmentSchedule[i].assessments[j].assessment_id = "";
                    performanceTaskData.assessmentSchedule[i].assessments[j].stepInstruction = template.find('textarea[name=textareaStepInstruction' + currentDay + currentAssessmentId + ']').value;
                } else {
                    performanceTaskData.assessmentSchedule[i].assessments[j].responseType = template.find('input[name=responseType' + currentDay + currentAssessmentId + ']').value;
                    performanceTaskData.assessmentSchedule[i].assessments[j].studentWorkProduct = template.find('input[name=studentWorkProduct' + currentDay + currentAssessmentId + ']').value;

                    if (performanceTaskData.subject === 'ela') {
                        performanceTaskData.assessmentSchedule[i].assessments[j].standardsEvidence = template.find('textarea[name=standardsEvidence' + currentDay + currentAssessmentId + ']').value;
                    }

                    var currentAssessmentValues = performanceTaskData.assessmentSchedule[i].assessments[j].possibleValues;

                    for (var k = 0; k < currentAssessmentValues.length; k++) {
                        var currentAssessmentValueId = performanceTaskData.assessmentSchedule[i].assessments[j].possibleValues[k]._id,
                            possibleReponseImageInput = template.find('input[name=isPossibleResponseImage' + currentDay + currentAssessmentId + currentAssessmentValueId + ']'),
                            possibleResponseInput = template.find('input[name=possibleResponse' + currentDay + currentAssessmentId + currentAssessmentValueId + ']');

                        performanceTaskData.assessmentSchedule[i].assessments[j].possibleValues[k].isPossibleResponseImage = possibleReponseImageInput != null ? possibleReponseImageInput.checked : false;
                        performanceTaskData.assessmentSchedule[i].assessments[j].possibleValues[k].possibleResponse = possibleResponseInput != null ? possibleResponseInput.value : '';
                    }
                }
            }
        }
    }

    if (Session.get('selected_admin_performance_task') != null) {
        delete performanceTaskData._id;

        if (Session.get('selected_admin_performance_task')._id != null) {
            var reader = new FileReader();

            reader.onload = function(e) {
                performanceTaskData.visual = e.target.result;

                updatePerformanceTask(performanceTaskData, closePT);
            };

            if (file) {
                reader.readAsDataURL(file);
            } else {
                updatePerformanceTask(performanceTaskData, closePT);
            }

            console.log("Update Performance Task");
        } else {
            var addReader = new FileReader();

            addReader.onload = function(e) {
                performanceTaskData.visual = e.target.result;

                addPerformanceTask(performanceTaskData, closePT);
            };

            if (file) {
                addReader.readAsDataURL(file);
            } else {
                addPerformanceTask(performanceTaskData, closePT);
            }

            console.log("Add Performance Task");
        }
    }
};


Template.performanceTaskForm.events({
    'click #plan-toggle': function () {
        if ($('#collapseinstructionplan')[0].className === 'collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                // Logic for uploading media files via the CKEditor; passes a token to PHP for authentication
                var instructionEditor = $('#text-plan').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });

                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-plan').ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #overview-toggle': function () {
        if ($('#collapseinstructionoverview')[0].className === 'collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                // Logic for uploading media files via the CKEditor; passes a token to PHP for authentication
                var instructionEditor = $('#text-overview').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });

                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-overview').ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #outline-toggle': function () {
        if ($('#collapseinstructionOutline')[0].className === 'collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                // Logic for uploading media files via the CKEditor; passes a token to PHP for authentication
                var instructionEditor = $('#text-outline').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });

                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-outline').ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #outline-toggle': function () {
        if ($('#collapseinstructionOutline')[0].className === 'collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                // Logic for uploading media files via the CKEditor; passes a token to PHP for authentication
                var instructionEditor = $('#text-outline').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });

                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-outline').ckeditor(function(){
                this.destroy();
                this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #standards-toggle': function () {
        if ($('#collapseinstructionstandards')[0].className === 'collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                // Logic for uploading media files via the CKEditor; passes a token to PHP for authentication
                var instructionEditor = $('#text-ccss').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });

                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-ccss').ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #readySetGo-toggle': function () {
        if ($('#collapseinstructionreadysetgo')[0].className === 'collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                // Logic for uploading media files via the CKEditor; passes a token to PHP for authentication
                var instructionEditor = $('#text-readySetGo').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });

                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-readySetGo').ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #ifthen-toggle': function () {
        if ($('#collapseinstructionifthen')[0].className === 'collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                // Logic for uploading media files via the CKEditor; passes a token to PHP for authentication
                var instructionEditor = $('#text-ifthen').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });

                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-ifthen').ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #day-toggle': function (e) {
        if ($('#collapse' + this.day)[0].className === 'assessment-day-item panel-collapse collapse') {
            var currentDay = this.day;

            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';
                $(".collapseableList.in").collapse('hide');
                removeAllCKs();
                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }

                var instructionEditor = $('#text-dayInstructions'+currentDay).ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });
                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });

        } else {
            $('#text-dayInstructions' + this.day).ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'change #select-grade': function (evt) {
        // Update the common core filter with the grade selected
        var newValue = $(evt.target).val(),
            oldValue = Session.get("standards_filters").grade;

        if (newValue != oldValue) {
            Session.set("standards_filters", {grade: newValue, subject: Session.get("standards_filters").subject});
        }
    },
    'change #radio-subject': function (evt) {
        // Update the common core filter with the subject selected
        var newValue = $(evt.target).val(),
            oldValue = Session.get("standards_filters").subject;

        if (newValue != oldValue) {
            Session.set("standards_filters", {grade: Session.get("standards_filters").grade, subject: newValue});
        }
    },
    'click #add-book': function () {
        var selectedPerformanceTask = Session.get('selected_admin_performance_task'),
            newId = new Meteor.Collection.ObjectID()._str;

        if (!selectedPerformanceTask.books) {
            selectedPerformanceTask.books = [];
        }

        selectedPerformanceTask.books.push({_id: newId, name: "", url: ""});

        Session.set("selected_admin_performance_task", selectedPerformanceTask);
    },
    'click #delete-book': function () {
        var deleteBook = this;

        bootbox.confirm("Are you sure you want to delete book, " + deleteBook.name + "?", function(result) {
            if (result === true) {
                var selectedPerformanceTask = Session.get('selected_admin_performance_task'),
                    pos = selectedPerformanceTask.books.map(function (e) {
                        return e._id;
                    }).indexOf(deleteBook._id);

                selectedPerformanceTask.books.splice(pos, 1);

                Session.set("selected_admin_performance_task", selectedPerformanceTask);
            }

            return;
        });
    },
    'click #add-standard': function (evt, template) {
        // Add a common core state standard to the performance task
        var standard = template.find('select[name=standard]').value,
            selectedPerformanceTask = Session.get('selected_admin_performance_task');

        if (standard) {
            if (!selectedPerformanceTask.commonCoreStateStandards) {
                selectedPerformanceTask.commonCoreStateStandards = [];
            }

            selectedPerformanceTask.commonCoreStateStandards.push({_id: standard});
            Session.set('selected_admin_performance_task', selectedPerformanceTask);
        }
    },
    'click #delete-standard': function () {
        //Remove a common core state standard from the performance task
        var deleteId = this._id;
        bootbox.confirm("Are you sure you want to delete the standard, " + this._id + "?", function(result) {
            if (result === true) {
                // Remove performance task
                var selectedPerformanceTask = Session.get('selected_admin_performance_task');
                    //pos = selectedPerformanceTask.commonCoreStateStandards.map(function(e) { return e._id; }).indexOf(deleteId);
                    for ( var i = 0; i < selectedPerformanceTask.commonCoreStateStandards.length; i++){
                        if (selectedPerformanceTask.commonCoreStateStandards[i]._id === deleteId){
                            selectedPerformanceTask.commonCoreStateStandards.splice(i, 1);
                        }
                }
                Session.set('selected_admin_performance_task', selectedPerformanceTask);
            }
        });
    },
    'click #add-day': function (evt, template) {
        // Add an assessment day to the performance task
        var day = template.find('select[name=day]').value,
            selectedPerformanceTask = Session.get('selected_admin_performance_task');

        if (day) {
            if (!selectedPerformanceTask.assessmentSchedule) {
                selectedPerformanceTask.assessmentSchedule = [];
            }

            selectedPerformanceTask.assessmentSchedule.push({day: day, assessments: []});
            Session.set('selected_admin_performance_task', selectedPerformanceTask);
        }
    },
    'click #delete-day': function () {
        //Remove a day from the performance task assessments
        var day = this.day;
        bootbox.confirm("Are you sure you want to delete day " + this.day + "?", function(result) {
            if (result === true) {
                var selectedPerformanceTask = Session.get('selected_admin_performance_task'),
                    pos = selectedPerformanceTask.assessmentSchedule.map(function (e) {
                        return e.day;
                    }).indexOf(day);

                selectedPerformanceTask.assessmentSchedule.splice(pos, 1);
                Session.set('selected_admin_performance_task', selectedPerformanceTask);
            }
        });
    },
    'click #add-assessment': function (evt, template) {
        // Add an assessment to the performance task
        var id = new Meteor.Collection.ObjectID()._str,
            assessment = template.find('select[name=assessment' + this.day + ']').value;

        if (assessment) {
            var selectedPerformanceTask = Session.get('selected_admin_performance_task'),
                posDay = selectedPerformanceTask.assessmentSchedule.map(function(e) { return e.day; }).indexOf(this.day),
                stepType = "assessment";

            if (assessment == "assessment_instruction") {
                stepType = "instruction";
            } else {
                var assessmentData = Assessments.findOne({_id: assessment});
            }

            if (posDay != -1) {
                if (!selectedPerformanceTask.assessmentSchedule[posDay].assessments) {
                    selectedPerformanceTask.assessmentSchedule[posDay].assessments = [];
                }

                var possibleAssessValues = [];

                if (assessmentData) {
                    for (var i = 0; i < assessmentData.assessValues.length; i++) {
                        possibleAssessValues.push({
                            _id: assessmentData.assessValues[i]._id,
                            possibleResponse: "",
                            isPossibleResponseImage: false
                        });
                    }
                }

                selectedPerformanceTask.assessmentSchedule[posDay].assessments.push({_id: id, assessment_id: assessment, stepType: stepType, stepInstruction: "", possibleValues: possibleAssessValues});

                Session.set('display_assessment', {day: this.day, _id: id});
                Session.set('selected_admin_performance_task', selectedPerformanceTask);

                var self = this;

                //removeStepCKs();

                setTimeout(function(){
                    if ($("#collapse"+self.day)[0].className === 'assessment-day-item panel-collapse collapse') {
                        $("#collapse"+self.day).collapse('show');
                        (initializeCK(false));
                    }
                    else {
                        $("#collapse"+self.day).collapse('show');
                        initializeCK(true);
                    }
                }, 300);
            }
        }
    },
    'click #delete-assessment': function (evt, template) {
        //Remove an assessment from the performance task day
        var selectedPerformanceTask = Session.get('selected_admin_performance_task'),
            day = evt.currentTarget.getAttribute('data-day'),
            posDay = selectedPerformanceTask.assessmentSchedule.map(function(e) { return e.day; }).indexOf(day);

        if (posDay != -1) {
            var posAssessment = selectedPerformanceTask.assessmentSchedule[posDay].assessments.map(function(e) { return e._id; }).indexOf(this._id);

            if (posAssessment != -1) {
                selectedPerformanceTask.assessmentSchedule[posDay].assessments.splice(posAssessment, 1);
                Session.set('selected_admin_performance_task', selectedPerformanceTask);
            }
        }
    },
    'click #assessment-order-up': function (evt, template) {
        if (Session.get('selected_admin_performance_task') != null) {
            var selectedPT = Session.get('selected_admin_performance_task'),
                day = evt.currentTarget.getAttribute('data-day'),
                posDay = selectedPT.assessmentSchedule.map(function(e) { return e.day; }).indexOf(day),
                assessmentsList = selectedPT.assessmentSchedule[posDay].assessments,
                assessmentPos = assessmentsList.map(function(e) { return e._id; }).indexOf(this._id);

            if (assessmentPos != 0) {
                assessmentsList.splice(assessmentPos-1, 0, assessmentsList.splice(assessmentPos, 1)[0] );
                selectedPT.assessmentSchedule[posDay].assessments = assessmentsList;
                Session.set('selected_admin_performance_task', selectedPT);
            }
        }
    },
    'click #assessment-order-down': function (evt, template) {
        if (Session.get('selected_admin_performance_task') != null) {
            var selectedPT = Session.get('selected_admin_performance_task'),
                day = evt.currentTarget.getAttribute('data-day'),
                posDay = selectedPT.assessmentSchedule.map(function(e) { return e.day; }).indexOf(day),
                assessmentsList = selectedPT.assessmentSchedule[posDay].assessments,
                assessmentPos = assessmentsList.map(function(e) { return e._id; }).indexOf(this._id);

            if (assessmentPos != assessmentsList.length-1) {
                assessmentsList.splice(assessmentPos+1, 0, assessmentsList.splice(assessmentPos, 1)[0] );
                selectedPT.assessmentSchedule[posDay].assessments = assessmentsList;
                Session.set('selected_admin_performance_task', selectedPT);
            }
        }
    },
    'click #assessment-name-heading': function (evt, template) {
        // Clear displayed assessment
        Session.set('display_assessment', null);

        //  Hide other steps and destroy the
        var target = $(evt.target).parents("ul");
        target.find('.collapseableList').collapse('hide');
        var selector = $(evt.target).parent().next();

        target.find('.editable').not(selector.find('.editable')).ckeditor(function(){this.destroy()});

        if ($(evt.target).parent().next()[0].className === 'collapseableList collapse') {
            Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
                var glossaryTerms = Glossary.find().fetch(),
                    hash = '';

                if (user.services) {
                    hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                }
                var instructionEditor = selector.find('.editable').ckeditor({
                    filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                    filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                    filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                    filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                    filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                    filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                });
                instructionEditor.editor.glossaryTerms = glossaryTerms;
            });
        } else {
            $(evt.target).parent().next().find('.editable').ckeditor(function(){
                this.destroy();
                //this.filter.destroy();
                delete this.filter;
                delete this.activeFilter;
            });
        }
    },
    'click #day-name-heading': function (evt, template) {
        // Clear displayed assessment
        Session.set('display_assessment', null);
    },
    'click #btn-save-close': function (evt, template) {
        savePerformanceTask(evt, template, true);
    },
    'submit form': function (evt, template) {
        savePerformanceTask(evt, template, false);
     }
});


/**
 * Initialize CK Editor for a step;
 */
function initializeCK (expanded) {
        //allow time for new dom element to get placed ...
    Meteor.call('getUserToken', Meteor.user()._id, function(err, user) {
            var glossaryTerms = Glossary.find().fetch(),
                hash = '',
                selector =  "#collapse" + Session.get('display_assessment').day + " #pt-days-list li:last .editable";


            if (user.services) {
                        hash = encodeURIComponent(user.services.resume.loginTokens[0].hashedToken);
                    }
            if (!expanded){
                selector += ", #text-dayInstructions"+Session.get('display_assessment').day;
                removeAllCKs();
            }
                    var instructionEditor = $(selector).ckeditor({
                        filebrowserBrowseUrl: '/ck/ckfinder.html' + "?token=" + hash,
                        filebrowserImageBrowseUrl: '/ck/ckfinder.html?type=Images' + "&token=" + hash,
                        filebrowserFlashBrowseUrl: '/ck/ckfinder.html?type=Flash' + "&token=" + hash,
                        filebrowserUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Files' + "&token=" + hash,
                        filebrowserImageUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Images' + "&token=" + hash,
                        filebrowserFlashUploadUrl: '/ck/core/connector/php/connector.php?command=QuickUpload&type=Flash' + "&token=" + hash
                    });
                    instructionEditor.editor.glossaryTerms = glossaryTerms;
    });
}

function removeAllCKs () {
    for (var name in CKEDITOR.instances) {
        if (CKEDITOR.instances.hasOwnProperty(name) && CKEDITOR.instances[name]){
            CKEDITOR.instances[name].destroy()
        }
    }
}
