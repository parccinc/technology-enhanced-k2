Template.ipad.helpers({
    getPTPlan: function () {
        return Session.get("selected_performancetask").instructions.plan;
    },
    getPTOverview: function () {
        return Session.get("selected_performancetask").instructions.overview;
    },
    getheight: function () {
        var height = $(window).height();
        console.log(height);
        return height + 'px';
    }
});