/**
 * Template: classDetails
 *
 * Helper functions
 */

Template.classDetails.helpers({
    // Returns a list of subjects for the dropdown. Placed in a helper function so that it can be modified later if need be.
    subject: function () {
        var s = []

        s.push("ELA");
        s.push("Math");

        return s;
    },
    // Returns a filtered list of performance tasks based on the user's filter selections
    filterPerformanceTask: function () {
        return getFilterPerformanceTasks();
    },
    // Returns a filtered list of standards based on the user's filter selections
    filterStandards: function () {
        return getFilterStandards();
    },
    // Returns a filtered list of just ELA performance tasks
    filterElaPerformanceTasks: function () {
        return getElaFilteredPerformanceTasks();
    },
    // Returns a filtered list of just Math performance tasks
    filterMathPerformanceTasks: function () {
        return getMathFilteredPerformanceTasks();
    },
    // Returns the name of the selected group/classroom
    selectedGroup: function () {
        return getSelectedGroupName() || null;
    },
    // Returns whether or not a group/classroom has been selected
    groupSelected: function () {
        if (getGroupSelected()) {
            $('#class-detail-search-input').removeAttr('disabled');
        } else {
            $('#class-detail-search-input').attr('disabled','disabled');
        }

        return getGroupSelected();
    },
    // Returns whether or not a subject has been selected
    subjectSelected: function () {
        return getSubjectSelected();
    },
    // Returns whether or not a performance task has been selected
    performanceTaskSelected: function () {
        return getPerformanceTaskSelected();
    },
    // Returns whether or not a standard has been selected
    standardSelected: function () {
        return getStandardSelected();
    },
    // Returns the selected subject, if applicable
    selectedSubject: function () {
        return getSelectedSubject();
    },
    // Returns the selected performance task, if applicable
    selectedPerformanceTask: function () {
        return getSelectedPerformanceTask();
    },
    // Returns the selected standard, if applicable
    selectedStandard: function () {
        return getSelectedStandard();
    },
    // Returns whether or not the user has selected a group, subject, performance task, and standard.
    allUserSelectionsMade: function () {
        if (getGroupSelected() && getSubjectSelected() && getPerformanceTaskSelected() && getStandardSelected()) {
            return true;
        }
    },
    // Returns a list of all students in the current classroom/group
    groupStudent: function() {
        return listGroupStudents();
    },
    // Returns a list of assessments related to a performance task
    performanceTaskAssessment: function() {
        return listPerformanceTaskAssessments(Session.get('selected_performance_task'));
    },
    // Returns a list of possible assessment values associated with an assessment
    assessmentValue: function() {
        return listAssessmentValues(Session.get('selected_assessment'));
    },
    loadingReport: function () {
        return Session.get('loading_report');
    },
    displayAllPerformanceTasks: function () {
        var selectedSubject = Session.get('selected_subject');

        if (selectedSubject != null) {
            return selectedSubject.trim().toUpperCase() == "ALL" || selectedSubject.trim().toUpperCase() == "BOTH";
        } else {
            return true;
        }
    },
    filterSelectionMade: function () {
        return getSubjectSelected() || getPerformanceTaskSelected() || getStandardSelected();
    }
});


/**
 * Template: classDetails
 *
 * Events
 */

Template.classDetails.events({

    // User clicks the 'Save as .csv' button
    'click #class-details-csv-button': function () {
        if (mobileAndTabletcheck()) {

            //alert("Please access this page from a desktop computer to download a .csv file.");
            bootbox.alert("Please access this page from a desktop computer or laptop to download a .csv file.", function() {});

        } else {

            var fileText = "",
                reportData = getReportOutput(function () {
                });

            var removeUnwantedCharacters = function (input) {
                return input.replace(/,/g, "").replace(/"/g, '').replace(/“/g, "").replace(/”/g, "");
            };

            fileText += removeUnwantedCharacters(getSelectedGroupName()) + "\n\n";

            // Write the report output to a string
            //reportData.forEach(function (performanceTask, ptIndex) {
            //    if (reportData.hasAssessments) {
                    fileText += reportData.name + "\n";

                    reportData.assessmentSchedule.forEach(function (assessItem, assessItemIndex) {
                        if (assessItem.hasAssessments) {
                            fileText += "Day " + assessItem.day + "\n";

                            // Print header rows
                            assessItem.assessments.forEach(function (assessment, assessmentIndex) {
                                if (assessment.hasAssessments) {
                                    // First Header Column: Assessment name
                                    fileText += removeUnwantedCharacters(assessment.assessmentObject.name) /*+ " (" + removeUnwantedCharacters(formattedDate(assessment.timestamp)) + ")"*/ + ",";

                                    // Second Header Column: Number of students
                                    fileText += "Number of Students with Collected Information: " + listGroupStudents().length.toString() + ",";

                                    // Third - Last Header Columns: Student names
                                    listGroupStudents().forEach(function (student, studentIndex) {
                                        fileText += removeUnwantedCharacters(student.firstname) + " " + removeUnwantedCharacters(student.lastname) + ",";
                                    });

                                    fileText += "\n";

                                    // Print each row of the report
                                    assessment.assessmentObject.assessValues.forEach(function (assessValueItem, assessValueItemIndex) {
                                        // First Column: Assessment Value
                                        fileText += removeUnwantedCharacters(assessValueItem.value) + ",";

                                        // Second Column: Number of students with this assessment value
                                        fileText += assessValueItem.checkedCount.toString() + ",";
                                        // Third - Last Column: Student assessment selections
                                        assessValueItem.studentList.forEach(function (studentListItem, studentListItemIndex) {
                                            if (studentListItem) {
                                                if (studentListItem.flagged) {
                                                    fileText += "F,";
                                                } else if (assessment.assessmentObject.type == "rubric" && studentListItem.checked) {
                                                    if (assessValueItem.value.toUpperCase() == "MEETING") {
                                                        fileText += "M,";
                                                    } else if (assessValueItem.value.toUpperCase() == "APPROACHING") {
                                                        fileText += "A,";
                                                    } else if (assessValueItem.value.toUpperCase() == "DEVELOPING") {
                                                        fileText += "D,";
                                                    } else if (assessValueItem.value.toUpperCase() == "EMERGING") {
                                                        fileText += "E,";
                                                    } else {
                                                        fileText += "X,";
                                                    }
                                                } else if (studentListItem.checked) {
                                                    fileText += "X,";
                                                } else if (studentListItem.yes) {
                                                    fileText += "YES,";
                                                } else if (studentListItem.no) {
                                                    fileText += "NO,";
                                                } else {
                                                    fileText += ",";
                                                }
                                            } else {
                                                fileText += ",";
                                            }
                                        });

                                        fileText += "\n";
                                    });

                                    fileText += "\n";
                                }
                            });
                        }
                    });

                    fileText += "\n";
                //}
            //});

            // Save the .csv
            var blob = new Blob([fileText], {type: "text/plain;charset=utf-8"}),
                filename = removeUnwantedCharacters(getSelectedGroupName()).replace(/ /g, "_").toLowerCase();
            saveAs(blob, filename + ".csv");
        }
    },
    'submit #class-details-search-form': function (event, template) {
        event.preventDefault();

        var input = template.find('input[name=classDetailSearchInput]').value.trim().toUpperCase();

        if (input != null && input != "") {
            Session.set('selected_standard', input);
        }
    }
});


/**
 * Template: classDetails
 *
 * Rendered function
 */

Template.classDetails.rendered = function() {
    /* The filter uses the user's selected subject, performance task, and standard
       session variables, so reset these when the page loads.
     */
    Session.set('selected_subject', null);
    Session.set('selected_performance_task', null);
    Session.set('selected_standard', null);
    Session.set('selected_assessment', null);
    Session.set('selected_image_artifact', null);
    Session.set('loading_report', false);
    Session.set('pagination_data', []);

    // Disables the search box when the page is loaded.
    if (getGroupSelected()) {
        this.$('#class-detail-search-input').removeAttr('disabled');
    } else {
        this.$('#class-detail-search-input').attr('disabled','disabled');
    }

    $(window).resize();
}

Template.classDetails.onCreated(function(){
    Tracker.autorun(function(){
        var assessmentIds = [];
        if (Session.get('selected_group') && Session.get('selected_group')._id) {
            Students.find({classroom_id: Session.get('selected_group')._id}).fetch().forEach(function (aStudent) {
                for (var index in aStudent.assessmentdata) {
                    if (aStudent.assessmentdata.hasOwnProperty(index)) {
                        if (assessmentIds.indexOf(index) == -1) {
                            var push = false;
                            for (var i in aStudent.assessmentdata[index]) {
                                if (aStudent.assessmentdata[index].hasOwnProperty(i)) {
                                    for (var p in aStudent.assessmentdata[index][i]) {
                                        if (aStudent.assessmentdata[index][i].hasOwnProperty(p)) {
                                            if (aStudent.assessmentdata[index][i][p].value.length > 0 && PerformanceTasks.findOne({'assessmentSchedule.assessments._id': p})) {
                                                push = true;
                                                break;
                                            }
                                        }
                                    }

                                }
                                if (push === true) {
                                    break;
                                }
                            }
                            if (push) {
                                assessmentIds.push(index);
                            }
                        }
                    }
                }
            });
        }
        Session.set('assessmentIds', assessmentIds);
    });
});


/**
 * Template: classDetailsReport
 *
 * helpers
 */

Template.classDetailsReport.helpers({
    // Returns a list of subjects for the dropdown. Placed in a helper function so that it can be modified later if need be.
    titleColor: function(subject) {
        if (subject) {
            if (subject.toUpperCase() == "ELA") {
                return "#009900";
            } else if (subject.toUpperCase() == "MATH") {
                return "#8080c0";
            } else {
                return "#999999";
            }
        }
    },
    subject: function () {
        var s = []

        s.push("ELA");
        s.push("Math");

        return s;
    },
    // Returns a filtered list of performance tasks based on the user's filter selections
    filterPerformanceTask: function () {
        return getFilterPerformanceTasks();
    },
    // Returns a filtered list of standards based on the user's filter selections
    filterStandards: function () {
        return getFilterStandards();
    },
    // Returns an array of objects that gets outputted to the user
    reportOutput: function () {
        return getReportOutput(function(error){
            if (error) {
                console.log(error)
            } else {
                $('#class-details-page-content').scrollTop(0);

                resizeClassDetailsTables();
            }

            Session.set('loading_report', false);
        });
    },
    // Returns the name of the selected group/classroom
    selectedGroup: function () {
        if (Session.get('selected_group')) {
            return Session.get('selected_group').name || "";
        } else {
            return null;
        }
    },
    // Returns whether or not a group/classroom has been selected
    groupSelected: function () {
        if (getGroupSelected()) {
            $('#class-detail-search-input').removeAttr('disabled');
        } else {
            $('#class-detail-search-input').attr('disabled','disabled');
        }

        return getGroupSelected();
    },
    // Returns whether or not a subject has been selected
    subjectSelected: function () {
        return getSubjectSelected();
    },
    // Returns whether or not a performance task has been selected
    performanceTaskSelected: function () {
        return getPerformanceTaskSelected();
    },
    // Returns whether or not a standard has been selected
    standardSelected: function () {
        return getStandardSelected()
    },
    // Returns the selected subject, if applicable
    selectedSubject: function () {
        return getSelectedSubject();
    },
    // Returns the selected performance task, if applicable
    selectedPerformanceTask: function () {
        return getSelectedPerformanceTask();
    },
    // Returns the selected standard, if applicable
    selectedStandard: function () {
        return getSelectedStandard();
    },
    // Returns whether or not the user has selected a group, subject, performance task, and standard.
    allUserSelectionsMade: function () {
        return getGroupSelected() && getSubjectSelected() && getPerformanceTaskSelected() && getStandardSelected();
    },
    // Returns a list of all students in the current classroom/group
    groupStudent: function() {
        return listGroupStudents();
    },
    // Returns the number of students in the class
    numberOfStudents: function () {
        return this.assessmentObject.uniqueStudents.length;
    },
    // Returns a list of assessments related to a performance task
    performanceTaskAssessment: function() {
        return listPerformanceTaskAssessments(Session.get('selected_performance_task'));
    },
    // Returns a list of possible assessment values associated with an assessment
    assessmentValue: function() {
        return listAssessmentValues(Session.get('selected_assessment'));
    },
    // Returns a formatted version of the date
    formatDate: function(date) {
        return formattedDate(date);
    },
    // Ensures that the performance task has a color bar
    getPerformanceTaskColor: function (color) {
        if (color != null) {
            return color;
        } else {
            return "#999999"
        }
    },
    lengthOfArray: function (arr) {
        return arr.length;
    },
    isRubric: function (type) {
        return type == "rubric";
    },
    isMeeting: function (value) {
        return value.toUpperCase() == "MEETING";
    },
    isDeveloping: function (value) {
        return value.toUpperCase() == "DEVELOPING";
    },
    isApproaching: function (value) {
        return value.toUpperCase() == "APPROACHING";
    },
    isEmerging: function (value) {
        return value.toUpperCase() == "EMERGING";
    }
});

Template.classDetailsReport.events({
    'click #openClassArtifactModal': function () {
        Session.set('selectedArtifacts', this._id);
    }
});

/**
 * Template: classDetailsReport
 *
 * Rendered function
 */
Template.classDetailsReport.rendered = function(){
    $(window).resize();

    resizeClassDetailsTables();

};


/**
 * Template: classArtifactModal
 *
 * Helpers
 */
Template.classArtifactModal.helpers({
    className: function () {
        return Session.get('selected_group') != null ? Session.get('selected_group').name : "";
    },
    notes: function () {
        if (Session.get('selectedArtifacts')) {
            return Media.find({type: "note", performance_task_id: Session.get('selectedArtifacts')}).fetch();
        }
    },
    images: function () {
        if (Session.get('selectedArtifacts') !== null ) {
            return Media.find({type: "image", performance_task_id: Session.get('selectedArtifacts')}).fetch();
        }
    },
    // Converts an array into a comma separated string of list items
    arrayToString: function(list) {
        return convertArrayIdsToString(list);
    },

    // Format note date/time
    getFormattedDate: function (noteDate) {
        return formattedDate(noteDate);
    },

    fileInfo: function (mediaDataId) {
        if (mediaDataId) {
            return MediaData.findOne({_id: mediaDataId });
        } else {
            return null;
        }
    },

    // Returns the currently selected image artifact, if there is one
    selectedImageArtifact: function() {
        return Session.get('selected_image_artifact');
    },

    // Returns whether or not the user is currently viewing an image artifact or the entire modal.
    imageArtifactSelected: function() {
        return Session.get('selected_image_artifact') != null;
    },
    studentNames: function(nameIds) {
        var names = [];

        for (var i in nameIds) {
            var student = Students.findOne({ _id: nameIds[i] });

            if (student) {
                names.push(student.firstname + " " + student.lastname);
            }
        }

        return convertArrayToString(names);
    },
    hasMultipleImages: function(imageId) {
        return Media.findOne({ _id: imageId }).file.length > 1;
    }
});

/**
 * Template: classArtifactModal
 *
 * Events
 */
Template.classArtifactModal.events({
    'click #artifact-view-modal-back': function() {
        Session.set('selected_image_artifact', null);
    }
});

/**
 * Template: classArtifactModal
 *
 * Rendered
 */
Template.classArtifactModal.rendered = function () {
    $('#classArtifactViewModal').on('hidden.bs.modal', function () {
        Session.set('selected_image_artifact', null);
    });

    $('#notes a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })

    $('#images a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
};







/**
 * Template: loadingSpinner
 *
 * Loading animation for the reports.
 */
Template.loadingSpinner.helpers({
    isStudentDetails: function() {
        return Router.current().route.path(this) == "/studentDetails";
    }
});





function getListOfPerformanceTasks(){
    var query = {_id:{$in:Session.get('assessmentIds')}},
        subject = Session.get('selected_subject'),
        performanceTask = Session.get('selected_performance_task'),
        standards = Session.get('selected_standard');
    if (subject !== "All" && subject !== "Both") {
        query.subject = subject.toLowerCase();
    }
    if (performanceTask !== "All") {
        query._id = performanceTask
    }
    if (standards !== "All") {
        query['commonCoreStateStandards._id'] = standards;
    }
    return PerformanceTasks.find(query).fetch();
}

/**
 * Template: classDetailsPagination
 *
 * Helpers
 */
Template.classDetailsPagination.helpers({
    page: function () {
        var output = Session.get('output');
        console.log(output);
        var paginationData = [];
        for (var i = 0; i < output.length; i++) {
            var a = {
                pageNumber: i + 1,
                image: output[i].visual,
                name: output[i].name.replace(/"/g, ''),
                initial: output[i].name.substr(0, 1).toUpperCase(),
                subject: output[i].subject
            };

            if (output[i].subject.trim().toUpperCase() == "ELA") {
                a.color = "#009900";
            } else if (output[i].subject.trim().toUpperCase() == "MATH") {
                a.color = "#8080c0";
            } else {
                a.color = "#000000";
            }

            paginationData.push(a);
        }
        return paginationData;
    },
    displayPaginationMenu: function () {
        return Session.get('details_num_pages') > 0;
    },
    firstPage: function () {
        return Session.get('details_page') === 1;

    },
    lastPage: function () {
        return Session.get('details_num_pages') === Session.get('details_page');
    }
});







/**
 * Template: classDetailsPaginationItem
 *
 * Rendered
 */
Template.classDetailsPaginationItem.rendered = function () {
    $('[data-toggle="tooltip"]').tooltip();
}

/**
 * Template: classDetailsPaginationItem
 *
 * Helpers
 */
Template.classDetailsPaginationItem.helpers({
    isActivePage: function (input) {
        return input == Session.get('details_page');
    },
    isELA: function (subject) {
        return subject === "ela";
    }
});










/**
 * Javascript functions
 */

// Returns a list of all performance tasks for the current grade and subject
allPerformanceTasks = function() {
    if (Session.get('selected_grade')) {
        return PerformanceTasks.find({ grade: Session.get('selected_grade') }).fetch();
    } else {
        return null;
    }
};
// Returns whether or not the user has selected a group/classroom
getGroupSelected = function() {
    return Session.get('selected_group') && Session.get('selected_group').name != "";
}
// Returns whether or not the user has selected a subject
getSubjectSelected = function() {
    return Session.get('selected_subject') != null;
}
// Returns whether or not the user has selected a performance task
getPerformanceTaskSelected = function() {
    return Session.get('selected_performance_task') != null;
}
// Returns whether or not the user has selected a standard
getStandardSelected = function() {
    return Session.get('selected_standard') != null;
}
// Returns a filtered array of performance tasks based on the user's filter selections
getFilterPerformanceTasks = function() {
    var pts = allPerformanceTasks(),
        selectedSubject = Session.get('selected_subject');

    // Check if subject has already been selected by the user
    if (selectedSubject != null && selectedSubject != "" && pts) {
        // Users wants all subjects
        if (selectedSubject.toUpperCase() == "BOTH" || selectedSubject.toUpperCase() == "ALL") {
            // Do nothing. The user wants all subjects.

        } else {
            // User wants a particular subject
            // Loop through each object in the array and build a new array using only the objects with the selected subject
            var tmp = [];
            for (var i = 0; i < pts.length; i++) {
                if (pts[i].subject == selectedSubject.toLowerCase()) {
                    tmp.push(pts[i]);
                }
            }
            pts = tmp;
        }
    }

    function compare(a,b) {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    }

    // Set colors
    for (var i in pts) {
        pts[i].color = setPerformanceTaskColor(pts[i].subject);
    }

    if (pts) {
        return pts.sort(compare);
    } else {
        return null;
    }
};
// Returns just all ELA performance tasks
getElaFilteredPerformanceTasks = function() {
    var elaArray = [],
        allPts = allPerformanceTasks();

    // Iterate through all performance tasks and sort into two arrays
    for (var i in allPts) {
        if (allPts.hasOwnProperty(i)) {
            if (allPts[i].subject.toUpperCase() == "ELA") {
                elaArray.push(allPts[i]);
            }
        }
    }

    function compare(a,b) {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    }

    // Set colors
    for (var i in elaArray) {
        elaArray[i].color = setPerformanceTaskColor(elaArray[i].subject);
    }

    if (elaArray) {
        return elaArray.sort(compare);
    } else {
        return null;
    }
}
// Returns just all Math performance tasks
getMathFilteredPerformanceTasks = function() {
    var mathArray = [],
        allPts = allPerformanceTasks();

    // Iterate through all performance tasks and sort into two arrays
    for (var i in allPts) {
        if (allPts.hasOwnProperty(i)) {
            if (allPts[i].subject.toUpperCase() == "MATH") {
                mathArray.push(allPts[i]);
            }
        }
    }

    function compare(a,b) {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    }

    // Set colors
    for (var i in mathArray) {
        mathArray[i].color = setPerformanceTaskColor(mathArray[i].subject);
    }

    if (mathArray) {
        return mathArray.sort(compare);
    } else {
        return null;
    }
}
// Returns a filtered array of standards based on the user's filter selections
getFilterStandards = function() {
    var selectedPerformanceTask = Session.get('selected_performance_task');

    if (selectedPerformanceTask) {
        var pts = getFilterPerformanceTasks(),
            standards = [];

        if (pts) {
            // Loop through each performance task
            for (var i = 0; i < pts.length; i++) {
                if (pts.hasOwnProperty(i.toString())) {
                    // Check if the current performance task should be included in the list
                    if (!(selectedPerformanceTask.toUpperCase() == "ALL" || selectedPerformanceTask.toUpperCase() == "BOTH")) {
                        if (pts[i]._id.trim().toUpperCase() != selectedPerformanceTask.trim().toUpperCase()) {
                            continue;
                        }
                    }
                    // Loop through each standard in the current performance task and push it to the array if it does not already exist
                    for (var j = 0; j < pts[i].commonCoreStateStandards.length; j++) {
                        if (pts[i].commonCoreStateStandards.hasOwnProperty(j.toString())) {
                            if (!(standards.indexOf(pts[i].commonCoreStateStandards[j]._id.toString().trim().toUpperCase()) > -1)) {
                                standards.push(pts[i].commonCoreStateStandards[j]._id.trim().toUpperCase());
                            }
                        }
                    }
                }
            }
        }

        return standards.sort();
    } else {
        var pts = PerformanceTasks.find({ grade: Session.get('selected_grade')}).fetch(),
            standards = [];

        if (pts) {
            // Loop through each performance task
            for (var i = 0; i < pts.length; i++) {
                if (pts.hasOwnProperty(i.toString())) {
                    // Loop through each standard in the current performance task and push it to the array if it does not already exist
                    for (var j = 0; j < pts[i].commonCoreStateStandards.length; j++) {
                        if (pts[i].commonCoreStateStandards.hasOwnProperty(j.toString())) {
                            if (!(standards.indexOf(pts[i].commonCoreStateStandards[j]._id.toString().trim().toUpperCase()) > -1)) {
                                standards.push(pts[i].commonCoreStateStandards[j]._id.trim().toUpperCase());
                            }
                        }
                    }
                }
            }
        }

        return standards.sort();
    }
}

// Returns the selected standard
getSelectedStandard = function() {
    if(Session.get('selected_standard') != null) {
        if (Session.get('selected_standard').toUpperCase() == "ALL"){
            return "All";
        } else {
            return Session.get('selected_standard');
        }
    } else {
        return "Standard";
    }
}

// Returns the selected performance task
getSelectedPerformanceTask = function() {
    // Check if a performance task has been selected
    if(Session.get('selected_performance_task') != null) {
        /* Check if the selected performance task matches the subject that the user has currently selected.
         * If not, then set the selected performance task to null.
         */
        var ptID = Session.get('selected_performance_task'),
            selectedPerformanceTask = PerformanceTasks.findOne({ _id: ptID });

        if (ptID.toUpperCase() == "ALL"){
            return "All";
        } else {
            if (selectedPerformanceTask) {
                return selectedPerformanceTask.name;
            }
        }
    } else {
        return "Performance Task";
    }
}

// Returns the selected subject
getSelectedSubject = function() {
    if(Session.get('selected_subject') != null) {
        if (Session.get('selected_subject').toUpperCase() == "BOTH" | Session.get('selected_subject').toUpperCase() == "ALL"){
            return "All";
        } else if (Session.get('selected_subject').toUpperCase() == "ELA") {
            return "ELA";
        } else if (Session.get('selected_subject').toUpperCase() == "MATH") {
            return "Math";
        }
    } else {
        return "Subject";
    }
}

// Returns an array of the students in the current group/class
listGroupStudents = function() {

    function compare(a,b) {
        if (a.lastname < b.lastname)
            return -1;
        if (a.lastname > b.lastname)
            return 1;
        return 0;
    }

    return Students.find({ classroom_id: Session.get('selected_group')._id }, {
        firstname: 1,
        lastname: 1
    }).fetch().sort(compare);
}

// Returns a list of assessments related to a performance task
listPerformanceTaskAssessments = function(performanceTaskId) {
    var currentPT = PerformanceTasks.findOne({ _id: performanceTaskId }),
        assessmentScheduleList = currentPT.assessmentSchedule;

    if (currentPT) {
        for (var day in currentPT.assessmentSchedule) {
            if (currentPT.assessmentSchedule.hasOwnProperty(day)) {

                for (var assessmentIndex = 0; assessmentIndex < currentPT.assessmentSchedule[day].assessments.length; assessmentIndex++) {
                    if (currentPT.assessmentSchedule[day].assessments.hasOwnProperty(assessmentIndex)) {

                        if (currentPT.assessmentSchedule[day].assessments[assessmentIndex].assessment_id == null || currentPT.assessmentSchedule[day].assessments[assessmentIndex].assessment_id == "") {
                            currentPT.assessmentSchedule[day].assessments.splice(assessmentIndex, 1);
                            assessmentIndex--;
                        }
                    }
                }
            }
        }
    }

    return assessmentScheduleList;
}

// Returns a list of the possible values for an assessment
listAssessmentValues = function(assessmentId) {
    var currentAssessment = Assessments.findOne({ _id: assessmentId });

    if (currentAssessment) {
        return currentAssessment.assessValues;
    }
}

// Returns the date in a more aesthetically pleasing format
var formattedDate = function (date) {
    if (date) {
        var monthNames = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec"),
            d = date,
            hours = d.getHours(),
            minutes = d.getMinutes(),
            ampm = hours >= 12 ? 'PM' : 'AM';

        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;

        var strTime = hours + ':' + minutes + ' ' + ampm;

        return monthNames[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear() + " " + strTime;
    }

    return "";
};

var convertArrayIdsToString = function (list) {
    if(list != null) {
        var temp = "";
        for(var key in list){
            if(key != list.length - 1) {
                temp += list[key]._id + ", ";
            }else{
                temp += list[key]._id;
            }
        }
        return temp;
    }
    return null;
};

var convertArrayToString = function (list) {
    if(list != null) {
        var temp = "";
        for(var key in list){
            if(key != list.length - 1) {
                temp += list[key] + ", ";
            }else{
                temp += list[key];
            }
        }
        return temp;
    }
    return null;
};

var getSelectedGroupName = function () {
    var group = Session.get('selected_group');

    if (group) {
        return group.name;
    }
}

var setPerformanceTaskColor = function (subject) {
    // Assign green to ELA and purple to Math.
    // If a performance task does not have a color for some reason, it will be made grey.
    if (subject.toUpperCase() == "ELA") {
        return "#009900";
    } else if (subject.toUpperCase() == "MATH") {
        return "#8080c0";
    } else {
        return "#999999";
    }

    return "#999999";
};

var getReportOutput = function (callback) {
    var output = getListOfPerformanceTasks(), pts;
    if (output.length) {
        //if standard filter is not selected
        if (Session.get('selected_standard') === "All"){
            pts = [output[Session.get('details_page') - 1]];
        } else {
            //if a standard filter is selected we are going to have to loop through everything;
            pts = output;
            output = [];
        }
        pts.forEach(function(pt){
            // addAssessment is used if standard filter is selected
            var addAssessment = false;
            pt.noAssess = true;
            pt.assessmentSchedule.forEach(function (schedule, index) {
                pt.assessmentSchedule[index].hasAssessments = false;
                schedule.assessments.forEach(function (assessment, ind) {
                    pt.assessmentSchedule[index].assessments[ind].hasAssessments = false;
                    if (assessment.assessment_id) {
                        var assessmentObject = Assessments.findOne({_id: assessment.assessment_id});
                        var makeLoop = false;
                        assessmentObject.commonCoreStateStandards.forEach(function(standard){
                            if (standard._id === Session.get('selected_standard')){
                                makeLoop = true;
                                addAssessment = true;
                            }
                        });

                        if (makeLoop || Session.get('selected_standard') === 'All') {
                            assessmentObject.uniqueStudents = [];
                            assessmentObject.assessValues.forEach(function (assessmentValue, p) {
                                var isYesNoList = assessmentObject.type === "yesnolist";
                                assessmentValue.studentList = listGroupStudents();
                                assessmentValue.checkedCount = 0;
                                assessmentValue.studentList.forEach(function (student, qindex) {
                                    if (student.assessmentdata) {
                                        var thisPerformanceTaskId = pt._id,
                                            thisDayId = pt.assessmentSchedule[index].day,
                                            thisAssessmentId = pt.assessmentSchedule[index].assessments[ind]._id;
                                        if (
                                            student.assessmentdata[thisPerformanceTaskId.toString()] &&
                                            student.assessmentdata[thisPerformanceTaskId.toString()][thisDayId.toString()] &&
                                            student.assessmentdata[thisPerformanceTaskId.toString()][thisDayId.toString()][thisAssessmentId.toString()]
                                        ) {
                                            var studentObject = student,
                                                studentAssessment = student.assessmentdata[thisPerformanceTaskId.toString()][thisDayId.toString()][thisAssessmentId.toString()],
                                                studentAssessmentArray = studentAssessment.value,
                                                currentAssessmentValue = assessmentValue.value;
                                            if (assessment.timestamp) {
                                                if (studentAssessment.timestamp > assessment.timestamp) {
                                                    assessment.timestamp = studentAssessment.timestamp;
                                                }
                                            } else {
                                                assessment.timestamp = studentAssessment.timestamp;
                                            }

                                            if (studentAssessmentArray && studentAssessmentArray.indexOf("FLAG") > -1) {
                                                studentObject.flagged = true;
                                            } else {
                                                if (studentAssessmentArray && studentAssessmentArray.indexOf(currentAssessmentValue) > -1) {
                                                    studentObject.checked = true;
                                                    assessmentValue.checkedCount++;

                                                    // LS PAR-188: Counting number of students that have been assessed
                                                    if (assessmentObject.uniqueStudents.indexOf(student._id) == -1) {
                                                        assessmentObject.uniqueStudents.push(pt.assessmentSchedule[student._id]);
                                                    }
                                                    pt.noAssess = false;
                                                    pt.assessmentSchedule[index].assessments[ind].hasAssessments = true;
                                                    pt.assessmentSchedule[index].hasAssessments = true;
                                                }
                                            }
                                            if (isYesNoList) {
                                                studentAssessment.value.forEach(function (studAssVal) {
                                                    if (studAssVal.observation === "FLAG") {
                                                        student.flagged = true;
                                                    }

                                                    if (currentAssessmentValue == studAssVal.observation) {
                                                        if (studAssVal.isobserved == "yes") {
                                                            studentObject.yes = true;
                                                            assessmentValue.checkedCount++;
                                                            // LS PAR-188: Counting number of students that have been assessed
                                                            if (assessmentObject.uniqueStudents.indexOf(student._id) == -1) {
                                                                assessmentObject.uniqueStudents.push(student._id);
                                                            }
                                                            // END OF PAR-188

                                                            // Flags to show whether or not to include this part in the report output
                                                        } else if (studAssVal.isobserved == "no") {
                                                            studentObject.no = true;
                                                            assessmentValue.checkedCount++;
                                                            // LS PAR-188: Counting number of students that have been assessed
                                                            if (assessmentObject.uniqueStudents.indexOf(student._id) == -1) {
                                                                assessmentObject.uniqueStudents.push(student._id);
                                                            }
                                                            // END OF PAR-188
                                                        }
                                                        pt.noAssess = false;
                                                        pt.assessmentSchedule[index].assessments[ind].hasAssessments = true;
                                                        pt.assessmentSchedule[index].hasAssessments = true;

                                                    }
                                                })
                                            }
                                        }
                                    }
                                    assessmentValue.studentList[qindex] = studentObject;

                                });
                                assessmentObject.assessValues[p] = assessmentValue;
                            });
                        }
                        pt.assessmentSchedule[index].assessments[ind].assessmentObject = assessmentObject;
                    }
                })
            });
            if (Session.get('selected_standard') === "All" && !pt.noAssess) {
                output[Session.get('details_page') - 1] = pt;
            }
            else if (Session.get('selected_standard') === "All" && pt.noAssess){
                output.splice(Session.get('details_page')-1, 1);
            }
            else if (addAssessment === true && !pt.noAssess) {
                output.push(pt);
            }
        });
    }
    console.log(output);
    Session.set('details_num_pages', output.length);
    Session.set('output', output);
    if (output.length <= 0) {
        $.bootstrapGrowl("There are no assessments for your selection.", {
            type: 'warning',
            align: 'center',
            width: 'auto',
            allow_dismiss: true,
            delay: 4000
        });
    }
    callback();
    return output[Session.get('details_page') - 1];

};

resizeClassDetailsTables = function () {
    // Set the width of each table
    setTimeout( function() { $('.class-details-table').width(listGroupStudents().length * 80 + 400); } , 0);
};

updateDetailsPage = function (pageNum) {
    if (pageNum != Session.get('details_page')) {
        Session.set('loading_report', true);
        setTimeout(function () {
            Session.set('details_page', pageNum);
        }, 100);
    }
};

replaceWithBookIcon = function (a) {
    if (!a.image) {
        $(a).replaceWith("<i class='fa fa-2x fa-book white-text'></i>");
    }
};

// Returns true if the user is using a tablet or phone.
mobileAndTabletcheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};










/** jQuery **/

$(document).ready(function(){

    $( window ).resize(function() {
        // Set height of the report output view
        var offset = $('#parcc-nav').height() + $('#class-details-filter-bar').height() + $('#class-details-title-bar').height();
        $( "#class-details-page-content" ).height($(window).height() - offset);
    });
});
