// Parcc glossary admin -- client

////////// Variables //////////

// Default when add new glossary term
var defaultAdd = {
    _id: "",
    term: "",
    definition: ""
};

// Default currently selected glossary term
Session.setDefault('selected_glossary_term', null);

/*** Template: glossary ***/

////////// UI Display //////////

Template.glossary.helpers({
    selectedGlossaryTerm: function () {
        return Session.get("selected_glossary_term");
    },

    // Determine if should show glossary term form
    showGlossaryForm: function () {
        return Session.get("selected_glossary_term") !== null;
    }
});

// Remove CKEDITOR instances when you leave the page
Template.glossary.onDestroyed(function(){
    for (var name in CKEDITOR.instances) {
        if (CKEDITOR.instances.hasOwnProperty(name) && CKEDITOR.instances[name]){
            CKEDITOR.instances[name].destroy()
        }
    }
});

/*** Template: glossaryTermsList ***/

////////// UI Display //////////

Template.glossaryTermsList.helpers({
    // Get the list of glossary terms
    glossaryTerms: function () {
        return Glossary.find({}, {sort: {term: 1}});
    },

    // Sets active glossary term
    isActiveGlossaryTerm: function () {
        return Session.get('selected_glossary_term') !== null && Session.get('selected_glossary_term')._id === this._id  ? "active" : "";
    }
});


////////// Events //////////

Template.glossaryTermsList.events({
    'click #select-glossary-term': function (evt, template) {
        evt.stopPropagation();

        // Set the selected glossary term
        Session.set('selected_glossary_term', this);
        if (CKEDITOR.instances['text-glossary-definition']){
            //CKEDITOR.instances['text-glossary-definition'].setData(this.definition);
            $("#cke_text-glossary-definition iframe").contents().find("body").html(this.definition);
            $("#glossary-container").scrollTop(0);
        }
    },
    'click #add-glossary-term': function (evt) {
        // Default glossary term when adding1 a new one
        evt.stopPropagation();
        Session.set('selected_glossary_term', defaultAdd);
        if (CKEDITOR.instances['text-glossary-definition']){
            //CKEDITOR.instances['text-glossary-definition'].setData('New Term');
            $("#cke_text-glossary-definition iframe").contents().find("body").html("");
            $("#glossary-container").scrollTop(0);
        }
    },
    'click #delete-glossary-term': function () {
        var deleteTerm = this;

        bootbox.confirm("Are you sure you want to delete the glossary term, " + deleteTerm.term + "?", function(result) {
            if (result === true) {
                // Remove glossary term
                Meteor.call('removeGlossaryTerm', deleteTerm, function(err, glossaryTerm) {
                    Session.set("selected_glossary_term", null);
                });

                console.log("Remove Glossary Term");
            }
        });
    }
});


/*** Template: glossaryTermForm ***/

////////// UI Display //////////

Template.glossaryTermForm.helpers({
    // Set the form mode (add or edit)
    glossaryTermMode: function () {
        return this !== null && this._id !== null && this._id !== "" ? "Edit" : "Add";
    }
});

////////// Events //////////

Template.glossaryTermForm.events({
    'submit form': function (evt, template) {
        evt.preventDefault();

        var glossaryTermData = Session.get('selected_glossary_term') !== null ? Session.get('selected_glossary_term') : {};

        // Update data from form values 
        glossaryTermData.term = template.find('input[name=glossaryTerm]').value;
        glossaryTermData.definition = template.find('textarea[name=textareaGlossaryDefinition]').value;

        if (Session.get('selected_glossary_term') !== null) {
            if (Session.get('selected_glossary_term')._id !== null && Session.get('selected_glossary_term')._id !== "") {
                delete glossaryTermData._id;

                Meteor.call('updateGlossaryTerm', Session.get("selected_glossary_term")._id, glossaryTermData, function (err, glossary) {
                    if (err) {
                        showBootstrapAlert("danger", err.reason);

                    } else {
                        if (glossary == "missingGlossaryData") {
                            showBootstrapAlert("danger", "Please fill out all fields.");
                        } else if (glossary) {
                            showBootstrapAlert("success", glossaryTermData.term + " has been updated.");
                            Session.set("selected_glossary_term", null);

                        } else {
                            showBootstrapAlert("danger", "This glossary term could not be updated.");
                        }
                    }
                });

                console.log("Update Glossary Term");
            } else {
                glossaryTermData._id = new Meteor.Collection.ObjectID()._str;

                // Add new glossary term to the database
                Meteor.call('addGlossaryTerm', glossaryTermData, function(err, glossary) {
                    if (err) {
                        showBootstrapAlert("danger", err.reason);

                    } else {
                        if (glossary == "missingGlossaryData") {
                            showBootstrapAlert("danger", "Please fill out all fields.");

                        } else if (glossary) {
                            showBootstrapAlert("success", glossaryTermData.term + " has been added.");
                            Session.set("selected_glossary_term", null);

                        } else {
                            showBootstrapAlert("danger", "This glossary term could not be added.");
                        }
                    }
                });

                console.log("Add Glossary Term");
            }
        }
     }
});

Template.glossaryTermForm.rendered = function () {
    CKEDITOR.replace('text-glossary-definition', {
        toolbarGroups: [
            {"name":"basicstyles","groups":["basicstyles"]},
            {"name":"document","groups":["mode"]},
            {"name":"styles","groups":["styles"]}
        ]
    });
};
