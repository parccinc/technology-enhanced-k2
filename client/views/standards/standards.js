// Parcc standard admin -- client

////////// Variables //////////

// Default when add new standard
var defaultAdd = {
    _id: "",
    grade: "K",
    subject: "ela",
    description: ""
};


// Default selected standard
Session.setDefault('selected_standard', null);

// Default current filters for the session
Session.setDefault('standards_filters', {grade: "K", subject: "ela"});


/*** Template: standards ***/

////////// UI Display //////////

Template.standards.helpers({
    selectedStandard: function () {
        return Session.get("selected_standard");
    },

    // Determine if should show standard form
    showStandardsForm: function () {
        return Session.get("selected_standard") != null;
    }
});


/*** Template: standardsFilter ***/

////////// UI Display //////////

Template.standardsFilter.helpers({
    isGradeFilterSelected: function (grade) {
        return Session.get("standards_filters") != null && Session.get("standards_filters").grade == grade;
    },

    isSubjectFilterChecked: function (subject) {
        return Session.get("standards_filters") !== null && Session.get("standards_filters").subject == subject;
    }
});


////////// Events //////////

Template.standardsFilter.events({
    'click #filter-standards': function (evt, template) {
        // Filter the standards by grade and subject
        var filterGrade = template.find('select[name=filterGrade]').value,
            filterSubject = template.find('input:radio[name=filterSubject]:checked').value;

        Session.set('standards_filters', {grade: filterGrade, subject: filterSubject});
        Session.set('selected_standard', null);
    }
});




/*** Template: standardsList ***/

////////// UI Display //////////

Template.standardsList.helpers({
    // Get the list of standards
    standards: function () {
        return CommonCoreStateStandards.find(Session.get("standards_filters"), {sort: {_id: 1}});
    },

    // Sets active standards
    isActiveStandard: function () {
        return Session.get('selected_standard') != null && Session.get('selected_standard')._id == this._id  ? "active" : "";
    }
});


////////// Events //////////

Template.standardsList.events({
    'click #select-standard': function (evt, template) {
        evt.stopPropagation();

        // Set the standard
        Session.set('selected_standard', this);
    },
    'click #add-standard': function () {
        // Default standard when adding a new one
        Session.set('selected_standard', defaultAdd);
    },
    'click #delete-standard': function () {
        var deleteStandard = this;

        bootbox.confirm("Are you sure you want to delete the standard, " + deleteStandard._id + "?", function(result) {
            if (result === true) {
                // Remove standard
                Meteor.call('removeCommonCoreStateStandard', deleteStandard._id);
            }

            return;
        });
    }
});




/*** Template: standardsForm ***/

////////// UI Display //////////

Template.standardsForm.helpers({
    // Set the form mode (add or edit)
    standardMode: function () {
        return this != null && this._id != null && this._id != "" ? "Edit" : "Add";
    },

    // Set the form mode (add or edit)
    isEditMode: function () {
        return this != null && this._id != null && this._id != "" ? true : false;
    },

    isGradeSelected: function (grade) {
        return this != null && this.grade == grade;
    },

    isSubjectChecked: function (subject) {
        return this != null && this.subject == subject;
    }
});

////////// Events //////////

Template.standardsForm.events({
    'submit form': function (evt, template) {
        evt.preventDefault();

        var standardData = Session.get('selected_standard') != null ? Session.get('selected_standard') : {};

        // Update data from form values 
        standardData.grade = template.find('select[name=grade]').value;
        standardData.subject = template.find('input:radio[name=subject]:checked').value;
        standardData.description = template.find('input[name=standardDescription]').value;

        if (Session.get('selected_standard') != null) {
            if (Session.get('selected_standard')._id != null && Session.get('selected_standard')._id != "") {
                delete standardData._id;

                Meteor.call('updateCommonCoreStateStandard', Session.get('selected_standard')._id, standardData, function (err, standard) {
                    // LS PAR-133: Validate user input
                    if (err) {
                        showBootstrapAlert("danger", err.reason);
                    } else {
                        if (standard) {
                            showBootstrapAlert("success", Session.get('selected_standard')._id + " has been updated.");
                            Session.set('selected_standard', null);
                        } else {
                            showBootstrapAlert("danger", "Please fill out every field.");
                        }
                    }
                    // END OF PAR-133
                });

                console.log("Update Standard");
            } else {
                standardData._id = template.find('input[name=standardId]').value;

                if (CommonCoreStateStandards.find({_id: standardData._id}).count() > 0) {
                    showBootstrapAlert("danger", "A standard with this name already exists.");
                } else {
                    Meteor.call('addCommonCoreStateStandard', standardData, function (err, standard) {
                        // LS PAR-133: Validate user input
                        if (err) {
                            showBootstrapAlert("danger", err.reason);
                        } else {
                            if (standard) {
                                showBootstrapAlert("success", standardData._id + " has been added.");
                                Session.set('selected_standard', null);
                            } else {
                                showBootstrapAlert("danger", "Please fill out every field.");
                            }
                        }
                        // END OF PAR-133
                    });
                    console.log("Add Standard");
                }
            }
        }
     }
});
