// Parcc header -- client

////////// Variables //////////

// User Roles
CONTENT_ADMIN = "Content Administrator (Curator)",
MEMBER_EDUCATOR = "PARCC-Member Educator";

// Default currently selected performance task group
Session.setDefault('selected_group', null);

// Default currently selected grade
Session.setDefault('selected_grade', null);

// Default connection status
var disconnected = false;

// Hard coded admin users
var adminUsernames = ["teacherk@yahoo.com",
                      "teacherk@k.com",
                      "teacherg1@yahoo.com",
                      "teacherg2@yahoo.com",
                      "edccm@yahoo.com",
                      "centercm@yahoo.com",
                      "parcccm@yahoo.com",
                      "k2contentadmin1@yahoo.com",
                      "k2contentadmin2@yahoo.com"];

////////// UI Display //////////

Template.header.helpers({
    // Determines if a group is selected.
    isGroupSelected: function () {
        return Session.get('selected_group') != null;
    },

    // Display the selected group
    selectedGroupName: function () {
        return Session.get('selected_group') != null ? Session.get('selected_group').name : "Select a class or a group";
    },

    // Display the classrooms + groups + groups shared with the logged in user.
    groups: function () {
        var groupData = [];

        if (Meteor.user() && Meteor.user().profile) {
            // Classrooms
            if (Meteor.user().profile.classrooms) {
                $.merge(groupData, Meteor.user().profile.classrooms);
            }

            // Groups
            if (Meteor.user().profile.groups) {
                $.merge(groupData, Meteor.user().profile.groups);
            }

            // Shared Groups
            if (Meteor.user().profile.sharedgroups) {
                for (var i = 0; i < Meteor.user().profile.sharedgroups.length; i++) {
                    var user = Meteor.users.findOne({ 'profile.groups': { $elemMatch: { _id: Meteor.user().profile.sharedgroups[i]._id } } });

                    if (user) {
                        var sharedGroup = $.grep(user.profile.groups, function(e){ return e._id === Meteor.user().profile.sharedgroups[i]._id; });
                        $.merge(groupData, sharedGroup);
                    }
                }
            }
        }

        return groupData;
    },

    // Get the name of the signed in user
    signedInUser: function () {
        return Meteor.user().profile.firstName + " " + Meteor.user().profile.lastName;
    },

    // Get the currently selected performance task name
    performanceTask: function () {
        if (Session.get('selected_performancetask')!= null) {
            return Session.get('selected_performancetask').name;
        }

        return "";
    },

    // Determine if breadcrumb should be displayed
    displayBreadcrumb: function () {
        var currentRoute = Router.current();
        if (!currentRoute) return false;

        return Meteor.user() && currentRoute.route.getName() == 'studentassessment';
    },

    // Determine if user is on the student details page
    goToDashboard: function () {
        var currentRoute = Router.current();
        if (!currentRoute || !currentRoute.route) return false;

        return Meteor.user() && currentRoute.route.getName() != 'studentDetails' && currentRoute.route.getName() != 'classDetails' && currentRoute.route.getName() != 'journal';
    },

    // Determine if admin functionality should be displayed based on username logged in
    displayAdminFunctions: function () {
        return adminUsernames.indexOf(Meteor.user().emails[0].address) != -1 || Roles.userIsInRole(Meteor.user(), CONTENT_ADMIN);
    },

    // Determines whether the application is online
    isOnline: function () {
        if (!Meteor.status().connected && !disconnected) {
            showBootstrapAlert("info", "You are now working offline.");
            disconnected = true;
        } else if (Meteor.status().connected && disconnected === true) {
            disconnected = false;
            showBootstrapAlert("info", "Connection reestablished.");
        }

        return Meteor.status().connected;
    },

    // Determine if using SAML login
    isSaml: function () {
        if (Meteor.settings.public.environment === "local") {
            return false;
        }

        return true;
    }
});

////////// Events //////////

Template.header.events({
    'click #select-group': function () {
        // Set (1) the selected group (2) the subject back to ela (default) for the dashboard
        // and (3) the grade to the selected group's grade
        Session.set("selected_group", this);
        /* LS PAR-143: Updated this because I am using 'selected_subject' in the class report filter.
                       Angie said that this should not be a problem.

        Session.set("selected_subject", "ela");
         */
        Session.set("selected_grade", this.grade);
        // LS PAR-211: Set the selected student to null when the group changes
        Session.set("selected_student", null);
        // END OF PAR-211
    },
    'click #go-to-classrooms': function () {
        Session.set("selected_classroom", null);
        Session.set("edited_classroom", null);
        Session.set("edited_student", null);
    },
    'click #go-to-groups': function () {
        Session.set("admin_selected_group", null);
        Session.set("admin_selected_students", null);
        Session.set("admin_selected_classroom", null);
        Session.set("admin_selected_group_student", null);
    },
    'click #logout-user': function (event) {
        event.preventDefault();

        if (Meteor.settings.public.environment === "local") {
            Router.go('entrySignOut');
        } else {
            Meteor.logoutWithSaml({
                provider:"onelogin"
            }, function(error, result){
                if (error) {
                    console.log(error);
                }
                Session.set("selected_group", null);
            });

            Router.go('home');
        }
    },
    'click #login-user': function (event) {
        event.preventDefault();

        Meteor.loginWithSaml({
            provider:"onelogin"
        }, function(error, result){
            if (error) {
                console.log(error);
            }
            console.log(result);
        });
    }
});











