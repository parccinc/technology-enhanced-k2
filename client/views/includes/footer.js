/*** Template: footer ***/

////////// UI Display //////////

Template.footer.helpers({
    // Checks to see if the current location within the application
    // is the student assessment area
    isStudentAssessment: function () {
        var currentRoute = Router.current();
        if (!currentRoute) return false;

        return currentRoute.route.getName() == 'studentassessment';
    },

    // Return the performance task instruction data
    performanceTaskInstruction: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").instructions;
        }

        return null;
    },

    // Get the student resources file for the selected peformance task
    getStudentResources: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").studentResource;
        }

        return "";
    },

    // Get the color assigned to the selected peformance task
    getTaskColor: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").color;
        }

        return "";
    },

    // Determine if the currently selected performance task is an ela task
    isSubjectEla: function () {
        if (Session.get("selected_performancetask") != null) {
            return Session.get("selected_performancetask").subject == 'ela';
        }

        return false;
    }
});


////////// Events //////////

Template.footer.events({
    'click .instruction-popover': function (evt, template) {
        $('.instruction-popover')
            .popover()
            // Functionality to perform when user clicks footer instruction
            .click(function(e) {
                var taskColor = Session.get("selected_performancetask").color; // Get the color of the currently selected performance task

                // Show the popover
                $(this).popover('show');

                // Set the popover border color to the task color
                $('.popover-content').css({"border-color": taskColor});

                // Set the popover title background to the task color
                $('.popover-title').css({"background-color": taskColor});

                // Add an "x" close button to the popover title bar
                $('.popover-title').append('<button type="button" class="popover-close close">&times;</button>');

                // Add the close functionality to hide the popover when the close "x" is clicked
                $('.close').click(function(e){
                    $('.instruction-popover').popover('hide');
                });

                // Prevents any further default click actions from occurring
                e.preventDefault();
            })
            // Functionality to perform when footer instruction popover is shown
            .on('shown.bs.popover', function (e) {
                var anchorId = null,
                    selectedDay = Session.get("selected_day"), // Get the selected performance task day
                    selectedAssessment = Session.get('selected_assessment');  // Get the selected performance task assessment

                if (selectedAssessment) {
                    console.log(Session.get('selected_assessment'));
                    anchorId = "#" + selectedAssessment.step.replace( /(:|\.|\[|\])/g, "\\$1" );
                } else if (selectedDay) {
                    console.log(Session.get('selected_day'));
                    anchorId = "#Day" + selectedDay;
                }

                if (anchorId) {
                    // If an anchor is found for the step then scroll the popover content to that anchor
                    if ($('.popover-content').find(anchorId) && $('.popover-content').find(anchorId).position()) {
                        $('.popover-content').animate({scrollTop: $('.popover-content').find(anchorId).position().top-50}, '500', 'swing', function() {
                            // Once the scrolling animation effect is complete, clear the queue so it does not continue to fire the animation
                            // multiple times
                            $('.popover-content').clearQueue();
                        });

                    }
                }
            });
    }
});











