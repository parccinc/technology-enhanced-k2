// Parcc classroom admin -- client

////////// Variables //////////

// Default when add new classroom
var defaultAdd = {
    _id: "",
    name: "",
    grade: ""
};

// Default currently selected classroom
Session.setDefault('selected_classroom', null);

// Default student currently being edited
Session.setDefault('edited_student', null);


////////// Helpers for in-place editing //////////

// Returns an event map that handles the "escape" and "return" keys and
// "blur" events on a text input (given by selector) and interprets them
// as "ok" or "cancel".
var okCancelEvents = function (selector, callbacks) {
    var ok = callbacks.ok || function () {};
    var cancel = callbacks.cancel || function () {};

    var events = {};
    events['keyup '+selector+', keydown '+selector+', focusout '+selector] =
        function (evt) {
            if (evt.type === "keydown" && evt.which === 27) {
                // escape = cancel
                cancel.call(this, evt);

            } else if (evt.type === "keyup" && evt.which === 13 ||
                evt.type === "focusout") {
                // blur/return/enter = ok/submit if non-empty
                var value = String(evt.target.value || "");
                if (value)
                    ok.call(this, value, evt);
                else
                    cancel.call(this, evt);
            }
        };

    return events;
};



/*** Template: classroom ***/

////////// UI Display //////////

Template.classroom.helpers({
    // Display the classrooms associated with the logged in teacher.
    classrooms: function () {
        if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.classrooms) {
            return Meteor.user().profile.classrooms;
        }

        return [];
    },

    // Sets active classroom
    isActiveClassroom: function () {
        return Session.get("selected_classroom") !== null && Session.get("selected_classroom")._id === this._id ? "active" : "";
    },

    // Indicate if a classroom is selected
    classroomIsSelected: function () {
        return Session.get("selected_classroom") !== null;
    }
});

////////// Events //////////

Template.classroom.events({
    // Add a new classroom
    'click #add-classroom': function (evt, template) {
        // Default classroom when adding a new one
        Session.set('selected_classroom', defaultAdd);
    },
    // User has selected a classroom from the classroom list menu
    'click #select-classroom': function (evt, template) {
        evt.stopPropagation();

        // Set classroom being selected when a classroom is clicked on
        Session.set('selected_classroom', this);
    },
    // User wants to view the details for a classroom
    'click #view-classroom-details': function (evt, template) {
        evt.preventDefault();

        // Set selected group for the details view
        Session.set('selected_group', this);

        // Set the selected student for the details view
        Session.set('selected_student', null);

        // Go to the student details page
        Router.go('studentDetails');
    },
    // User wants to delete a classroom
    'click #remove-classroom': function () {
        var selectedClassroom = this,
            deletedId = this._id;

        bootbox.confirm("Are you sure you want to delete classroom, " + selectedClassroom.name + "?", function(result) {
            if (result === true) {
                // Remove classroom
                Meteor.call('removeUserClassroom', selectedClassroom, function(err, classroom) {
                    Session.set("selected_classroom", null);

                    if (deletedId === Session.get("selected_group")._id) {
                        Session.set("selected_group", null);
                    }
                });

                console.log("Remove User Classroom");
            }

            return;
        });
    },

    'click #clear-data': function () {
        Meteor.call('clearUserData', function(err, classroom) {
         console.log("done");
        });
    }
});



/*** Template: student ***/

////////// UI Display //////////

Template.student.helpers({
    // Get the name of the selected classroom
    selectedClassroomName: function () {
        return Session.get("selected_classroom").name;
    },

    // Get the grade of the selected classroom
    isClassroomGradeSelected: function (grade) {
        return Session.get("selected_classroom").grade === grade ? 'selected' : '';
    },

    // Determine if the user can select a classroom grade
    isClassroomGradeEnabled: function () {
        return Session.get("selected_classroom")._id.trim().length > 0 ? "disabled" : '';
    },

    // Determine if form is in edit mode
    isClassroomEditable: function () {
        return Session.get("selected_classroom")._id.trim().length > 0;
    },

    // Display the students associated with the classroom selected.
    students: function () {
        if (Session.get("selected_classroom") !== null) {
            return Students.find({classroom_id: Session.get("selected_classroom")._id}, {sort: {lastname: 1, firstname: 1}});
        }

        return [];
    },

    // Indicate if a student is in edit mode
    editingStudent: function () {
        return Session.get("edited_student") !== null && Session.get("edited_student")._id === this._id;
    }
});

////////// Events //////////

Template.student.events({
    // User clicks the view details icon for a student
    'click #view-student-details': function(evt, template) {
        evt.preventDefault();

        // Set selected group for the details view
        Session.set('selected_group', Session.get("selected_classroom"));

        // Set the selected student for the details view
        Session.set('selected_student', this);

        // Go to the student details page
        Router.go('studentDetails');
    },
    // User clicks on the edit icon to modify a student
    'click #edit-classroom-student': function (evt, template) {
        evt.stopPropagation();

        // Set student being edited when a student is double clicked on
        Session.set('edited_student', this);
    },
    // User clicks on the delete icon to delete a student
    'click #remove-classroom-student': function (evt, template) {
        evt.stopPropagation();

        var deleteStudent = this;

        bootbox.confirm("Are you sure you want to delete " + deleteStudent.firstname + " " + deleteStudent.lastname + "?", function(result) {
            if (result === true) {
                // Remove student
                Meteor.call('removeStudent', deleteStudent._id);

                console.log("Remove Classroom Student");
            }

            return;
        });
    },
    //User clicks the update student button to save their changes
    'click #update-student': function (evt, template) {
        var studentData = Session.get("edited_student");

        if (studentData != null) {
            studentData.firstname = template.find('input[name=txtEditFirstName]').value;
            studentData.lastname = template.find('input[name=txtEditLastName]').value;
            delete studentData._id;

            Meteor.call('updateStudent', Session.get("edited_student")._id, studentData, function (err, student) {
                Session.set('edited_student', null);
            });

            console.log("Edit Classroom Student");
        }
    },
    // User clicks the cancel update button to cancel making student changes
    'click #cancel-update': function () {
        Session.set('edited_student', null);
    },
    // User clicks the add student button to save a new student
    'submit #add-student-form':function (evt, template) {
        evt.preventDefault();

        var inputFirstName = template.find('input[name=txtAddFirstName]'),
            inputLastName = template.find('input[name=txtAddLastName]'),
            studentData = {
                classroom_id: Session.get("selected_classroom")._id,
                firstname: inputFirstName.value,
                lastname: inputLastName.value
            };

        // When pressing enter on the add student input, save the new student
        // LS PAR-126: Adding validation on student first and last name input on both the client and server
        if (studentData.firstname && studentData.firstname.trim() !== "") {
            Meteor.call('addStudent', studentData, function (err, classroom) {
                if (classroom) {
                    inputFirstName.value = '';
                    inputLastName.value = '';
                    inputFirstName.focus();
                } else {
                    showBootstrapAlert("danger", "Students must have a first name.");
                }
                console.log(classroom);
            });
        }
        // END OF PAR-126

        console.log("Add Classroom Student");
    },
    // User clicks the update classroom button to save classroom name changes
    'submit #save-classroom-form': function (evt, template) {
        evt.preventDefault();

        var classroomData = Session.get('selected_classroom'),
            inputName = template.find('input[name=classroomName]'),
            selectGrade = template.find('select[name=classroomGrade]');

        classroomData.name = inputName.value;
        classroomData.grade = selectGrade.value;

        // LS PAR-126: Adding validation to prevent blank classroom names
        if (classroomData.name && classroomData.name.trim() !== ""
            && classroomData.grade && classroomData.grade.trim() !== "") {

            if (classroomData._id.trim().length === 0) {
                // Add classroom
                classroomData._id = new Meteor.Collection.ObjectID()._str;

                // save the new classroom
                Meteor.call('addUserClassroom', classroomData, function(err, classroom) {
                    if (err) {
                        showBootstrapAlert("danger", err.reason);
                        return;
                    }

                    if (classroom === "blankValue") {
                        showBootstrapAlert("danger", "Please enter a classroom name and select a grade.");
                        return;
                    } else if (classroom === "classroomExists") {
                        showBootstrapAlert("danger", "\"" + classroomData.name.trim() + "\" is already in use.");
                        return;
                    }

                    Session.set("selected_classroom", classroomData);
                });

            } else {
                // Update classroom
                Meteor.call('editUserClassroom', classroomData, function (err, classroom) {
                    // Handle any errors from the server
                    if (err) {
                        showBootstrapAlert("danger", err.reason);
                        return;
                    }

                    if (classroom === "blankValue") {
                        showBootstrapAlert("danger", "Classroom name and grade cannot be blank.");
                        return;
                    }

                    Session.set("selected_classroom", null);
                });
            }

        } else {
            showBootstrapAlert("danger", "Classroom name and grade cannot be blank.");
        }
        // END OF PAR-126
    },
    // User clicks the upload classroom button and is taken to the upload modal
    'click #upload-classroom-data': function () {
        if (mobileAndTabletcheck()) {
            bootbox.alert("Please access this page from a desktop computer or laptop to download a .csv file.", function () {
            });
        } else {
            $('#uploadDataModal').modal("show");
        }
    }
});

/*** Template: uploadDataModal ***/

////////// Events //////////

Template.uploadDataModal.events({
    // Allows a user to select 1->n csv text files for upload of
    // student data to be added to the database
    'change #files': function (evt, template) {
        var files = evt.target.files || evt.dataTransfer.files;
        for (var i = 0, file; file = files[i]; i++) {
            //if (file.type.indexOf("text") === 0) {
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    var text = e.target.result,
                        all = $.csv.toObjects(text),
                        badFormat = false;
                    if (!all) {
                        //alert("Error parsing file");
                        bootbox.alert("Error parsing file.", function() {});
                        return
                    }
                    _.each(all, function (entry) {
                        if (!badFormat) {
                            if (entry.firstname === undefined || entry.lastname === undefined) {
                                //alert("Your file is missing columns.  Please download the sample classroom file to see the file format required.");
                                bootbox.alert("Your file is missing columns.  Please download the sample classroom file to see the file format required.", function() {});
                                badFormat = true;
                                return;
                            }
                            var studentData = {
                                classroom_id: Session.get("selected_classroom")._id,
                                firstname: entry.firstname,
                                lastname: entry.lastname
                            };
                            if (studentData.firstname.trim().length > 0 || studentData.lastname.trim().length > 0) {
                                Meteor.call('addStudent', studentData, function(err, classroom) {
                                });
                            }
                        }
                    });

                    if (!badFormat) {
                        template.find('input[id=files]').value  = "";
                        $('#uploadDataModal').modal("hide");
                    }
                }
                reader.readAsText(file);
            }
        //}

    }
});

