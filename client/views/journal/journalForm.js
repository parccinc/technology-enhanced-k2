// Parcc user journal form -- client

////////// Variables //////////
var formatTimestamp = function (ts) {
    if (ts) {
        var monthNames = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec"),
            d = ts,
            hours = d.getHours(),
            minutes = d.getMinutes(),
            ampm = hours >= 12 ? 'PM' : 'AM';

        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;

        var strTime = hours + ':' + minutes + ' ' + ampm;

        return monthNames[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear() + " " + strTime;
    }

    return "";
}

var saveJournal = function (evt, template) {
    if (Session.get('selected_journal_entry') != null) {
        var journalEntryData = Session.get('selected_journal_entry');

        // Update data from form values 
        journalEntryData.class_info = template.find('textarea[name=textareaJournalQuestion1]').value;
        journalEntryData.strengths_needs_info = template.find('textarea[name=textareaJournalQuestion2]').value;
        journalEntryData.actions_info = template.find('textarea[name=textareaJournalQuestion3]').value;
        journalEntryData.comments = template.find('textarea[name=textareaJournalComments]').value;
        journalEntryData.timestamp = new Date();

        delete journalEntryData.name;

        if (Session.get('selected_journal_entry')._id != null && Session.get('selected_journal_entry')._id != "") {
            Meteor.call('editUserJournalEntry', journalEntryData, function (err, journal) {
                if (journal) {
                    showBootstrapAlert("success", Session.get('selected_journal_entry').name + " retrospective Journal entry modified.");
                    Session.set('selected_journal_entry', null);
                } else {
                    showBootstrapAlert("danger", "Could not add this retrospective journal entry.");
                }
            });

            console.log("Update Journal Entry");
        } else {
            journalEntryData._id = new Meteor.Collection.ObjectID()._str;

            // Add new journal entry to the database
            Meteor.call('addUserJournalEntry', journalEntryData, function(err, journal) {
                if (journal) {
                    showBootstrapAlert("success", Session.get('selected_journal_entry').name + " retrospective Journal entry added.");
                    Session.set('selected_journal_entry', null);
                } else {
                    showBootstrapAlert("danger", "Could not add this retrospective journal entry.");
                }
            });

            console.log("Add Journal Entry");
        }
    }
}

var generateJournalPdf = function() {

    // Create a new document
    pdf_doc = new jsPDF('portrait', 'mm', 'letter');

    /** Declare/initialize variables **/
    var margin = 15,
        pageHeight = pdf_doc.internal.pageSize.height - (2 * margin),
        pageWidth = pdf_doc.internal.pageSize.width - (2 * margin),
        firstLine = margin + 5,
        firstIndent = margin,
        secondIndent = firstIndent + 10,
        thirdIndent = secondIndent + 10,
        assessmentLineMaxChar = 90,
        rubricRectWidth = 35,
        rubricRectHeight = 6,
        rectCornerRadius = 1,
        largeFontSize = 16,
        smallFontSize = 10,
        currentPage = 0,
        greyedOutTextColor = "#7B7B7B",
        onePagePerPerformanceTask = true,
        renderPdfInColor = false,
        journalData = Session.get('selected_journal_entry'),
        groupName = Session.get('selected_group').name || "";

    function drawPageFooter() {
        pdf_doc.setFontSize(smallFontSize);
        pdf_doc.setTextColor(0);
        pdf_doc.text(firstIndent, pageHeight + (margin + 3), "Last Modified: " + formatTimestamp(journalData.timestamp));

        currentPage++;
        pdf_doc.text(pageWidth + (margin / 2), pageHeight + (margin + 3), currentPage.toString());
    }

    var addNewPdfPage = function() {
        pdf_doc.addPage();
        currentLine = firstLine;

        drawPageFooter();
    }

    var checkNeedNewPage = function () {
        // Check if there is still space on current page
        if (currentLine >= pageHeight) {
            addNewPdfPage();
        }
    }

    // Draw dividing line.
    var drawDividingLine = function () {
        currentLine += 3;
        pdf_doc.setDrawColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
        pdf_doc.line(thirdIndent, currentLine, 192, currentLine);
    };

    // Draw footer details on first page
    drawPageFooter();

    // Add journal info to the header
    var currentLine = firstLine;
    if(renderPdfInColor) {
        pdf_doc.setDrawColor(0);
        pdf_doc.setFillColor(hexToRgb("#cccccc")[0], hexToRgb("#cccccc")[1], hexToRgb("#cccccc")[2]);
        pdf_doc.rect(firstIndent - 2, currentLine + 2, pageWidth, -8, 'F');
    } else {
        pdf_doc.setLineWidth(.5);
        pdf_doc.setDrawColor(0);
        pdf_doc.line(firstIndent, currentLine + 2, pageWidth + 9, currentLine + 2);
        pdf_doc.setLineWidth(0.1);
    }

    pdf_doc.setFontSize(largeFontSize);
    pdf_doc.setFontType("bold");
    pdf_doc.setTextColor(0);
    var journalHeader = pdf_doc.splitTextToSize(journalData.name + " Retrospective Journal for " + groupName, pageWidth-margin);
    pdf_doc.text(firstIndent, currentLine, journalHeader);

    // Add journal data
    pdf_doc.setFontSize(smallFontSize);

    // Question 1
    var splitQ1 = pdf_doc.splitTextToSize("What do I know about my class because of this performance task (including strengths/needs of the class relative to specific CCSS, as well as general information learned about my students)?", pageWidth-margin);
    console.log(splitQ1);
    pdf_doc.setFontType("bold");
    pdf_doc.text(firstIndent, currentLine += 10, splitQ1);

    checkNeedNewPage();

    var splitQ1Answer = pdf_doc.splitTextToSize(journalData.class_info, pageWidth-margin);
    pdf_doc.setFontType("normal");
    pdf_doc.text(firstIndent, currentLine += 10, splitQ1Answer);

    checkNeedNewPage();

    // Question 2
    var splitQ2 = pdf_doc.splitTextToSize("What do I know about the strengths and needs of individuals or groups of students relative to specific CCSS?", pageWidth-margin);
    pdf_doc.setFontType("bold");
    pdf_doc.text(firstIndent, currentLine += 15, splitQ2);

    checkNeedNewPage();

    var splitQ2Answer = pdf_doc.splitTextToSize(journalData.strengths_needs_info, pageWidth-margin);
    pdf_doc.setFontType("normal");
    pdf_doc.text(firstIndent, currentLine += 10, splitQ2Answer);

    checkNeedNewPage();

    // Question 3
    var splitQ3 = pdf_doc.splitTextToSize("After reflecting on the outcomes of this performance task, these are the curricular/instructional actions I want to take:", pageWidth-margin);
    pdf_doc.setFontType("bold");
    pdf_doc.text(firstIndent, currentLine += 15, splitQ3);

    checkNeedNewPage();

    var splitQ3Answer = pdf_doc.splitTextToSize(journalData.actions_info, pageWidth-margin);
    pdf_doc.setFontType("normal");
    pdf_doc.text(firstIndent, currentLine += 10, splitQ3Answer);

    checkNeedNewPage();

    // Comments
    var splitComments = pdf_doc.splitTextToSize("Comments:", pageWidth-margin);
    pdf_doc.setFontType("bold");
    pdf_doc.text(firstIndent, currentLine += 15, splitComments);

    checkNeedNewPage();

    var splitCommentsAnswer = pdf_doc.splitTextToSize(journalData.comments, pageWidth-margin);
    pdf_doc.setFontType("normal");
    pdf_doc.text(firstIndent, currentLine += 10, splitCommentsAnswer);

    checkNeedNewPage();

    if(pdf_doc) {
        // Build file name
        var filename = "retrospective_journal_" + journalData.name.toLowerCase() + "_" + groupName.toLowerCase() + ".pdf";

        filename = filename.replace(" ", "_");
        filename = filename.replace(",", "");
        filename = filename.replace("-", "_");
        filename = filename.replace("/", "_");

        pdf_doc.save(filename);
    }
}

// Converts hex color value to RGB
var hexToRgb = function (hexColor){
    // Remove '#'
    hexColor = ((hexColor.charAt(0)=="#") ? hexColor.substring(1,7):hexColor);

    // Parse out RGB values
    var r = parseInt((hexColor).substring(0,2),16),
        g = parseInt((hexColor).substring(2,4),16),
        b = parseInt((hexColor).substring(4,6),16);

    var rgb = [r, g, b];

    return rgb;
}

/*** Template: journalForm ***/

////////// UI Display //////////

Template.journalForm.helpers({
    //Return the date formatted
    getFormattedDate: function (journalDate) {
        return formatTimestamp(journalDate);
    },

    // Display the selected group
    selectedGroupName: function () {
        return Session.get('selected_group') != null ? Session.get('selected_group').name : "";
    }
});

////////// Events //////////

Template.journalForm.events({
    'submit form': function (evt, template) {
        evt.preventDefault();

        saveJournal(evt, template);
     },

    // Clear journal text area fields
    'click #clear-journal': function (evt, template) {
        if (Session.get('selected_journal_entry') != null) {
            var journal = Session.get('selected_journal_entry');

            template.find('textarea[name=textareaJournalQuestion1]').value = "";
            template.find('textarea[name=textareaJournalQuestion2]').value = "";
            template.find('textarea[name=textareaJournalQuestion3]').value = "";
            template.find('textarea[name=textareaJournalComments]').value = "";

            journal.class_info = "";
            journal.strengths_needs_info = "";
            journal.actions_info = "";
            journal.comments = "";

            Session.set('selected_journal_entry', journal);
        }
    },

    // Save and download a PDF of the journal entry
    'click #save-as-pdf': function (evt, template) {
        generateJournalPdf();
    }
});