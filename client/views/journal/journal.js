// Parcc user journal -- client

////////// Variables //////////

// Default when add new standard
var defaultAdd = {
    _id: "",
    performance_task_id: "",
    class_id: "",
    class_info: "",
    strengths_needs_info: "",
    actions_info: "",
    comments: ""
};

// Default currently selected journal entry
Session.setDefault('selected_journal_entry', null);

/*** Template: journal ***/

////////// UI Display //////////

Template.journal.helpers({
    // Determine if a classroom or group is selected
    isClassGroupSelected: function () {
        return Session.get('selected_group') !== null;
    },

    // Determine which tasks to display on the journal,
    // selected based on selected grade.
    tasks: function () {
        var selected_grade = Session.get('selected_grade');

        if (selected_grade == null)
            return [];

        return PerformanceTasks.find({"grade": selected_grade}, {sort: {subject: 1, name: 1}});
    },

    // Get Journal entry for the user + PT + class/group selected
    joinWithJournal: function () {
        var pt = {performance_task_id: this._id, name: this.name},
            selected_group = Session.get('selected_group');

        if (selected_group === null)
            return pt;

        var journal = Meteor.users.findOne({"_id": Meteor.userId(), "profile.journal": { $elemMatch: {"performance_task_id": pt.performance_task_id, "class_id": selected_group._id}} });

        if (!journal) {
            journal = defaultAdd;
            journal.class_id = selected_group._id;
        } else {
            journal = _.find(journal.profile.journal, function (journal) { return journal.performance_task_id === pt.performance_task_id && journal.class_id === selected_group._id });
        }

        return  _.extend(pt, _.omit(journal, 'performance_task_id'));
    },

    // Sets active journal entry
    isActiveJournalEntry: function () {
        return Session.get('selected_journal_entry') !== null
            && Session.get('selected_journal_entry').performance_task_id === this.performance_task_id ? "active" : "";
    },

    // Determine if a journal entry exists for the PT + class
    journalExists: function () {
        return this._id !== null && this._id.length > 0;
    },

    // Determine if should show journal entry form
    showJournalForm: function () {
        return Session.get("selected_journal_entry") != null;
    },

    // Retrieve selected journal info
    selectedJournal: function () {
        return Session.get("selected_journal_entry");
    },

    //Return the date formatted
    getFormattedDate: function (journalDate) {
        if (journalDate) {
            var monthNames = new Array("Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                    "Oct", "Nov", "Dec"),
                d = journalDate,
                hours = d.getHours(),
                minutes = d.getMinutes(),
                ampm = hours >= 12 ? 'PM' : 'AM';

            hours = hours % 12;
            hours = hours ? hours : 12;
            minutes = minutes < 10 ? '0' + minutes : minutes;

            var strTime = hours + ':' + minutes + ' ' + ampm;

            return monthNames[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear() + " " + strTime;
        }

        return "";
    }
});


////////// Events //////////

Template.journal.events({
    // Set the selected journal entry
    'click #select-journal-entry': function (evt, template) {
        evt.stopPropagation();

        Session.set('selected_journal_entry', this);
    }
});


