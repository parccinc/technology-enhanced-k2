// Parcc dashboard -- client

////////// Variables //////////

// Default currently selected subject
Session.setDefault('selected_subject', "ela");

// Default currently selected pt info
Session.setDefault('selected_pt_info', null);

var ccssCounter = 0,
    elaBackgroundColor = "#009900",
    mathBackgroundColor = "#72547D";

// used to shade the task boxes a color percentage indicated
function shadeColor(color, percent) {
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}


/*** Template: dashboard ***/

////////// UI Display //////////

Template.dashboard.rendered = function () {
    if (Meteor.user() && Session.get('display_help') && Session.get('display_help') === true) {
        $('#helpInfoModal').modal("show");
    }
}

Template.dashboard.helpers({
    // Determines if a group is selected.
    isGroupSelected: function () {
        return Session.get('selected_group') != null;
    },

    // Determine which tasks to display on the dashboard,
    // selected based on selected grade and selected subject.
    tasks: function () {
        var selected_subject = Session.get('selected_subject'),
            selected_grade = Session.get('selected_grade');

        if (selected_subject == null || selected_grade == null)
            return [];

        ccssCounter = 0;
        console.log(PerformanceTasks.find({"grade": selected_grade, "subject": selected_subject}, {sort: {displayorder: 1}}).fetch());

        return PerformanceTasks.find({"grade": selected_grade, "subject": selected_subject}, {sort: {displayorder: 1}});
    },

    // Make the selected subject active
    activeSubject: function (subject) {
        return Session.get("selected_subject") != null && Session.get("selected_subject") == subject ? "active" : "";
    },

    // Check if the selected subject is equal to the passed in subject argument
    isSelectedSubject: function (subject) {
        return Session.get("selected_subject") != null && Session.get("selected_subject") == subject ? true : false;
    }
});


////////// Events //////////

Template.dashboard.events({
    // Set ELA as the selected subject
    'click #btn-ela': function () {
        Session.set("selected_subject", "ela");
    },
    // Set Math as the selected subject
    'click #btn-math': function () {
        Session.set("selected_subject", "math");
    },
    'click': function () {
        $(".ela-math-tip").tooltip('hide');
        $(".standard-tip").tooltip('hide');
    }
});


/*** Template: task ***/

////////// UI Display //////////

Template.task.rendered = function () {
    console.log("task");
    $( "#effect" ).effect( "fade");

    $('.ela-math-tip').tooltip({placement: 'right', container: 'body'});
};

Template.task.helpers({
    // Check to see if an image exists for the PT
    isImageFound: function () {
        return this.visual.trim().length > 0;
    },
    // Check if the selected subject is ELA
    isELA: function (subject) {
        return Session.get("selected_subject") != null && Session.get("selected_subject") == 'ela';
    },
    // Get background color based on subject
    getBackgroundColor: function () {
        return this.subject === "ela" ? elaBackgroundColor : mathBackgroundColor;
    },
    // Retrieve a darker shaded version of the performance task background color
    getDarkenedColor: function () {
        var color = this.subject === "ela" ? elaBackgroundColor : mathBackgroundColor;

        return shadeColor(color, -0.4);
    },
    // Retrieve a lighter shaded version of the performance task background color
    getLightenedColor: function (color) {
        return shadeColor(color, 0.6);
    },
    urlFormattedName: function () {
        return this.name.replace(/[^ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789]+/gi, '-')
    }
});

Template.task.events({
    // Display pt At-a-glance info
    'click #at-a-glance-link': function () {
        Session.set("selected_pt_info", this.instructions.plan);

        $('#ptInfoModal').modal("show");
    },
    'click .ela-math-tip': function (e) {
        e.stopPropagation();
        $(e.target).tooltip('show');

    }
});

/*** Template: ccssList ***/

////////// UI Display //////////

Template.ccssList.helpers({
    // Breaks the ccss list into a grid of 4 or 5 columns per row
    // depending upon subject, math or ela
    ccss: function () {
        var selectedSubject = Session.get('selected_subject'),
            all = this,
            chunks = [],
            size = selectedSubject == 'ela' ? 5 : 4;
        while (all.length > size) {
            chunks.push({ row: all.slice(0, size)});
            all = all.slice(size);
        }
        chunks.push({row: all});

        return chunks;
    }
});


/*** Template: ccssItem ***/

////////// UI Display //////////

//initialize all tooltips in this template
Template.ccssItem.rendered = function() {
    $('.standard-tip').tooltip({placement: 'right'});

};

Template.ccssItem.helpers({
    // Gets the description details of the ccss
    joinWithCommonCoreStandards: function () {
        var commonCoreStandard = this;
        var standards = CommonCoreStateStandards.findOne({_id: commonCoreStandard._id});
        return _.extend(commonCoreStandard, _.omit(standards, '_id'));
    },

    // Allow more column width for math because it can have much longer CCSS id's
    subjectColSize: function () {
        return Session.get('selected_subject') == 'ela' ? 'col-sm-2 col-md-2 col-lg-2' : 'col-sm-3 col-md-3 col-lg-3';
    }
});

Template.ccssItem.events({
   'click .standard-tip': function (e) {
       e.stopPropagation();
       $(e.target).tooltip('show');
   }
});
/*** Template: ptInfoModal ***/

////////// UI Display //////////

Template.ptInfoModal.helpers({
    // Get performance task info to display in the modal
    getPtInfo: function () {
        return Session.get("selected_pt_info");
    }
});
