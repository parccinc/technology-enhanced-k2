
function initializeFilters(){
    Session.set('selected_subject', null);
    Session.set('selected_performance_task', null);
    Session.set('selected_standard', null);
    Session.set('selected_assessment', null);
    Session.set('selected_image_artifact', null);
    // Disables the search box when the page is loaded.
    if (getGroupSelected()) {
        this.$('#class-detail-search-input').removeAttr('disabled');
    } else {
        this.$('#class-detail-search-input').attr('disabled','disabled');
    }

}
////////////////////// VARIABLES ////////////////////////

// Default currently selected students
Session.setDefault('selected_student', null);

/** Template: studentDetailsColumn **/


// Helpers

Template.studentDetailsColumn.helpers({

});

// Events
Template.studentDetailsColumn.events({
    'click #alpha-sort': function () {
        var groupOrderList = Session.get('group_list_ordered'),
            currentGroupOrder = groupOrderList[Session.get("selected_group")._id],
            alphaList = sortByKey(currentGroupOrder, "lastname");

        groupOrderList[Session.get("selected_group")._id] = alphaList;

        Session.set('group_list_ordered', groupOrderList);
    },
    'click #my-custom-sort': function () {
        var  groupOrderList = Session.get('group_list_ordered'),
            studentList =  Students.find({ _id: { $in: Session.get("selected_group").students } }).fetch(),
            sortingArr = Session.get("selected_group").students;

        groupOrderList[Session.get("selected_group")._id] = sortArrayByArray(sortingArr, studentList, "_id");

        Session.set('group_list_ordered', groupOrderList);
    }
});

/** Template: studentDetailsList **/

////////// UI Display //////////

Template.studentDetailsList.rendered = function() {
    $('#student-list').sortable({
        stop: function(event, ui) {
            var newGroupOrderList = [],
                groupOrderList = Session.get('group_list_ordered'),
                currentGroupOrder = groupOrderList[Session.get("selected_group")._id];

            $("#student-list li").each(function(i, el){
                var id = $(el).data('id');
                newGroupOrderList.push(currentGroupOrder[currentGroupOrder.map(function(e) { return e._id; }).indexOf(id)]);
            });


            groupOrderList[Session.get("selected_group")._id] = newGroupOrderList;

            Session.set('group_list_ordered', groupOrderList);
        }
    });
};

Template.studentDetailsList.helpers({
    // Get list of students for the selected group
    studentDetailsList: function () {
        if (Session.get("selected_group") != null) {
            var groupOrderList = Session.get('group_list_ordered');

            if (groupOrderList != null) {
                if (groupOrderList[Session.get("selected_group")._id]) {
                    return groupOrderList[Session.get("selected_group")._id];
                }
            } else {
                groupOrderList = {};
            }

            if (Session.get("selected_group").students) {
                var studentList = Students.find({ _id: { $in: Session.get("selected_group").students } }).fetch();

                if (studentList.length > 0) {
                    var sortingArr = Session.get("selected_group").students;

                    groupOrderList[Session.get("selected_group")._id] = sortArrayByArray(sortingArr, studentList, "_id");

                    Session.set('group_list_ordered', groupOrderList);

                    return studentList;
                }
            } else {
                var studentList = Students.find({classroom_id: Session.get("selected_group")._id}, {sort: {lastname: 1}}).fetch();

                if (studentList.length > 0) {
                    groupOrderList[Session.get("selected_group")._id] = studentList;
                    Session.set('group_list_ordered', groupOrderList);

                    return studentList;
                }
            }
        }

        return [];
    },

    // Sets active student
    isActiveStudent: function (id) {
        if(Session.get('selected_student') !== null && Session.get('selected_student')._id === this._id){
            return "active";
        }else{
            return "";
        }
    },

    isStudentAssessed: function () {
        if (Session.get("selected_performancetask") != null
            && Session.get('selected_day') != null
            && Session.get("selected_assessment") != null) {

            var currentStudent = this,
                performanceTaskId = Session.get("selected_performancetask")._id,
                day = Session.get('selected_day'),
                assessmentId = Session.get("selected_assessment")._id;

            if (currentStudent.assessmentdata
                && currentStudent.assessmentdata != null
                && currentStudent.assessmentdata[performanceTaskId]
                && currentStudent.assessmentdata[performanceTaskId][day]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId]
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId] != null
                && currentStudent.assessmentdata[performanceTaskId][day][assessmentId].length > 0) {
                return true;
            }
        }

        return false;
    },


    getOddEvenColumn: function (id) {
        var groupOrderList = Session.get('group_list_ordered');

        if (groupOrderList) {
            var currentList = groupOrderList[Session.get("selected_group")._id] || [],
                currentIdx = currentList.map(function(e) { return e._id; }).indexOf(id);

            if (currentIdx != -1 && currentIdx % 2) {
                return "odd";
            }

            return "even";
        }

        return "";
    },
    studentSelected: function() {
        return Session.get('selected_student') != null;
    }
});

////////// Events //////////

// Function to sort an array but a specified object key
function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

// If more than one student is selected and at if all student's selected data
// for the given performance task + day + assessment does not match then
// we need to alert them that the "active" selections will not be shown
// for that reason
alertDataCollision = function () {
    if (Session.get('selected_student') != null
        && Session.get('selected_student').length > 1
        && Session.get('selected_day') != null
        && Session.get("selected_assessment") != null) {

        var students = Session.get('selected_student'),
            performanceTaskId = Session.get("selected_performancetask")._id,
            day = Session.get('selected_day'),
            assessmentId = Session.get("selected_assessment")._id,
            distinctValues = students.map(function(e) {
                    if (e.assessmentdata
                        && e.assessmentdata[performanceTaskId]
                        && e.assessmentdata[performanceTaskId][day]
                        && e.assessmentdata[performanceTaskId][day][assessmentId]) {
                        return e.assessmentdata[performanceTaskId][day][assessmentId].join();
                    }

                    return "";
                }
            ),
            lastValue = distinctValues[0];

        for (var i = 1; i < distinctValues.length; i++) {
            if (distinctValues[i] != lastValue) {
                return true;
            }
        }

        return false;
    }
};

Template.studentDetailsList.events({
    'click #student-item': function (evt, template) {
        // Set student being selected when a student is clicked on
        Session.set('selected_standard', null);
        Session.set('selected_performance_task', null);
        Session.set('selected_subject', null);
        Session.set('selected_student', this);
        Session.set('view_value', null);

        if (alertDataCollision()) {
            if ($('.bootstrap-growl')) {
                $('.bootstrap-growl').removeClass('bootstrap-growl');
            }

            $.bootstrapGrowl("<b>Student information cannot be displayed.</b><br/>You have selected more than one student whose information is not the same.", {
                type: 'warning',
                align: 'center',
                width: 'auto',
                allow_dismiss: true,
                delay: 5000
            });
        }
    }
});


Template.studentDetailsFilters.helpers({
    // Returns a list of subjects for the dropdown. Placed in a helper function so that it can be modified later if need be.
    subject: function () {
        return ["ELA", "Math"];

    },
    // Returns a filtered list of performance tasks based on the user's filter selections
    filterPerformanceTask: function () {
        return getFilterPerformanceTasks();
    },
    // Returns a filtered list of standards based on the user's filter selections
    filterStandards: function () {
        return getFilterStandards();
    },
    // Returns a filtered list of just ELA performance tasks
    filterElaPerformanceTasks: function () {
        return getElaFilteredPerformanceTasks();
    },
    // Returns a filtered list of just Math performance tasks
    filterMathPerformanceTasks: function () {
        return getMathFilteredPerformanceTasks();
    },
    // Returns the name of the selected group/classroom
    selectedGroup: function () {
        if (Session.get('selected_group')) {
            return Session.get('selected_group').name || "";
        } else {
            return null;
        }
    },
    // Returns whether or not a group/classroom has been selected
    groupSelected: function () {
        if (getGroupSelected()) {
            $('#class-detail-search-input').removeAttr('disabled');
        } else {
            $('#class-detail-search-input').attr('disabled','disabled');
        }

        return getGroupSelected();
    },
    // Returns whether or not a subject has been selected
    subjectSelected: function () {
        return getSubjectSelected();
    },
    // Returns whether or not a performance task has been selected
    performanceTaskSelected: function () {
        return getPerformanceTaskSelected();
    },
    // Returns whether or not a standard has been selected
    standardSelected: function () {
        return getStandardSelected()
    },
    // Returns the selected subject, if applicable
    selectedSubject: function () {
        return getSelectedSubject();
    },
    //Returns selected Subject formatted
    formatSubject: function (subject) {
        if (subject == "All") {
            return "All Subjects";
        }

        return subject;
    },
    // Returns the selected performance task, if applicable
    selectedPerformanceTask: function () {
        return getSelectedPerformanceTask();
    },
    //Returns selected Subject formatted
    formatPT: function (pt) {
        if (pt == "All") {
            return "All PTs";
        }

        return pt;
    },
    // Returns the selected standard, if applicable
    selectedStandard: function () {
        return getSelectedStandard();
    },
    //Returns selected Standard formatted
    formatStandard: function (standard) {
        if (standard == "All") {
            return "All Standards";
        }

        return standard;
    },
    // Returns whether or not the user has selected a group, subject, performance task, and standard.
    filterSelectionMade: function () {
        return getSubjectSelected() || getPerformanceTaskSelected() || getStandardSelected();
    },
    // Returns a list of all students in the current classroom/group
    groupStudent: function() {
        return listGroupStudents();
    },
    // Returns a list of assessments related to a performance task
    performanceTaskAssessment: function() {
        return listPerformanceTaskAssessments(Session.get('selected_performance_task'));
    },
    // Returns a list of possible assessment values associated with an assessment
    assessmentValue: function() {
        return listAssessmentValues(Session.get('selected_assessment'));
    },
    displayAllPerformanceTasks: function () {
        var selectedSubject = Session.get('selected_subject');

        if (selectedSubject != null) {
            return selectedSubject.trim().toUpperCase() == "ALL" || selectedSubject.trim().toUpperCase() == "BOTH";
        } else {
            return true;
        }
    },
    studentSelected: function() {
        /* Check if we're on the student details page because the class details
         * page uses the same code and the filters will be disabled until a
         * student is selected, which is not possible on the class details.
         */
        if (Router.current().route.path(this) == "/classDetails") {
            return true;
        } else {
            return Session.get('selected_student') != null;
        }
    },
    isStudentDetails: function() {
        return Router.current().route.path(this) == "/studentDetails";
    },
    isClassDetails: function() {
        return Router.current().route.path(this) == "/classDetails";
    }
});

Template.studentDetailsFilters.events({
    // User navigates between Class/Student view
    'click .class-student-unselected': function () {
        var currentPage = Router.current().route.path(this);

        if (currentPage == "/studentDetails") {
            Session.set('loading_report', true);

            setTimeout(function () {
                Router.go("classDetails");
            }, 100);
        } else if (currentPage == "/classDetails") {
            Session.set('loading_report', true);

            setTimeout(function () {
                Router.go("studentDetails");
            }, 100);
        }
    },
    // User selects a subject from the filter
    'click #class-details-subject-filter li a': function (event) {
        var subjectSelected = event.target.innerHTML.trim();

        if (event.target.innerHTML.trim().toUpperCase() == "ALL SUBJECTS") {
            subjectSelected = "All";
        }

        if (Session.get('selected_subject') != subjectSelected) {

            if (Session.get('selected_performance_task') == null) {
                // Set performance task to "all" if the user is making their first filter selection
                Session.set('selected_performance_task', 'All');
            } else {
                // Check if Performance Task is the same subject as the subject that the user selected
                if (Session.get('selected_performance_task').trim().toUpperCase() != "ALL" && subjectSelected.toUpperCase() != "ALL") {
                    var subject = PerformanceTasks.findOne({_id: Session.get('selected_performance_task')}).subject;

                    if (subject.trim().toUpperCase() != subjectSelected.toUpperCase()) {
                        Session.set('selected_performance_task', 'All');
                    }
                }
            }

            if (Session.get('selected_standard') == null) {
                // Set standard to "all" if the user is making their first filter selection
                Session.set('selected_standard', 'All');
            } else {
                // Check if Standard is the same subject as the subject that the user selected
                if (Session.get('selected_standard').trim().toUpperCase() != "ALL" && subjectSelected.toUpperCase() != "ALL") {
                    var subject = CommonCoreStateStandards.findOne({_id: Session.get('selected_standard')}).subject;

                    if (subject.trim().toUpperCase() != subjectSelected.toUpperCase()) {
                        Session.set('selected_standard', 'All');
                    }
                }
            }

            Session.set('loading_report', true);
        }

        setTimeout(function () {
            Session.set('selected_subject', subjectSelected);
            Session.set('details_page', 1);
        }, 100);
    },
    // User selects a performance task from the filter
    'click #class-details-performance-task-filter li a': function (event) {
        var selectedPT = event.target.innerHTML.trim();

        if (event.target.innerHTML.trim().toUpperCase() == "ALL PTS") {
            selectedPT = "All";
        }

        if (Session.get('selected_performance_task') != event.target.id) {

            if (Session.get('selected_standard') == null) {
                // Set standard to "all" if the user is making their first filter selection
                Session.set('selected_standard', 'All');
            } else {
                // Check if Standard is the same subject as the subject that the user selected
                if (Session.get('selected_standard').trim().toUpperCase() != "ALL" && selectedPT.toUpperCase() != "ALL") {
                    var ccss = CommonCoreStateStandards.findOne({_id: Session.get('selected_standard')}),
                        subject = ccss.subject;

                    if (subject && subject.trim().toUpperCase() != PerformanceTasks.findOne({_id: event.target.id }).subject.trim().toUpperCase()) {
                        Session.set('selected_standard', 'All');
                    }
                }
            }

            if (Session.get('selected_subject') == null) {
                Session.set('selected_subject', 'All');
            }
            Session.set('loading_report', true);
        }

        setTimeout(function () {
            Session.set('selected_performance_task', event.target.id);
            Session.set('details_page', 1);
        }, 100);
    },
    // User selects a standard from the filter
    'click #class-details-standard-filter li a': function (event) {
        var selectedStandard = event.target.innerHTML.trim();

        if (event.target.innerHTML.trim().toUpperCase() == "ALL STANDARDS") {
            selectedStandard = "All";
        }

        if (Session.get('selected_standard') != selectedStandard) {
            if (Session.get('selected_subject') == null) {
                Session.set('selected_subject', 'All');
            }
            if (Session.get('selected_performance_task') == null) {
                Session.set('selected_performance_task', 'All');
            }
            Session.set('loading_report', true);
        }

        setTimeout(function () {
            Session.set('selected_standard', selectedStandard);
            Session.set('details_page', 1);
        }, 100);
    },
    /*'keydown #class-detail-search-input': function (e) {
        if (e.which === 13){

            if (Session.get('selected_standard') != e.target.value.trim()) {
                if (Session.get('selected_subject') == null) {
                    Session.set('selected_subject', 'All');
                }
                if (Session.get('selected_performance_task') == null) {
                    Session.set('selected_performance_task', 'All');
                }
                Session.set('loading_report', true);
            }

            setTimeout(function () {
                Session.set('selected_standard', e.target.value.trim().toUpperCase());
            }, 100);

            return false;
        }
    },*/
    'click a[href="#"]': function(e){
        e.preventDefault();
    },
    'submit #details-standards-search-filter': function (event) {
        event.preventDefault();

        var userInput = $('#class-detail-search-input').val().trim();

        if (userInput != null && userInput != "" && Session.get('selected_standard') != userInput.toUpperCase()) {
            if (Session.get('selected_subject') == null) {
                Session.set('selected_subject', 'All');
            }
            if (Session.get('selected_performance_task') == null) {
                Session.set('selected_performance_task', 'All');
            }
            Session.set('loading_report', true);

            setTimeout(function () {
                Session.set('selected_standard', userInput.toUpperCase());
            }, 100);
        }
    }
});

Template.studentDetailsFilters.rendered = function(){
    initializeFilters();
};
