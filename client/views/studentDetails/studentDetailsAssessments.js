/** Global Variables **/

var pdf_doc;


/** studentDetailsAssessments Template **/

// Helpers //

Template.studentDetailsAssessments.helpers({
    // Returns array of assessment data for a student
    assessmentData: function () {
        return getAssessmentData(Session.get('selected_student'), function () {
            $('#scroll-container').scrollTop(0);
        });
    },
    // Returns selected student
    student: function () {
        return Session.get('selected_student');
    },
    // Functions to return a student's assessment score
    isMeeting: function (scoreText) {
        return scoreText === "Meeting";
    },
    isApproaching: function (scoreText) {
        return scoreText === "Approaching";
    },
    isDeveloping: function (scoreText) {
        return scoreText === "Developing";
    },
    isEmerging: function (scoreText) {
        return scoreText === "Emerging";
    },
    // Returns whether or not an assessment is flagged
    isFlagged: function (scoreText){
        return scoreText === "FLAG";
    },
    // Returns whether or not an assessment is a checklist
    isChecklist: function (type){
        return type === "checklist";
    },
    isRubric: function(type){
        return type === "rubric"
    },
    isYesNoList: function(type){
        return type === "yesnolist"
    },
    // Checks if subject is math
    isMath: function (text){
        return text === "math";
    },
    // Checks if standards array contains any data
    isArray: function (input) {
       return Object.prototype.toString.call( input ) === '[object Array]'
    },
    // Converts an array into a comma separated string of list items
    arrayToString: function(list) {
        return convertArrayToString(list);
    },
    standardDescription: function (standard) {
        return ccssDescription(standard);
    },
    yesno: function(object){
        var background, icon, color;
        if (object.isobserved === 'yes') {
            background = '#009900';
            icon = 'YES';
            color = 'white';
        } else {
            background = '#3179D0';
            icon = 'NO ';
            color = 'white';
        }
        return {background:background, icon:icon, color:color, observation:object.observation};
    },
    yesNoList: function(object) {
        return getYesNoListContext(object);
    },
    getPerformanceTaskColor: function(subject) {
        // Assign green to ELA and purple to Math.
        // If a performance task does not have a color for some reason, it will be made grey.
        if (subject.toUpperCase() == "ELA") {
            return "#009900";
        } else if (subject.toUpperCase() == "MATH") {
            return "#8080c0";
        } else {
            return "#999999";
        }

        return "#999999";
    },
    allUserSelectionsMade: function() {
        return Session.get('selected_subject') != null && Session.get('selected_performance_task') != null && Session.get('selected_standard') != null;
    },
    groupSelected: function() {
        return Session.get('selected_group') != null;
    },
    studentSelected: function() {
        return Session.get('selected_student') != null;
    },
    // Format note date/time
    getFormattedDate: function (noteDate) {
        return formattedDate(noteDate);
    },
    canGeneratePdf: function() {
        return Session.get('selected_group') != null &&         Session.get('selected_student') != null &&
               Session.get('selected_subject') != null &&       Session.get('selected_performance_task') != null &&
               Session.get('selected_performance_task') != null;
    },
    loadingReport: function () {
        return Session.get('loading_report');
    }
});

Template.registerHelper('and', function (param1, param2) {
    return param1 && param2;
})


// Events

Template.studentDetailsAssessments.events = {
    // Add note/media to selected students
    'click #student-details-artifacts-icon': function () {
        Session.set("selected_performancetask", PerformanceTasks.findOne({_id: this._id}));
        //$('#artifactViewModal').modal("show");
    },
    // Render page to PDF
    'click #student-details-download-pdf-button': function () {
        generateStudentPdf();
    },
    // Functions to filter assessments
    'click #pt-dropdown-all': function () {
        Session.set("subject_filter", "all");
        $("#subject-dropdown-button-text").html("All");
    },
    'click #pt-dropdown-ela': function () {
        Session.set("subject_filter", "ela");
        $("#subject-dropdown-button-text").html("ELA");
    },
    'click #pt-dropdown-math': function () {
        Session.set("subject_filter", "math");
        $("#subject-dropdown-button-text").html("Math");
    }
};



// Rendered

Template.studentDetailsAssessments.rendered = function () {
    //Session.set("selected_subject", "both");
    //Session.set("subject_filter", "all");
    Session.set('selected_image_artifact', null);
    Session.set('loading_report', false);

    $(window).resize();     // Correctly sizes the scrollable pane on page load
};



/** artifactListModal Template **/

// Helpers

Template.artifactListModal.helpers({
    noteData: function (){
        return getNoteData();
    },

    imageData: function (){
        return getImageData();
    },

    fileInfo: function (mediaDataId) {
        if (mediaDataId) {
            return MediaData.findOne({_id: mediaDataId });
        } else {
            return null;
        }
    },

    performanceTaskNoteData: function (){
        if (Session.get('selected_performancetask')) {
            return getNoteDataForPerformanceTask(Session.get('selected_performancetask')._id);
        } else {
            return null;
        }
    },

    performanceTaskImageData: function (){
        if (Session.get('selected_performancetask')) {
            return getImageDataForPerformanceTask(Session.get('selected_performancetask')._id);
        } else {
            return null;
        }
    },

    // Format note date/time
    getFormattedDate: function (noteDate) {
        return formattedDate(noteDate);
    },

    // Returns boolean value indicating whether or not the selected student has notes for the selected performance task
    hasNotes: function(noteData) {
        return getHasNotes(noteData);
    },

    // Returns boolean value indicating whether or not the selected student has notes for the selected performance task
    hasImages: function(noteData) {
        return getHasImages(noteData);
    },

    // Converts an array into a comma separated string of list items
    arrayToString: function(list){
        return convertArrayToString(list);
    },

    // Returns whether or not the user is currently viewing an image artifact or the entire modal.
    imageArtifactSelected: function() {
        return Session.get('selected_image_artifact') != null;
    }
});



/** artifactViewModal Template **/

// Helpers

Template.artifactViewModal.helpers({
    // Returns the current performance task
    currentPerformanceTask: function (){
        if (Session.get('selected_performancetask') != null) {
            return Session.get('selected_performancetask').name;
        }

        return "";
    },
    studentFirstName: function () {
        var currStudent = Session.get('selected_student');

        if (currStudent) {
            return currStudent.firstname || "";
        }
    },
    studentLastName: function () {
        var currStudent = Session.get('selected_student');

        if (currStudent) {
            return currStudent.lastname || "";
        }
    },

    // Returns whether or not the user is currently viewing an image artifact or the entire modal.
    imageArtifactSelected: function() {
        return Session.get('selected_image_artifact') != null;
    },

    // Returns the currently selected image artifact, if there is one
    selectedImageArtifact: function() {
        return Session.get('selected_image_artifact');
    },

    fileInfo: function (mediaDataId) {
        if (mediaDataId) {
            return MediaData.findOne({_id: mediaDataId });
        } else {
            return null;
        }
    }
});

// Events

Template.artifactViewModal.events({
    'click #artifact-view-modal-back': function() {
        Session.set('selected_image_artifact', null);
    }
});

// Rendered

Template.artifactViewModal.rendered = function () {
    $('#artifactViewModal').on('hidden.bs.modal', function () {
        Session.set('selected_image_artifact', null);
    });
}



/** JS functions **/

var convertArrayToString = function (list) {
    if(list != null) {
        var temp = "";
        for(var key in list){
            if(key != list.length - 1) {
                temp += list[key]._id + ", ";
            }else{
                temp += list[key]._id;
            }
        }
        return temp;
    }
    return null;
};

var formattedDate = function (date) {
    if (date) {
        var monthNames = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec"),
            d = date,
            hours = d.getHours(),
            minutes = d.getMinutes(),
            ampm = hours >= 12 ? 'PM' : 'AM';

        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;

        var strTime = hours + ':' + minutes + ' ' + ampm;

        return monthNames[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear() + " " + strTime;
    }

    return "";
};

var getAssessmentData = function (student, callback) {
    var performanceTaskID,
        performanceTaskArray = [],
        currentPerformanceTask,
        subjectFilter = Session.get('selected_subject'), //subject filter
        selectedPerformanceTask = Session.get('selected_performance_task'), // performance task filter
        selectedStandard = Session.get('selected_standard'); // standard filter

    if (student != null) {
        var assessmentData = student.assessmentdata;                                                          // List of completed performance tasks
        for (performanceTaskID in assessmentData) {                                                        // Loop through completed performance tasks in the students collection
            if (selectedPerformanceTask === performanceTaskID || selectedPerformanceTask === null || selectedPerformanceTask === "All"){
                if (assessmentData.hasOwnProperty(performanceTaskID)) {                                       // Make sure the property does not come from a prototype
                    // PAR-77 - Adding ELA/Math filter
                    var query = {_id: performanceTaskID};
                    if (subjectFilter && subjectFilter !== null && subjectFilter !== "All"){
                        query.subject = subjectFilter.toLowerCase();
                    }
                    currentPerformanceTask = PerformanceTasks.findOne(query);

                    assessmentArray = [];

                    if (currentPerformanceTask) {
                        for (day in assessmentData[performanceTaskID]) {                                          // Loop through the completed assessments in the students collection
                            if (assessmentData[performanceTaskID].hasOwnProperty(day)) {                          // Make sure the property does not come from a prototype
                                standardArray = [];
                                for (assessmentID in assessmentData[performanceTaskID][day]) {
                                    if (assessmentData[performanceTaskID][day].hasOwnProperty(assessmentID)) {    // Make sure the property does not come from a prototype

                                        // Search for the corresponding assessment_id in the performance task
                                        // Iterate through each day/part in the assessmentSchedule
                                        for (var assessmentScheduleIndex in currentPerformanceTask.assessmentSchedule) {
                                            if (currentPerformanceTask.assessmentSchedule.hasOwnProperty(assessmentScheduleIndex)) {
                                                // Iterate through each assessment in the assessmentSchedule day/part
                                                for (var assessmentScheduleAssessmentIndex in currentPerformanceTask.assessmentSchedule[assessmentScheduleIndex].assessments) {
                                                    if (currentPerformanceTask.assessmentSchedule[assessmentScheduleIndex].assessments.hasOwnProperty(assessmentScheduleAssessmentIndex)) {
                                                        if (currentPerformanceTask.assessmentSchedule[assessmentScheduleIndex].assessments[assessmentScheduleAssessmentIndex]._id === assessmentID && currentPerformanceTask.assessmentSchedule[assessmentScheduleIndex].assessments[assessmentScheduleAssessmentIndex].assessment_id != null && currentPerformanceTask.assessmentSchedule[assessmentScheduleIndex].assessments[assessmentScheduleAssessmentIndex].assessment_id != "") {
                                                            var currentAssessment = Assessments.findOne({_id: currentPerformanceTask.assessmentSchedule[assessmentScheduleIndex].assessments[assessmentScheduleAssessmentIndex].assessment_id});
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (currentAssessment) {

                                            var assessmentHasValues = false;

                                            // Handle checklist assessments
                                            if (currentAssessment.type == "checklist") {

                                                // Loop through the assessValues in the assessment document
                                                for (var checklistItem in currentAssessment.assessValues) {
                                                    if (currentAssessment.assessValues.hasOwnProperty(checklistItem)) {

                                                        for (var score in assessmentData[performanceTaskID][day][assessmentID].value) {
                                                            if (assessmentData[performanceTaskID][day][assessmentID].value.hasOwnProperty(score)) {

                                                                if (assessmentData[performanceTaskID][day][assessmentID].value[score] == currentAssessment.assessValues[checklistItem].value) {
                                                                    currentAssessment.assessValues[checklistItem].checked = true;
                                                                    assessmentHasValues = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                data = currentAssessment.assessValues;
                                                data.timestamp = assessmentData[performanceTaskID][day][assessmentID].timestamp;
                                            } else {
                                                data = assessmentData[performanceTaskID][day][assessmentID];
                                            }

                                            // Ensure that an assessment is not displayed if a user enters a value and then removes it.
                                            if (data.value && data.value.length > 0) {
                                                assessmentHasValues = true;
                                            }

                                            standardObject = CommonCoreStateStandards.findOne({_id: currentAssessment.standard});
                                            standardArray.push({
                                                assessmentObject: currentAssessment,
                                                standardObject: standardObject,
                                                hasAssessments: assessmentHasValues,
                                                data: data
                                            });
                                        }
                                    }
                                }

                                // LS PAR-129: check if there are any assessments for the current day
                                var hasAssessments = false;
                                for (var standard in standardArray) {

                                    if (standardArray[standard].assessmentObject.type == "checklist") {
                                        // Checklist
                                        for (var dataIndex in standardArray[standard].data) {
                                            if (standardArray[standard].data[dataIndex].checked && standardArray[standard].data[dataIndex].checked == true) {
                                                hasAssessments = true;
                                            }
                                        }
                                    } else if (standardArray[standard].assessmentObject.type == "yesnolist") {
                                        // Yes/No List
                                        for (var assessValuesIndex in standardArray[standard].assessmentObject.assessValues) {
                                            if (standardArray[standard].assessmentObject.assessValues.hasOwnProperty(assessValuesIndex)) {
                                                for (var valueIndex in standardArray[standard].assessmentObject.assessValues[assessValuesIndex].value) {
                                                    if (standardArray[standard].assessmentObject.assessValues[assessValuesIndex].value.hasOwnProperty(valueIndex)) {
                                                        hasAssessments = true;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        // Rubric
                                        if (standardArray[standard].data && standardArray[standard].data.length > 0) {
                                            hasAssessments = true;
                                        }
                                        if (standardArray[standard].data && standardArray[standard].data.value.length > 0) {
                                            hasAssessments = true;
                                        }
                                    }
                                }
                                // END OF PAR-129

                                // Push to the array
                                assessmentArray.push({
                                    'day': day,
                                    'standardsArray': standardArray,
                                    'hasAssessments': hasAssessments
                                });
                            }
                        }

                        // filter by ccss
                        if (selectedStandard && selectedStandard != null && selectedStandard.trim().toUpperCase() != "ALL") {
                            var tempArr = [];
                            // loop through the assessments standards and see if they match the selected filter
                            assessmentArray.forEach(function(assessment, index){
                                assessment.standardsArray.forEach(function (standard) {
                                    standard.assessmentObject.commonCoreStateStandards.forEach(function(strd){
                                        if (strd._id.trim().toUpperCase().indexOf(selectedStandard.trim().toUpperCase()) > -1){
                                            // LS PAR-178: Ensure that the student's scores are not being pushed to the array twice.
                                            if (tempArr.indexOf(assessmentArray[index]) == -1) {
                                                // append the student scores to the object
                                                tempArr.push(assessmentArray[index]);
                                            }
                                            // END OF PAR-178
                                        }
                                    })
                                });
                            });

                            // Remove an unneccesary assessments from the array
                            for (var i in tempArr) {
                                if (tempArr.hasOwnProperty(i)) {

                                    for (var standardsArrayIndex = tempArr[i].standardsArray.length; standardsArrayIndex >= 0; standardsArrayIndex--) {
                                        if (tempArr[i].standardsArray.hasOwnProperty(standardsArrayIndex)) {

                                            var foundSelectedStandard = false;
                                            for (var stdIndex in tempArr[i].standardsArray[standardsArrayIndex].assessmentObject.commonCoreStateStandards) {
                                                if (tempArr[i].standardsArray[standardsArrayIndex].assessmentObject.commonCoreStateStandards.hasOwnProperty(stdIndex)) {

                                                    if (tempArr[i].standardsArray[standardsArrayIndex].assessmentObject.commonCoreStateStandards[stdIndex]._id.trim().toUpperCase().indexOf(selectedStandard.trim().toUpperCase()) > -1) {
                                                        foundSelectedStandard = true;
                                                    }
                                                }
                                            }

                                            if (!foundSelectedStandard) {
                                                tempArr[i].standardsArray.splice(standardsArrayIndex, 1);
                                            }

                                            /*if (tempArr[i].standardsArray[standardsArrayIndex].assessmentObject.commonCoreStateStandards.indexOf(selectedStandard) == -1) {
                                                tempArr[i].standardsArray.splice(standardsArrayIndex, 1);
                                            }*/
                                        }
                                    }
                                }
                            }

                            currentPerformanceTask.studentDetails = tempArr;
                        } else {
                            // Push current task to performanceTaskArray
                            currentPerformanceTask.studentDetails = assessmentArray;
                        }

                        // LS PAR-129: Check if the current performance task has any assessments
                        currentPerformanceTask.ptHasAssessments = false;
                        for (var assessment in currentPerformanceTask.studentDetails) {
                            if (currentPerformanceTask.studentDetails[assessment].hasAssessments) {
                                currentPerformanceTask.ptHasAssessments = true;
                            }
                        }
                        // END OF PAR-129
                            performanceTaskArray.push(currentPerformanceTask);
                        currentPerformanceTask = null;
                    }
                }
            }
        }
    }

    var reportHasAssessments = false;
    for (var ptIndex in performanceTaskArray) {
        if (performanceTaskArray.hasOwnProperty(ptIndex)) {
            if (performanceTaskArray[ptIndex].ptHasAssessments) {
                reportHasAssessments = true;
            }
        }
    }

    if (!reportHasAssessments) {
        $.bootstrapGrowl("There are is no information collected for your selection.", {
            type: 'warning',
            align: 'center',
            width: 'auto',
            allow_dismiss: true,
            delay: 4000
        });
    }

    console.log(performanceTaskArray);
    Session.set('loading_report', false);
    callback();
    return performanceTaskArray;
};

var ccssDescription = function (standard) {
    var ccss = CommonCoreStateStandards.findOne({ _id: standard });

    if (ccss && ccss.description) {
        return ccss.description.trim();
    } else {
        return "N/A";
    }
};

var getNoteData = function () {
    var student = Session.get("selected_student"),
        performanceTask = Session.get("selected_performancetask");

    if(student == null){
        return null;
    }

    var noteData = Media.find({ students: {$in: [student._id]}, type: "note"}).fetch();
    return noteData;
};

var getImageData = function () {
    var student = Session.get("selected_student"),
        performanceTask = Session.get("selected_performancetask");

    if(student == null){
        return null;
    }

    var noteData = Media.find({ students: {$in: [student._id]}, type: "image"}).fetch();

    return noteData;
};

var getNoteDataForPerformanceTask = function (performanceTaskId) {
    var student = Session.get("selected_student");

    if(student === null){
        return null;
    }

    var noteData = Media.find({ students: { $in: [student._id] }, performance_task_id: performanceTaskId, type: "note" }).fetch();
    return noteData;
};

var getImageDataForPerformanceTask = function (performanceTaskId) {
    var student = Session.get("selected_student");

    if(student === null){
        return null;
    }

    var noteData = Media.find({ students: { $in: [student._id] }, performance_task_id: performanceTaskId, type: "image" }).fetch();
    return noteData;
};

var getHasNotes = function (noteData) {
    if (noteData) {
        return noteData.length > 0 || false;
    }
};

var getHasImages = function (noteData) {
    if (noteData) {
        return noteData.length > 0 || false;
    }
};

var generateStudentPdf = function() {

    // Create a new document
    pdf_doc = new jsPDF('portrait', 'mm', 'letter');

    /** Declare/initialize variables **/
    var margin = 15,
        pageHeight = pdf_doc.internal.pageSize.height - (2 * margin),
        pageWidth = pdf_doc.internal.pageSize.width - (2 * margin),
        firstLine = margin + 5,
        firstIndent = margin,
        secondIndent = firstIndent + 10,
        thirdIndent = secondIndent + 10,
        assessmentLineMaxChar = 90,
        rubricRectWidth = 35,
        rubricRectHeight = 6,
        rectCornerRadius = 1,
        largeFontSize = 16,
        smallFontSize = 10,
        currentPage = 0,
        greyedOutTextColor = "#7B7B7B",
        onePagePerPerformanceTask = true,
        renderPdfInColor = false,
        firstName = Session.get('selected_student').firstname || "",
        lastName = Session.get('selected_student').lastname || "",
        justAddedImage = false;

    function drawPageFooter() {
        pdf_doc.setFontSize(smallFontSize);
        pdf_doc.setTextColor(0);
        pdf_doc.text(firstIndent, pageHeight + (margin + 3), firstName + " " + lastName);

        currentPage++;
        pdf_doc.text(pageWidth + (margin / 2), pageHeight + (margin + 3), currentPage.toString());
    }

    var addNewPdfPage = function() {
        pdf_doc.addPage();
        currentLine = firstLine;

        drawPageFooter();
    }

    var addCenteredText = function (fontSize, string, y) {
        pdf_doc.setFontSize(fontSize);
        var textWidth = (pdf_doc.getStringUnitWidth(string) / fontSize),
            x = (pageWidth - textWidth) / 2.0;
        pdf_doc.text(string, x, y);
    }

    // Draw dividing line.
    var drawDividingLine = function () {
        currentLine += 3;
        pdf_doc.setDrawColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
        pdf_doc.line(thirdIndent, currentLine, 192, currentLine);
    };

    // Splits a string into several lines based on a predefined length
    var splitIntoLines = function (input, maxLineWidth, indentLines) {
        var lines = [],                 // Return value
            tempInput = input.trim(),   // Temp variable used to store string input
            stringEnd,                  // Index of the last character of the current line
            done = false;               // Done scanning the input string

        // Remove double spaces
        while(tempInput.indexOf("  ") > -1){
            tempInput = tempInput.replace("  ", " ");
        }

        // Replace tabs with spaces
        tempInput = tempInput.replace('\t', ' ');

        // Parse input into separate strings
        while(!done){                                                                                               // Loop through the input string in "chunks"
            if(tempInput.length > 0) {                                                                              // Check if unchecked portion of the string is longer than 0 characters
                if(tempInput.length > maxLineWidth) {                                                               // Check if string needs to be split
                    stringEnd = tempInput.substr(0, Math.min(maxLineWidth, tempInput.length)).lastIndexOf(' ');     // Split the string at the last instance of a blank space in the current "chunk"

                    // If this line contains a newline character, push a new line to the lines array
                    var newlineComp = 0;
                    if (tempInput.substr(0, stringEnd).indexOf('\n') > -1) {
                        stringEnd = tempInput.substr(0, stringEnd).indexOf('\n') - 1;
                        newlineComp = 1;
                    }

                    // If this is the first line of the assessment verbiage, do not indent. Indent the rest.
                    if(lines.length == 0) {
                        lines.push(tempInput.substr(0, stringEnd));                                                     // Push the result to the lines[] array
                    } else {
                        if (indentLines) {
                            lines.push("   " + tempInput.substr(0, stringEnd));
                        } else {
                            lines.push(tempInput.substr(0, stringEnd));
                        }
                    }

                    // Store the remainder of the string in the tempInput variable
                    if (tempInput[stringEnd + 1] !== null) {
                        tempInput = tempInput.substr(stringEnd + newlineComp + 1, tempInput.length - newlineComp - 1);
                    } else {
                        tempInput = "";
                        done = true;
                    }
                } else {
                    // If this line contains a newline character, push a new line to the lines array
                    var newlineComp = 0;
                    if (tempInput.indexOf('\n') > -1) {
                        stringEnd = tempInput.indexOf('\n') - 1;
                        newlineComp = 1;

                        // If this is the first line of the assessment verbiage, do not indent. Indent the rest.
                        if (lines.length == 0) {
                            lines.push(tempInput.substr(0, stringEnd));
                        } else {
                            if (indentLines) {
                                lines.push("   " + tempInput.substr(0, stringEnd));
                            } else {
                                lines.push(tempInput.substr(0, stringEnd));
                            }
                        }

                        // Store the remainder of the string in the tempInput variable
                        if (tempInput[stringEnd + 1] !== null) {
                            tempInput = tempInput.substr(stringEnd + newlineComp + 1, tempInput.length - newlineComp - 1);
                        } else {
                            tempInput = "";
                            done = true;
                        }
                    } else {
                        // If this is the first line of the assessment verbiage, do not indent. Indent the rest.
                        if(lines.length == 0) {
                            lines.push(tempInput);
                        } else {
                            if (indentLines) {
                                lines.push("   " + tempInput);
                            } else {
                                lines.push(tempInput);
                            }
                        }
                        done = true;
                    }
                }
            } else {            // Unchecked portion of the string is empty
                done = true;    // Done with while loop
            }
        }

        return lines;
    }


    // Draw footer details on first page
    drawPageFooter();

    // Add student name to the header
    var currentLine = firstLine;
    if(renderPdfInColor) {
        pdf_doc.setDrawColor(0);
        pdf_doc.setFillColor(hexToRgb("#cccccc")[0], hexToRgb("#cccccc")[1], hexToRgb("#cccccc")[2]);
        pdf_doc.rect(firstIndent - 2, currentLine + 2, pageWidth, -8, 'F');
    } else {
        pdf_doc.setLineWidth(.5);
        pdf_doc.setDrawColor(0);
        pdf_doc.line(firstIndent, currentLine + 2, pageWidth + 9, currentLine + 2);
        pdf_doc.setLineWidth(0.1);
    }

    addCenteredText(largeFontSize, firstName + " " + lastName, currentLine);

    // Add Performance Tasks to the doc
    var pdfData = getAssessmentData(Session.get('selected_student'), function() {});

    // Loop through performance tasks
    function writePdf(a, pdfData, callback){
        //console.log('running writePdf');
        //console.log(a);
        //console.log(pdfData);
        if (pdfData.hasOwnProperty(a)) {
            if (pdfData[a].ptHasAssessments) {
                //console.log('actually running...');

                // Check if there is still space on current page
                if (currentLine >= pageHeight || justAddedImage) {
                    addNewPdfPage();
                }
                justAddedImage = false;

                currentLine += 8;

                // Render Performance Task in B/W or color
                if (renderPdfInColor) {
                    pdf_doc.setDrawColor(0);
                    pdf_doc.setFillColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                    pdf_doc.rect(firstIndent - 2, currentLine + 2, pageWidth, -8, 'F');     // Performance task name
                    pdf_doc.setTextColor(255);
                } else {
                    pdf_doc.setDrawColor(0);
                    pdf_doc.setTextColor(0);
                }

                pdf_doc.setFontSize(largeFontSize);
                pdf_doc.text(firstIndent, currentLine, pdfData[a].name);

                // Loop through studentDetails array in each performance task
                //console.log('looping through student details...')
                for (var b in pdfData[a].studentDetails) {
                    if (pdfData[a].studentDetails.hasOwnProperty(b)) {

                        // Check if this day has any assessments. If not, do not print it to the PDF.
                        if (pdfData[a].studentDetails[b].hasAssessments) {
                            // Check if there is still space on current page
                            if (currentLine >= pageHeight) {
                                addNewPdfPage();
                            }

                            currentLine += 8;
                            pdf_doc.setFontSize(largeFontSize);
                            pdf_doc.setTextColor(0);
                            if (pdfData[a].subject === "math") {
                                pdf_doc.text(secondIndent, currentLine, "Part " + pdfData[a].studentDetails[b].day);   // Part
                            } else {
                                pdf_doc.text(secondIndent, currentLine, "Day " + pdfData[a].studentDetails[b].day);   // Day
                            }

                            // Loop through standardsArray of studentDetails
                            for (var c in pdfData[a].studentDetails[b].standardsArray) {
                                if (pdfData[a].studentDetails[b].standardsArray.hasOwnProperty(c)) {

                                    // Check if this assessment has any values
                                    if (pdfData[a].studentDetails[b].standardsArray[c].hasAssessments) {

                                        // Check if there is still space on current page
                                        if (currentLine >= pageHeight) {
                                            addNewPdfPage();
                                        }

                                        // Draw assessment name
                                        currentLine += 6;
                                        pdf_doc.setFontSize(smallFontSize);
                                        pdf_doc.setTextColor(0);
                                        pdf_doc.setFillColor(hexToRgb('#CCCCCC')[0], hexToRgb('#CCCCCC')[1], hexToRgb('#CCCCCC')[2]);
                                        pdf_doc.rect(thirdIndent - 1, currentLine - 4, 160, rubricRectHeight, 'F');
                                        pdf_doc.text(thirdIndent, currentLine, pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.name);
                                        pdf_doc.text(157, currentLine, formattedDate(pdfData[a].studentDetails[b].standardsArray[c].data.timestamp));

                                        // Check is common core state standards are stored in array
                                        if (pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.commonCoreStateStandards && pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.commonCoreStateStandards.length > 0) {

                                            // Loop through CCSS in assessmentObject
                                            for (var d in pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.commonCoreStateStandards) {
                                                if (pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.commonCoreStateStandards.hasOwnProperty(d)) {
                                                    var tempStandardLineText = pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.commonCoreStateStandards[d]._id + " - " + ccssDescription(pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.commonCoreStateStandards[d]._id);
                                                    var tempStandardLineArray = splitIntoLines(tempStandardLineText, assessmentLineMaxChar, true);

                                                    // Check if there is still space on current page
                                                    if (currentLine + (6 * tempStandardLineArray.length) >= pageHeight) {
                                                        addNewPdfPage();
                                                    }

                                                    // Loop through each line in the current standard.
                                                    pdf_doc.setTextColor(0);
                                                    for (var i in tempStandardLineArray) {

                                                        /*// Check if there is still space on current page
                                                         if (currentLine >= pageHeight) {
                                                         addNewPdfPage();
                                                         }*/

                                                        if (tempStandardLineArray.hasOwnProperty(i)) {
                                                            currentLine += 6;
                                                            pdf_doc.setFontSize(smallFontSize);
                                                            pdf_doc.setTextColor(0);
                                                            pdf_doc.text(thirdIndent, currentLine, tempStandardLineArray[i]);  // Print the name and description of each standard.
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            if (pdfData[a].studentDetails[b].standardsArray[c].standardObject && pdfData[a].studentDetails[b].standardsArray[c].standardObject != "") {
                                                var tempStandardLineText = (pdfData[a].studentDetails[b].standardsArray[c].standardObject._id.trim() || "N/A") + ' - ' + (pdfData[a].studentDetails[b].standardsArray[c].standardObject.description || "N/A").trim();
                                                var tempStandardLineArray = splitIntoLines(tempStandardLineText, assessmentLineMaxChar, true);

                                                // Check if there is still space on current page
                                                if (currentLine + (6 * tempStandardLineArray.length) >= pageHeight) {
                                                    addNewPdfPage();
                                                }

                                                // Loop through each line in the current standard.
                                                pdf_doc.setTextColor(0);
                                                for (var i in tempStandardLineArray) {

                                                    /*// Check if there is still space on current page
                                                     if (currentLine >= pageHeight) {
                                                     addNewPdfPage();
                                                     }*/

                                                    if (tempStandardLineArray.hasOwnProperty(i)) {
                                                        currentLine += 6;
                                                        pdf_doc.setFontSize(smallFontSize);
                                                        pdf_doc.setTextColor(0);
                                                        pdf_doc.text(thirdIndent, currentLine, tempStandardLineArray[i]);  // Print the name and description of each standard.
                                                    }
                                                }
                                            }
                                        }


                                        currentLine += 1;

                                        // Handle rubric assessments
                                        if (pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.type == "rubric") {
                                            currentLine += 6;
                                            pdf_doc.setFontSize(smallFontSize);

                                            // Meeting
                                            pdf_doc.setTextColor(0);
                                            if (pdfData[a].studentDetails[b].standardsArray[c].data.value[0] == "Meeting") {

                                                // Draw PDF in color/B&W
                                                if (renderPdfInColor) {
                                                    pdf_doc.setDrawColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.setFillColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.roundedRect(thirdIndent, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(255);
                                                } else {
                                                    pdf_doc.setDrawColor(0);
                                                    pdf_doc.setFillColor(255);
                                                    pdf_doc.roundedRect(thirdIndent, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(0);
                                                }

                                            } else {
                                                pdf_doc.setTextColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
                                            }
                                            pdf_doc.text(thirdIndent + 10, currentLine, "Meeting");

                                            // Approaching
                                            pdf_doc.setTextColor(0);
                                            if (pdfData[a].studentDetails[b].standardsArray[c].data.value[0] == "Approaching") {

                                                // Draw PDF in color/B&W
                                                if (renderPdfInColor) {
                                                    pdf_doc.setDrawColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.setFillColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.roundedRect(thirdIndent + 41, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(255);
                                                } else {
                                                    pdf_doc.setDrawColor(0);
                                                    pdf_doc.setFillColor(255);
                                                    pdf_doc.roundedRect(thirdIndent + 41, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(0);
                                                }

                                            } else {
                                                pdf_doc.setTextColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
                                            }
                                            pdf_doc.text(thirdIndent + 48, currentLine, "Approaching");

                                            // Developing
                                            pdf_doc.setTextColor(0);
                                            if (pdfData[a].studentDetails[b].standardsArray[c].data.value[0] == "Developing") {

                                                // Draw PDF in color/B&W
                                                if (renderPdfInColor) {
                                                    pdf_doc.setDrawColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.setFillColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.roundedRect(thirdIndent + 78, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(255);
                                                } else {
                                                    pdf_doc.setDrawColor(0);
                                                    pdf_doc.setFillColor(255);
                                                    pdf_doc.roundedRect(thirdIndent + 78, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(0);
                                                }

                                            } else {
                                                pdf_doc.setTextColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
                                            }
                                            pdf_doc.text(thirdIndent + 86, currentLine, "Developing");

                                            // Emerging
                                            pdf_doc.setTextColor(0);
                                            if (pdfData[a].studentDetails[b].standardsArray[c].data.value[0] == "Emerging") {

                                                // Draw PDF in color/B&W
                                                if (renderPdfInColor) {
                                                    pdf_doc.setDrawColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.setFillColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                    pdf_doc.roundedRect(thirdIndent + 114, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(255);
                                                } else {
                                                    pdf_doc.setDrawColor(0);
                                                    pdf_doc.setFillColor(255);
                                                    pdf_doc.roundedRect(thirdIndent + 114, currentLine - 4, rubricRectWidth, rubricRectHeight, rectCornerRadius, rectCornerRadius, 'FD');
                                                    pdf_doc.setTextColor(0);
                                                }

                                            } else {
                                                pdf_doc.setTextColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
                                            }
                                            pdf_doc.text(thirdIndent + 124, currentLine, "Emerging");
                                            pdf_doc.setTextColor(0);
                                            currentLine += 2;


                                            // Handle yes/no lists
                                        } else if (pdfData[a].studentDetails[b].standardsArray[c].assessmentObject.type == "yesnolist") {
                                            pdf_doc.setFontSize(smallFontSize);

                                            var assessValuesObject = getYesNoListContext(pdfData[a].studentDetails[b].standardsArray[c]);

                                            for (var d in assessValuesObject.assessmentObject.assessValues) {                                            // Loop through assessments in standard
                                                if (assessValuesObject.assessmentObject.assessValues.hasOwnProperty(d)) {

                                                    // Check if there is still space on current page
                                                    if (currentLine >= pageHeight) {
                                                        addNewPdfPage();
                                                    }

                                                    if (assessValuesObject.assessmentObject.assessValues[d].isobserved != null && assessValuesObject.assessmentObject.assessValues[d].isobserved == "YES") {
                                                        var tempAssessmentLineText = "YES\b\b\b" + assessValuesObject.assessmentObject.assessValues[d].value;       // Store assessment description in a string and then convert to array of strings separated by line breaks
                                                    } else if (assessValuesObject.assessmentObject.assessValues[d].isobserved != null && assessValuesObject.assessmentObject.assessValues[d].isobserved == "NO") {
                                                        var tempAssessmentLineText = "NO\b\b\b\b\b" + assessValuesObject.assessmentObject.assessValues[d].value;
                                                    } else {
                                                        var tempAssessmentLineText = "\b\b\b\b\b\b\b\b\b\b" + assessValuesObject.assessmentObject.assessValues[d].value;
                                                    }

                                                    // Remove/replace special characters
                                                    tempAssessmentLineText = tempAssessmentLineText.replace("→", "->");

                                                    var tempAssessmentLineArray = splitIntoLines(tempAssessmentLineText, assessmentLineMaxChar, true);

                                                    // Check if there is still space on current page
                                                    if (currentLine + (6 * tempAssessmentLineArray.length) >= pageHeight) {
                                                        addNewPdfPage();
                                                    }

                                                    if (assessValuesObject.assessmentObject.assessValues[d].isobserved == "YES" || assessValuesObject.assessmentObject.assessValues[d].isobserved == "NO") {                                                       // Check if this assessment is selected
                                                        if (renderPdfInColor) {
                                                            pdf_doc.setDrawColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                            pdf_doc.setFillColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                            pdf_doc.roundedRect(thirdIndent, currentLine + 1, 155, 5 * tempAssessmentLineArray.length, rectCornerRadius, rectCornerRadius, 'FD');
                                                            pdf_doc.setTextColor(255);
                                                        } else {
                                                            pdf_doc.setDrawColor(0);
                                                            pdf_doc.setFillColor(255);
                                                            pdf_doc.roundedRect(thirdIndent, currentLine + 1, 155, 5 * tempAssessmentLineArray.length, rectCornerRadius, rectCornerRadius, 'FD');
                                                            pdf_doc.setTextColor(0);
                                                        }
                                                    } else {
                                                        pdf_doc.setTextColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
                                                    }

                                                    for (var i in tempAssessmentLineArray) {

                                                        /*// Check if there is still space on current page
                                                         if (currentLine >= pageHeight) {
                                                         addNewPdfPage();
                                                         }*/

                                                        // Print each line of the description
                                                        currentLine += 5;

                                                        pdf_doc.text(thirdIndent + 1, currentLine, tempAssessmentLineArray[i]);
                                                    }

                                                    currentLine += 1;
                                                }
                                            }
                                            currentLine += 2;

                                            // Handle checklist assessments
                                        } else {
                                            pdf_doc.setFontSize(smallFontSize);
                                            for (var d in pdfData[a].studentDetails[b].standardsArray[c].data) {                                            // Loop through assessments in standard
                                                if (pdfData[a].studentDetails[b].standardsArray[c].data.hasOwnProperty(d)) {

                                                    if (pdfData[a].studentDetails[b].standardsArray[c].data[d].value) {
                                                        var tempAssessmentLineText = "- " + pdfData[a].studentDetails[b].standardsArray[c].data[d].value.trim();       // Store assessment description in a string and then convert to array of strings separated by line breaks
                                                    } else {
                                                        var tempAssessmentLineText = "";
                                                    }

                                                    // Remove/replace special characters
                                                    tempAssessmentLineText = tempAssessmentLineText.replace("→", "->");

                                                    var tempAssessmentLineArray = splitIntoLines(tempAssessmentLineText, assessmentLineMaxChar, true);

                                                    // Check if there is still space on current page
                                                    if (currentLine + (6 * tempAssessmentLineArray.length) >= pageHeight) {
                                                        addNewPdfPage();
                                                    }

                                                    if (pdfData[a].studentDetails[b].standardsArray[c].data[d].checked) {                                                       // Check if this assessment is selected
                                                        if (renderPdfInColor) {
                                                            pdf_doc.setDrawColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                            pdf_doc.setFillColor(hexToRgb(pdfData[a].color)[0], hexToRgb(pdfData[a].color)[1], hexToRgb(pdfData[a].color)[2]);
                                                            pdf_doc.roundedRect(thirdIndent, currentLine + 1, 155, 5 * tempAssessmentLineArray.length, rectCornerRadius, rectCornerRadius, 'FD');
                                                            pdf_doc.setTextColor(255);
                                                        } else {
                                                            pdf_doc.setDrawColor(0);
                                                            pdf_doc.setFillColor(255);
                                                            pdf_doc.roundedRect(thirdIndent, currentLine + 1, 155, 5 * tempAssessmentLineArray.length, rectCornerRadius, rectCornerRadius, 'FD');
                                                            pdf_doc.setTextColor(0);
                                                        }
                                                    } else {
                                                        pdf_doc.setTextColor(hexToRgb(greyedOutTextColor)[0], hexToRgb(greyedOutTextColor)[1], hexToRgb(greyedOutTextColor)[2]);
                                                    }

                                                    for (var i in tempAssessmentLineArray) {

                                                        /*// Check if there is still space on current page
                                                         if (currentLine >= pageHeight) {
                                                         addNewPdfPage();
                                                         }*/

                                                        // Print each line of the description
                                                        currentLine += 5;

                                                        pdf_doc.text(thirdIndent + 1, currentLine, tempAssessmentLineArray[i]);
                                                    }

                                                    currentLine += 1;
                                                }
                                            }
                                            currentLine += 2;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // LS PAR-88: Adding note artifacts to the PDF
                Session.set('selected_performancetask', 'all');
                if (getHasNotes(getNoteDataForPerformanceTask(pdfData[a]._id))) {
                    //console.log('has note data for task');
                    var notes = getNoteDataForPerformanceTask(pdfData[a]._id);

                    currentLine += 8;

                    // Check if there is still space on current page
                    if (currentLine >= pageHeight) {
                        addNewPdfPage();
                    }

                    // Print the "notes" header on the page
                    pdf_doc.setFontSize(largeFontSize);
                    pdf_doc.setTextColor(0);
                    pdf_doc.text(secondIndent, currentLine, "Notes for " + pdfData[a].name.trim());

                    // Check if there is still space on current page
                    if (currentLine >= pageHeight) {
                        addNewPdfPage();
                    }

                    // Loop through each note
                    for (var x in notes) {

                        drawDividingLine();

                        // Check if there is still space on current page
                        if (currentLine >= pageHeight) {
                            addNewPdfPage();
                        }

                        // Print the standards and time for the current note
                        currentLine += 6;
                        pdf_doc.setFontSize(smallFontSize);
                        pdf_doc.setFontType("bold");
                        pdf_doc.setTextColor(0);

                        if (notes[x].commonCoreStateStandards) {
                            pdf_doc.text(thirdIndent, currentLine, formattedDate(notes[x].captured_date) + " - " + convertArrayToString(notes[x].commonCoreStateStandards));
                        } else {
                            pdf_doc.text(thirdIndent, currentLine, formattedDate(notes[x].captured_date));
                        }

                        // Get note text
                        var tempNotesLineText = notes[x].note;

                        // Split text into lines
                        var tempNotesLineArray = splitIntoLines(notes[x].note, assessmentLineMaxChar, false);

                        // Check if there is still space on current page
                        if (currentLine + (6 * tempAssessmentLineArray.length) >= pageHeight) {
                            addNewPdfPage();
                        }

                        for (var i in tempNotesLineArray) {

                            /*// Check if there is still space on current page
                             if (currentLine >= pageHeight) {
                             addNewPdfPage();
                             }*/

                            // Print the text of the current note
                            currentLine += 6;
                            pdf_doc.setFontSize(smallFontSize);
                            pdf_doc.setFontType("normal");
                            pdf_doc.setTextColor(0);

                            pdf_doc.text(thirdIndent, currentLine, tempNotesLineArray[i]);
                        }
                    }
                }
                //console.log("past note data");
                // if there is an image in a note for this performance task, render it on the pdf using this 'if' block
                if (getImageDataForPerformanceTask(pdfData[a]._id).length > 0) {
                    justAddedImage = true;
                    //console.log('image data here:');
                    var notes = getImageDataForPerformanceTask(pdfData[a]._id);
                    //console.log(notes);
                    // add image
                    imageLoop(0, notes, function(pdf_doc){
                        if (pdfData[a + 1] !== undefined) {
                            //console.log('recursing...(1)');
                            return writePdf(a + 1, pdfData, callback);
                        } else {
                            //console.log('evaluated to false:');
                            //console.log(pdfData[a + 1]);
                            //console.log('Calling the Callback!');
                            callback(pdf_doc);
                        }
                    });
                } else {
                    pdf_doc.setFontType("normal");
                    // END OF PAR-88

                    if (onePagePerPerformanceTask && a < pdfData.length - 1) {          // Add a new page for each new performance task if true
                        addNewPdfPage();
                    }
                    if (pdfData[a + 1] !== undefined) {
                        //console.log('recursing...(2)');
                        return writePdf(a + 1, pdfData, callback);
                    } else {
                        callback(pdf_doc);
                    }
                }
            }
        }
    }
    //call the writePdf function. Instead of a for loop there are recursive calls which ends with this callback
    writePdf(0, pdfData, function(pdf_doc) {
        //console.log('callback called!');
        if(pdf_doc) {
            // Build file name
            var filename = "student_details_" + firstName.toLowerCase() + "_" + lastName.toLowerCase() + ".pdf";

            filename = filename.replace(" ", "_");
            filename = filename.replace(",", "");
            filename = filename.replace("-", "_");
            filename = filename.replace("/", "_");

            console.log("Saving PDF...")
            pdf_doc.save(filename);
        }
    });
    /**
     * Input image info + url and return the callback with the base 64 img
     * @param url
     * @param pageWidth
     * @param callback
     * @param imageInfo
     * @param outputFormat
     */
    function convertImgToBase64URL(url, pageWidth, callback, imageInfo, outputFormat){
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function(){
            var canvas = document.createElement('CANVAS'),
                ctx = canvas.getContext('2d'), dataURL;
            canvas.height = this.height;
            canvas.width = this.width;
            var height = this.height * pageWidth * 3 / 4 / this.width;
            ctx.drawImage(this, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            //pass in base 64, height of image, and image Info
            callback(dataURL,pageWidth * 3/ 4, height,imageInfo);
            canvas = null;
        };
        img.src = url;
    }

    /**
     * Loop Through images, if loop is not done, call fileLoop. Else, run the callback.
     * @param p index for notes array
     * @param notes
     * @param callback
     * @returns {*}
     */
    function imageLoop(p, notes, callback) {
        //console.log("image Loop called!");
        //console.log(notes);
        //console.log(notes[p]);
        if (notes[p] !== undefined) {
            return fileLoop(0, p, notes, notes[p].file, callback)
        } else {
            return callback(pdf_doc);
        }

    }

    /**
     * Loop through files. If loop is done, then re-call imageLoop
     * @param d index for file array
     * @param p index for notes array
     * @param notes
     * @param file
     * @param callback
     * @returns {*}
     */
    function fileLoop(d, p, notes, file, callback){
        if (file[d] !== undefined) {
            var imageData = MediaData.findOne({_id: file[d]._id});
            //convert image to base 64
            //console.log('fileLoop');
            //console.log(notes);
            //console.log(file);
            if (imageData) {
                convertImgToBase64URL(imageData.url(), pageWidth, function (base64Img, width, imageHeight, imageInfo) {
                    //add new page if the image will be cutoff
                    if (currentLine + imageHeight + 11 > pageHeight) {
                        addNewPdfPage();
                    }

                    currentLine += 6;
                    pdf_doc.setFontSize(smallFontSize);
                    pdf_doc.setFontType("bold");
                    pdf_doc.setTextColor(0);

                    //add date and standards if applicable
                    if (notes[p].commonCoreStateStandards.length) {
                        pdf_doc.text(thirdIndent, currentLine, formattedDate(notes[p].captured_date) + " - " + convertArrayToString(notes[p].commonCoreStateStandards));
                    } else {
                        pdf_doc.text(thirdIndent, currentLine, formattedDate(notes[p].captured_date));
                    }
                    pdf_doc.setFontType("normal");


                    //add image to pdf
                    pdf_doc.addImage(base64Img, imageInfo.extension(), pageWidth/4 - 8, currentLine + 5, width, imageHeight);
                    currentLine += imageHeight + 6;
                    return fileLoop(d + 1, p, notes, file, callback)
                }, imageData);
            }
        } else {
            return imageLoop(p+1, notes, callback);
        }
    }
};

// Converts hex color value to RGB
var hexToRgb = function (hexColor){
    // Remove '#'
    hexColor = ((hexColor.charAt(0)=="#") ? hexColor.substring(1,7):hexColor);

    // Parse out RGB values
    var r = parseInt((hexColor).substring(0,2),16),
        g = parseInt((hexColor).substring(2,4),16),
        b = parseInt((hexColor).substring(4,6),16);

    var rgb = [r, g, b];

    return rgb;
}

var getYesNoListContext = function (object) {
    var returnObject = object;

    for (var assessValuesIndex in returnObject.assessmentObject.assessValues) {
        if (returnObject.assessmentObject.assessValues.hasOwnProperty(assessValuesIndex)) {
            // Check if this assessValue has been marked yes, no, or not at all
            for (var dataValuesIndex in returnObject.data.value) {
                if (returnObject.data.value.hasOwnProperty(dataValuesIndex)) {
                    if (returnObject.assessmentObject.assessValues[assessValuesIndex].value == returnObject.data.value[dataValuesIndex].observation) {
                        returnObject.assessmentObject.assessValues[assessValuesIndex].isobserved = returnObject.data.value[dataValuesIndex].isobserved.toUpperCase();
                    }

                    returnObject.assessmentObject.assessValues[assessValuesIndex].observation = returnObject.data.value[dataValuesIndex].observation.toUpperCase();
                }
            }
        }
    }

    for (var assessValuesIndex in returnObject.assessmentObject.assessValues) {
        if (returnObject.assessmentObject.assessValues.hasOwnProperty(assessValuesIndex)) {

            if (returnObject.assessmentObject.assessValues[assessValuesIndex].isobserved != null) {

                // Add background colors to the objects
                if (returnObject.assessmentObject.assessValues[assessValuesIndex].isobserved == "YES") {
                    returnObject.assessmentObject.assessValues[assessValuesIndex].background = "#009900";
                    returnObject.assessmentObject.assessValues[assessValuesIndex].color = "#FFFFFF";

                } else if (returnObject.assessmentObject.assessValues[assessValuesIndex].isobserved == "NO") {
                    returnObject.assessmentObject.assessValues[assessValuesIndex].background = "#3179D0";
                    returnObject.assessmentObject.assessValues[assessValuesIndex].color = "#FFFFFF";
                }
            } else if (returnObject.assessmentObject.assessValues[assessValuesIndex].observation === "FLAG") {
                returnObject.assessmentObject.assessValues[assessValuesIndex].background = "#FF6600";
                returnObject.assessmentObject.assessValues[assessValuesIndex].color = "#FFFFFF";
            } else {
                returnObject.assessmentObject.assessValues[assessValuesIndex].color = "#7B7B7B";
            }
        }
    }

    return returnObject;
}



/** jQuery **/

$(document).ready(function() {
    // Used to properly size the details container to make it scrollable
    $(window).resize(function () {
        var height = $(this).height() - $('#parcc-nav').height() - $('#student-details-name-header').height();
        $('#scroll-container').height(height);

        height = $(this).height() - $('#parcc-nav').height() - $('#class-details-filter-bar').height() - $('#sort-toolbar').height() - 5;
        $('#students-list-container').height(height);
    });
});