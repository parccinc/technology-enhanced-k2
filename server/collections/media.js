///////////////////////////////////////////////////////////////////////////////
// Media

/*
 Media data collected by the teacher
 */
Meteor.methods({
    getMedia: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return Media.findOne(id);
    },
    removeMedia: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        if (Meteor.user()) {
            check(id, String);
            return Media.remove(id);
        } else {
            return false;
        }
    },
    addMedia: function (mediaFileData) {
        if (Meteor.userId() === null){
            return false;
        }
        if (mediaFileData.note && mediaFileData.note.trim() != "" || mediaFileData.type === "image") {
            return Media.insert(mediaFileData);
        } else {
            return false;
        }
    },
    updateMedia: function (id, mediaFileData) {
        if (Meteor.userId() === null){
            return false;
        }
        if (mediaFileData.note && mediaFileData.note.trim() != "") {
            return Media.update({_id: id}, {$set: mediaFileData});
        } else {
            return false;
        }
    }
});
