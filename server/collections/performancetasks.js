///////////////////////////////////////////////////////////////////////////////
// Performance Tasks

/*
 Each peformance task is represented by a document in the PerformanceTasks collection:
 _id: ObjectId
 grade: String (K, 1, 2)
 subject: String (math, ela)
 name: String (name of the performance task)
 author: String (author of the book, if applicable)
 document: String (url to instruction file)
 ebook: String (url to instruction file)
 studentResource: String (url to student resource file)
 outline: String (url to outline file)
 visual: String (link to task image)
 color: color coding to use for the task and rubrics
 commonCoreStateStandards: Array of common core state standards id's {_id}
 assessmentSchedule: Array of days, step, responseType, with Array of assessments per day
 and their possible responses
 [{day, dayInstructions, assessments: {_id, assessment_id, step, stepType, responseType, studentWorkProduct, standardsEvidence, assessValues: [{_id, possibleResponse, isPossibleResponseImage}]} }]
 instructions: {plan, overview, outline, readySetGo, steps, ccssEvidence, ifThen}
 lastModifiedBy: Meteor user _id
 lastModifiedDate: Date
 */
Meteor.methods({
    getPerformanceTask: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return PerformanceTasks.findOne(id);
    },
    removePerformanceTask: function (id) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        check(id, String);
        return PerformanceTasks.remove(id);
    },
    addPerformanceTask: function (performanceTaskData) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        // Check if all fields are filled out
        if (performanceTaskData.grade       && performanceTaskData.grade.trim() != "" &&
            performanceTaskData.subject     && performanceTaskData.subject.trim() != "" &&
            performanceTaskData.name        && performanceTaskData.name.trim() != "")
        {
            // Check if the steps and responses are filled out
            for (var i = 0; i < performanceTaskData.assessmentSchedule.length; i++) {
                for (var j = 0; j < performanceTaskData.assessmentSchedule[i].assessments.length; j++) {
                    if (performanceTaskData.assessmentSchedule.assessments[i].step.trim() == "") {
                        return false;
                    }
                }
            }

            performanceTaskData.lastModifiedBy = Meteor.userId();
            performanceTaskData.lastModifiedDate = new Date();

            var newPT = PerformanceTasks.insert(performanceTaskData);

            return newPT;
        } else {
            return false;
        }
    },
    updatePerformanceTask: function (id, performanceTaskData) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        if (performanceTaskData.grade       && performanceTaskData.grade.trim() != "" &&
            performanceTaskData.subject     && performanceTaskData.subject.trim() != "" &&
            performanceTaskData.name        && performanceTaskData.name.trim() != "")
        {
            // Check if steps and responses are filled out
            for (var i = 0; i < performanceTaskData.assessmentSchedule.length; i++) {
                for (var j = 0; j < performanceTaskData.assessmentSchedule[i].assessments.length; j++) {
                    if (performanceTaskData.assessmentSchedule[i].assessments[j].step.trim() == "") {
                        return false;
                    }
                }
            }

            performanceTaskData.lastModifiedBy = Meteor.userId();
            performanceTaskData.lastModifiedDate = new Date();

            return PerformanceTasks.update({_id: id}, {$set: performanceTaskData});
        } else {
            return false;
        }
    }
});