///////////////////////////////////////////////////////////////////////////////
// Users

/*
 Each User is represented by a document in the users collection:
 _id: ObjectId
 username: String
 profile: {
            firstName: String,
            lastName: String,
            grade: String, eg. K, 1, 2
            classrooms: Map of teacher classrooms
            groups: Map of teacher's saved groups
            sharedclassrooms: Array of other classrooms the teacher has access to
                for grouping purposes
            journal: Array of journal entries object: {_id, performance_task_id, class_id, class_info, strengths_needs_info, actions_info, comments},
            annotations: Array of annotation entries object: {_id, performance_task_id, assessment_id, instruction, annotation_date, annotation}
          }
 */

Meteor.methods({
    getUserToken: function (userId) {
        if (Meteor.userId() === null){
            return false;
        }
        if (userId === Meteor.userId()) {
            return  Meteor.users.findOne({_id: userId}, {fields: {services: 1}});
        }
    },
    getUserByEmail: function (email) {
        if (Meteor.userId() === null){
            return false;
        }
        if (email) {
            return Meteor.users.findOne({ emails: { $elemMatch: { address: email } } });
        }
    },
    updateUser: function (userId, user) {
        if (Meteor.userId() === null){
            return false;
        }
        if (userId === Meteor.userId()) {
            return Meteor.users.update({
                _id: userId
            },
            {
                $set: {
                    "profile.firstName": user.profile.firstName,
                    "profile.lastName": user.profile.lastName,
                    emails: user.emails
                }
            }
            );
        }
    },
    addUserClassroom: function (classroom) {
        if (Meteor.userId() === null){
            return false;
        }
        var userClassrooms = Meteor.users.find({ _id: Meteor.userId(), "profile.classrooms.name": { $in: [classroom.name] } }).count();

        if (userClassrooms > 0) {
            return "classroomExists";
        } else {
            if (classroom.name.trim() != "" && classroom.grade.trim() != "") {
                return Meteor.users.update({_id: Meteor.userId()}, {$addToSet: {'profile.classrooms': classroom}});
            } else {
                return "blankValue";
            }
        }
    },
    editUserClassroom: function (classroom) {
        if (Meteor.userId() === null){
            return false;
        }
        if (classroom.name && classroom.name.trim() != ""
            && classroom.grade && classroom.grade.trim() != "") {
            return Meteor.users.update({
                _id: Meteor.userId(),
                'profile.classrooms._id': classroom._id
            }, {$set: {"profile.classrooms.$.name": classroom.name, "profile.classrooms.$.grade": classroom.grade}})
        } else {
            return "blankValue";
        }
    },
    removeUserClassroom: function (classroom) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: Meteor.userId()}, { $pull: { 'profile.classrooms': classroom } });
    },
    addUserGroup: function (group) {
        if (Meteor.userId() === null){
            return false;
        }
        var userGroups = Meteor.users.find({ _id: Meteor.userId(), "profile.groups.name": { $in: [group.name] } }).count();

        if (userGroups > 0) {
            return false;
        } else {
            if (group.name.trim() != "" && group.grade.trim() != "") {
                return Meteor.users.update({_id: Meteor.userId()}, { $addToSet: { 'profile.groups': group } });
            } else {
                return "blankValue";
            }
        }
    },
    editUserGroup: function (group) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: Meteor.userId(), 'profile.groups._id': group._id}, { $set: { "profile.groups.$" : group } });
    },
    editSpecifiedUserGroup: function (userId, group) {
        if (Meteor.userId() === null){
            return false;
        }
        if (Meteor.userId() !== userId){
            return false
        }
        return Meteor.users.update({_id: userId, 'profile.groups._id': group._id}, { $set: { "profile.groups.$" : group } });
    },
    removeUserGroup: function (group) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: Meteor.userId()}, { $pull: { 'profile.groups': group } });
    },
    addSharedUserGroup: function (userId, groupId) {
        if (Meteor.userId() === null){
            return false;
        }
        var userSharedGroups = Meteor.users.find({ _id: userId, "profile.sharedgroups._id": { $in: [groupId] } }).count();

        if (userSharedGroups > 0) {
            return "groupExists";
        } else {
            return Meteor.users.update({_id: userId}, {$addToSet: {'profile.sharedgroups': {"_id": groupId }}});
        }
    },
    removeSharedGroup: function (userId, groupId) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: userId}, { $pull: { 'profile.sharedgroups': {"_id": groupId } } });
    },
    addUserJournalEntry: function (journalEntry) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: Meteor.userId()}, {$addToSet: {'profile.journal': journalEntry}});
    },
    editUserJournalEntry: function (journalEntry) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: Meteor.userId(), 'profile.journal._id': journalEntry._id}, { $set: { "profile.journal.$" : journalEntry } });
    },
    removeUserJournalEntry: function (journalEntry) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: Meteor.userId()}, { $pull: { 'profile.journal': journalEntry } });
    },
    addUserAnnotationEntry: function (annotationEntry) {
        if (Meteor.userId() === null){
            return false;
        }
        if (annotationEntry.annotation && annotationEntry.annotation.trim() != "") {
            return Meteor.users.update({_id: Meteor.userId()}, {$addToSet: {'profile.annotations': annotationEntry}});
        } else {
            return false;
        }
    },
    editUserAnnotationEntry: function (annotationEntry) {
        if (Meteor.userId() === null){
            return false;
        }
        if (annotationEntry.annotation && annotationEntry.annotation.trim() != "") {
            return Meteor.users.update({
                _id: Meteor.userId(),
                'profile.annotations._id': annotationEntry._id
            }, {$set: {"profile.annotations.$": annotationEntry}});
        } else {
            return false;
        }
    },
    removeUserAnnotationEntry: function (annotationEntry) {
        if (Meteor.userId() === null){
            return false;
        }
        return Meteor.users.update({_id: Meteor.userId()}, { $pull: { 'profile.annotations': annotationEntry } });
    }

    //clearUserData: function () {
    //    Meteor.users.update({}, { $set: { 'profile.groups': [] } }, {multi:true});
    //    Meteor.users.update({}, { $set: { 'profile.annotations': [] } }, {multi:true});
    //    Meteor.users.update({}, { $set: { 'profile.journal': [] } }, {multi:true});
    //    Meteor.users.update({}, { $set: { 'profile.sharedclassrooms': [] } }, {multi:true});
    //    Media.remove({});
    //    MediaData.remove({});
    //    Students.remove({'classroom_id': {$ne: "9a1dfcfe6ff794bfcaf1b89e"}});
    //
    //    return Meteor.users.update({'profile.classrooms._id': {$ne: "9a1dfcfe6ff794bfcaf1b89e"}}, { $set: { 'profile.classrooms': [] } }, {multi:true});
    //}
});
