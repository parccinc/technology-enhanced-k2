///////////////////////////////////////////////////////////////////////////////
// Students

/*
 Each student is represented by a document in the Students collection:
 _id: ObjectId
 classroom_id: ObjectId of the student belongs to
 firstname: String
 lastname: String
 assessmentdata: Array of assessments for the student
    performance task id->day->assessment id->{value: [array of values], timestamp: date}
 */

Meteor.methods({
    getStudent: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return Students.findOne(id);
    },
    removeStudent: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return Students.remove(id);
    },
    addStudent: function (studentData) {
        if (Meteor.userId() === null){
            return false;
        }
        if (studentData.firstname && studentData.firstname.trim() != "") {
            return Students.insert(studentData);
        } else {
            return false;
        }
    },
    updateStudent: function (id, studentData) {
        if (Meteor.userId() === null){
            return false;
        }
        return Students.update({_id: id}, { $set: studentData });
    },
    updateChecklistStudentAssessments: function (ids, setVal, setTimestamp, pullVal, removeSet) {
        if (Meteor.userId() === null){
            return false;
        }
        if (removeSet.toString().length > 0) {
            Students.update({_id: { $in: ids } }, {$pull: {removeSet: {$exists: true}}}, { multi: true });
        } else {
            Students.update({_id: { $in: ids } }, { $pull: pullVal }, { multi: true });
        }

        return Students.update({_id: { $in: ids } }, { $addToSet: setVal, $set: setTimestamp }, { multi: true });
    },
    updateYesnolistStudentAssessments: function (ids, setVal, setTimestamp, pullListVal, pullVal, removeSet) {
        if (Meteor.userId() === null){
            return false;
        }
        if (removeSet.toString().length > 0) {
            Students.update({_id: { $in: ids } }, {$pull: {removeSet: {$exists: true}}}, { multi: true });
        } else {
            Students.update({_id: { $in: ids } }, { $pull: pullVal }, { multi: true });
            Students.update({_id: { $in: ids } }, {$pull: pullListVal}, { multi: true });
        }

        return Students.update({_id: { $in: ids } }, { $addToSet: setVal, $set: setTimestamp }, { multi: true });
    },
    removeChecklistStudentAssessments: function (ids, setVal) {
        if (Meteor.userId() === null){
            return false;
        }
        return Students.update({_id: { $in: ids } }, { $pull: setVal }, { multi: true });
    },
    updateRubricStudentAssessments: function (ids, setVal) {
        if (Meteor.userId() === null){
            return false;
        }
        return Students.update({_id: { $in: ids } }, { $set: setVal }, { multi: true });
    },
    removeRubricStudentAssessments: function (ids, setVal) {
        if (Meteor.userId() === null){
            return false;
        }
        return Students.update({_id: { $in: ids } }, { $unset: setVal }, { multi: true });
    }
});
