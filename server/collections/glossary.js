///////////////////////////////////////////////////////////////////////////////
// Glossary Terms

/*
 Glossary collection:
 _id: ObjectId
 term: String
 definition: String
 lastModifiedBy: Meteor user _id
 lastModifiedDate: Date
 */
Meteor.methods({
    getGlossaryTerm: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return Glossary.findOne(id);
    },
    removeGlossaryTerm: function (glossaryData) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        return Glossary.remove(glossaryData._id);
    },
    addGlossaryTerm: function (glossaryData) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        if (glossaryData.term        && glossaryData.term.trim() != "" &&
            glossaryData.definition      && glossaryData.definition.trim() != "") {

            glossaryData.lastModifiedBy = Meteor.userId();
            glossaryData.lastModifiedDate = new Date();

            return Glossary.insert(glossaryData);
        } else {
            return "missingGlossaryData";
        }
    },
    updateGlossaryTerm: function (id, glossaryData) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        if (glossaryData.term        && glossaryData.term.trim() != "" &&
            glossaryData.definition      && glossaryData.definition.trim() != "" &&
            id                          && id.trim() != "") {

            glossaryData.lastModifiedBy = Meteor.userId();
            glossaryData.lastModifiedDate = new Date();

            return Glossary.update({_id: id}, { $set: glossaryData });
        } else {
            return "missingGlossaryData";
        }
    }
});