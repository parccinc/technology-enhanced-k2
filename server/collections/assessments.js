///////////////////////////////////////////////////////////////////////////////
// Assessments

/*
 Each assessment is represented by a document in the Assessments collection:
 _id: ObjectId
 grade: String (K, 1, or 2)
 subject: String (ela or math)
 type: String (rubric or checklist)
 name: String (RI1 Comprehension Rubric)
 displayName: String
 assessValues: Array of assessment possible values {_id, value, description}
 commonCoreStateStandards: Array of common core state standards {_id}
 lastModifiedBy: Meteor user _id
 lastModifiedDate: Date
 */
Meteor.methods({
    getAssessment: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return Assessments.findOne(id);
    },
    removeAssessment: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return Assessments.remove(id);
    },
    addAssessment: function (assessmentData) {
        // LS PAR-133: Adding user input validation
        if (Meteor.userId() === null) {
            return false;
        }

        if (assessmentData.grade        && assessmentData.grade.trim() != "" &&
            assessmentData.subject      && assessmentData.subject.trim() != "" &&
            assessmentData.type         && assessmentData.type.trim() != "" &&
            assessmentData.name         && assessmentData.name.trim() != "")
        {
            // Check if assessment has options
            if (!assessmentData.assessValues.length > 0) {
                return "noAssessments";
            }

            // Check that each assessment option is filled out
            for (var i = 0; i < assessmentData.assessValues.length; i++) {
                if (assessmentData.assessValues[i].value.trim() == "") {
                    return "blankAssessmentoValues";
                }
            }

            assessmentData.lastModifiedBy = Meteor.userId();
            assessmentData.lastModifiedDate = new Date();

            return Assessments.insert(assessmentData);
        } else {
            return "missingAssessmentData";
        }
        // END OF PAR-133
    },
    updateAssessment: function (id, assessmentData) {
        if (Meteor.userId() === null){
            return false;
        }
        // LS PAR-133: Adding user input validation
        if (assessmentData.grade        && assessmentData.grade.trim() != "" &&
            assessmentData.subject      && assessmentData.subject.trim() != "" &&
            assessmentData.type         && assessmentData.type.trim() != "" &&
            assessmentData.name         && assessmentData.name.trim() != "" &&
            id                          && id.trim() != "")
        {
            // Check if assessment has options
            if (!assessmentData.assessValues.length > 0) {
                return "noAssessments";
            }

            // Check that each assessment option is filled out
            for (var i = 0; i < assessmentData.assessValues.length; i++) {
                if (assessmentData.assessValues[i].value.trim() == "") {
                    return "blankAssessmentoValues";
                }
            }

            assessmentData.lastModifiedBy = Meteor.userId();
            assessmentData.lastModifiedDate = new Date();

            return Assessments.update({_id: id}, { $set: assessmentData });
        } else {
            return "missingAssessmentData";
        }
        // END OF PAR-133
    }
});