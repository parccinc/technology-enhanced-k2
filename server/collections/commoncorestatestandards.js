///////////////////////////////////////////////////////////////////////////////
// Common Core State Standards

/*
 Each common core state standard is represented by a document in the CommonCoreStateStandards collection:
 _id: String (eg: RF.K.1)
 grade: String (eg: K, 1, 2)
 subject: String (eg: ela or math)
 description: String
 lastModifiedBy: Meteor user _id
 lastModifiedDate: Date
 */
Meteor.methods({
    getCommonCoreStateStandard: function (id) {
        if (Meteor.userId() === null){
            return false;
        }
        check(id, String);
        return CommonCoreStateStandards.findOne(id);
    },
    removeCommonCoreStateStandard: function (id) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        check(id, String);
        return CommonCoreStateStandards.remove(id);
    },
    addCommonCoreStateStandard: function (standardData) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        // LS PAR-133: Validate user input
        if (standardData.grade &&       standardData.grade.trim() != "" &&
            standardData.subject &&     standardData.subject.trim() != "" &&
            standardData._id &&         standardData._id.trim() != "" &&
            standardData.description && standardData.description.trim() != "")
        {
            standardData.lastModifiedBy = Meteor.userId();
            standardData.lastModifiedDate = new Date();

            return CommonCoreStateStandards.insert(standardData);
        } else {
            return false;
        }
        // END OF PAR-133
    },
    updateCommonCoreStateStandard: function (id, standardData) {
        if (Meteor.userId() === null || !isAdmin()){
            return false;
        }
        // LS PAR-133: Validate user input
        if (standardData.grade &&       standardData.grade.trim() != "" &&
            standardData.subject &&     standardData.subject.trim() != "" &&
            standardData.description && standardData.description.trim() != "" &&
            id &&                       id != "")
        {
            standardData.lastModifiedBy = Meteor.userId();
            standardData.lastModifiedDate = new Date();

            return CommonCoreStateStandards.update({_id: id}, { $set: standardData });
        } else {
            return false;
        }
        // END OF PAR-133
    }
});
