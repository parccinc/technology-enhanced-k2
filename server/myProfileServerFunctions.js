/**
 * Created by sgi-dev007 on 3/31/15.
 */
Meteor.methods({
    // Validates and updates the user's email address
    updateEmail: function (currentUser, input) {
        if (Meteor.userId() === null){
            return false;
        }
        // Check if user input is blank
        if (currentUser._id !== Meteor.userId()){
            return false;
        }
        if (input == "") {
            return "blankInput";
        }

        // Check if user input is valid format
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        if (!re.test(input)){
            return "invalidEmailFormat";
        }

        // Check if email is currently in use by another user
        var emailTaken = Meteor.users.find({ "emails.address": input }).count();

        if (emailTaken > 0) {
            return "emailAlreadyTaken";
        }

        // Check if user is valid
        if (currentUser != null) {
            currentUser.emails[0].address = input;
            delete currentUser._id;

            // Update email in DB
            Meteor.call('updateUser', Meteor.userId(), currentUser, function (err, result) {
                return result;
            });
        }
    },
    // Validates and updates the user's last name
    updateLastName: function (currentUser, input) {
        if (Meteor.userId() === null){
            return false;
        }
        if (currentUser._id !== Meteor.userId()){
            return false;
        }
        // Check if user input contains non-alphanumeric characters
        if (input !== input.replace(/\W/g, '')) {
            return "invalidChar";
        }

        // Check if user input contains a number
        var hasNumber = /\d/;
        if (hasNumber.test(input)){
            return "invalidChar";
        }

        // Check if user input is blank
        if (input == "") {
            return "blankInput";
        }

        // Check if user is valid
        if (currentUser != null) {
            currentUser.profile.lastName = input;
            delete currentUser._id;

            // Update last name in DB
            Meteor.call('updateUser', Meteor.userId(), currentUser, function (err, result) {
                return result;
            });
        }
    },
    // Validates and updates the user's first name
    updateFirstName: function (currentUser, input) {
        if (Meteor.userId() === null){
            return false;
        }
        if (currentUser._id !== Meteor.userId()){
            return false;
        }
        // Check if user input contains invalid characters
        if (input !== input.replace(/\W/g, '')) {
            return "invalidChar";
        }

        // Check if user input contains numbers
        var hasNumber = /\d/;
        if (hasNumber.test(input)){
            return "invalidChar";
        }

        // Check if user input is blank
        if (input == "") {
            return "blankInput";
        }

        // Check if user is valid
        if (currentUser != null) {
            currentUser.profile.firstName = input;
            delete currentUser._id;

            // Update first name in DB
            Meteor.call('updateUser', Meteor.userId(), currentUser, function (err, result) {
                return result;
            });
        }
    },
    // Validates and updates the user's password
    updatePassword: function (currentPass, newPass, confirmNewPass) {
        if (Meteor.userId() === null){
            return false;
        }
        if (currentUser._id !== Meteor.userId()){
            return false;
        }
        if(Accounts._checkPassword(Meteor.user(), currentPass).error !== undefined) {
            return "wrongPassword";
        }

        var containsLetter = /[a-z]/i;

        // Check if any fields are blank
        if (currentPass == "" || newPass == "" || confirmNewPass == "") {
            return "emptyFields";
        }

        // Check if password is at least 7 characters
        if (newPass.length < 7) {
            return "passwordTooShort";
        }

        // Check that new passwords match
        if (newPass !== confirmNewPass) {
            return "passwordsDoNotMatch";
        }

        // Check if password contains at least one letter
        if (!containsLetter.test(newPass)) {
            return "passwordMustContainLetter";
        }


        // Change password
        return Accounts.setPassword(Meteor.userId(), newPass, { logout: false});
    }
});