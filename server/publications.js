// Publish students by classroom selected
Meteor.publish('classroomStudents', function (classroomId) {
    if (this.userId && classroomId != null) {
        check(classroomId, String);

        return Students.find({classroom_id: classroomId});
    }
});

// Reactive version of the studentsMedia publication

Meteor.publishComposite('reactiveStudentsMedia', function(classroomId) {
    return {
        find: function() {
            if (!this.userId || classroomId == "" || classroomId == null) {
                return false;
            }
            /** Get students for the current classroom/group **/
            var user = Meteor.users.findOne({ _id: this.userId }),
                groups = user.profile.groups,
                groupArray = [],
                sharedgroupsArray = [];

            for (var i in groups) {
                if (groups.hasOwnProperty(i)) {
                    if (groups[i]._id == classroomId) {
                        groupArray.push.apply(groupArray, groups[i].students);
                    }
                }
            }

            if (user.profile.sharedgroups) {
                var userSharedGroupIds = user.profile.sharedgroups.map(function(e) { return e._id; });

                var users = Meteor.users.find(
                    {'profile.groups': { $elemMatch: { _id: {$in: userSharedGroupIds} } }},
                    { fields: {profile: 1} });

                var usersArray = users.fetch();

                for (var i = 0; i < userSharedGroupIds.length; i++) {
                    var currentId = userSharedGroupIds[i];

                    for (var j = 0; j < usersArray.length; j++) {
                        if (usersArray[j].profile.groups) {
                            for (var k = 0; k < usersArray[j].profile.groups.length; k++) {
                                if (usersArray[j].profile.groups[k]._id === currentId) {
                                    sharedgroupsArray.push.apply(sharedgroupsArray, usersArray[j].profile.groups[k].students);

                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return Students.find({
                $or: [
                    {
                        classroom_id: classroomId
                    },
                    {
                        _id: {
                            $in: groupArray
                        }
                    },
                    {
                        _id: {
                            $in: sharedgroupsArray
                        }
                    }
                ]
            });
        },
        children: [
            {
                find: function(student) {
                    if (!this.userId || classroomId == "" || classroomId == null) {
                        return false;
                    }
                   /* *//** Get media **//*
                    var stds = students.fetch(),
                        studentIds = [];

                    for (var i in stds) {
                        if (stds.hasOwnProperty(i)) {
                            studentIds.push(stds[i]._id);
                        }
                    }*/

                    return Media.find({
                        students: student._id
                    });
                },
                children: [
                    {
                        find: function(media) {
                            if (!this.userId || classroomId == "" || classroomId == null) {
                                return false;
                            }
                            /** Get media data **/
                            var mediaDataArray = [];
                            if (media.type === 'image'){
                                var med = media.file;

                                for (var i in med) {
                                    if (med.hasOwnProperty(i)) {
                                        mediaDataArray.push(med[i]._id);
                                    }
                                }
                            }

                            return MediaData.find({
                                _id: {
                                    $in: mediaDataArray
                                }
                            });
                        }
                    }
                ]
            },
            {
                find: function(student) {
                    if (!this.userId || classroomId == "" || classroomId == null) {
                        return false;
                    }
                    /** Get performance tasks **/
                    var assessmentIds = [];

                    for (var index in student.assessmentdata){
                        //assessmentIds.push(index);
                    }

                    return PerformanceTasks.find({_id:{$in:assessmentIds}});
                }
            }
        ]
    }
});

// Publish media and students by classroom/group selected
Meteor.publish('studentsMedia', function (classroomId) {
    if (this.userId && classroomId) {

        /** Get students for the current classroom/group **/
        var user = Meteor.users.findOne({ _id: this.userId }),
            groups = user.profile.groups,
            groupArray = [],
            sharedgroupsArray = [];

        for (var i in groups) {
            if (groups.hasOwnProperty(i)) {
                if (groups[i]._id == classroomId) {
                    groupArray.push.apply(groupArray, groups[i].students);
                }
            }
        }

        if (user.profile.sharedgroups) {
            var userSharedGroupIds = user.profile.sharedgroups.map(function(e) { return e._id; });

            var users = Meteor.users.find(
                {'profile.groups': { $elemMatch: { _id: {$in: userSharedGroupIds} } }},
                { fields: {profile: 1} });

            var usersArray = users.fetch();

            for (var i = 0; i < userSharedGroupIds.length; i++) {
                var currentId = userSharedGroupIds[i];

                for (var j = 0; j < usersArray.length; j++) {
                    if (usersArray[j].profile.groups) {
                        for (var k = 0; k < usersArray[j].profile.groups.length; k++) {
                            if (usersArray[j].profile.groups[k]._id === currentId) {
                                sharedgroupsArray.push.apply(sharedgroupsArray, usersArray[j].profile.groups[k].students);

                                break;
                            }
                        }
                    }
                }
            }
        }

        var students = Students.find({
            $or: [
                {
                    classroom_id: classroomId
                },
                {
                    _id: {
                        $in: groupArray
                    }
                },
                {
                    _id: {
                        $in: sharedgroupsArray
                    }
                }
            ]
        });

        /** Get media **/
        var stds = students.fetch(),
            studentIds = [],
            assessmentIds = [];

        for (var i in stds) {
            if (stds.hasOwnProperty(i)) {
                studentIds.push(stds[i]._id);
                for (var index in stds[i].assessmentdata){
                    assessmentIds.push(index);
                }
            }
        }

        var performanceTasks = PerformanceTasks.find({_id:{$in:assessmentIds}});


        var media = Media.find({
            students: {
                $in: studentIds
            }
        });

        /** Get media data **/
        var med = media.fetch(),
            mediaDataArray = [];

        for (var i in med) {
            if (med.hasOwnProperty(i)) {
                if (med[i].type == "image" && med[i].file) {
                    for (var j in med[i].file) {
                        if (med[i].file.hasOwnProperty(j)) {
                            mediaDataArray.push(med[i].file[j]._id);
                        }
                    }
                }
            }
        }

        var mediaData = MediaData.find({
            _id: {
                $in: mediaDataArray
            }
        });

        return [
            students,
            media,
            mediaData,
            performanceTasks
        ];
    }
});


Meteor.publishComposite('reactiveUsersStudents', {
    find: function () {
        var user = Meteor.users.findOne({ _id: this.userId }),
            userGroupIds = [],
            userGroupStudentIds = [],
            userSharedGroupIds = [];

        // Get list of user's shared group ids
        if (user.profile.sharedgroups) {
            userSharedGroupIds = user.profile.sharedgroups.map(function(e) { return e._id; });
        }

        // Get list of user group and group student ids
        if (user.profile.groups) {
            userGroupIds = user.profile.groups.map(function(e) { return e._id; });

            for (var i = 0; i < user.profile.groups.length; i++) {
                userGroupStudentIds.push.apply(userGroupStudentIds, user.profile.groups[i].students);
            }
        }
        console.log('being called')
        // return users
        return Meteor.users.find({
            $or: [
                {_id: this.userId},
                {'profile.groups': { $elemMatch: { _id: {$in: userSharedGroupIds} } }},
                {'profile.sharedgroups': { $elemMatch: { _id: {$in: userGroupIds} } }}
            ]
        }, { fields: {profile: 1} });
    },
    children: [
        {
            find: function(user) {
                var userClassroomIds = [],
                    userSharedClassroomIds = [],
                    userGroupStudentIds = [],
                    userSharedGroupIds = [],
                    userSharedGroupStudentIds = [],
                    index = 0,
                    i = 0,
                    pindex = 0,
                    p = 0,
                    theUser = Meteor.users.findOne({_id:this.userId});
                // Get list of user's classroom ids

                for (index; index < theUser.profile.classrooms.length; index++) {
                    if (user.profile.sharedclassrooms) {
                        for (i; i < user.profile.sharedclassrooms.length; i++) {
                            if (theUser.profile.classrooms[index]._id === user.profile.sharedclassrooms[i]._id) {
                                userSharedClassroomIds.push(theUser.profile.classrooms[index]._id);
                            }
                        }
                    }
                    userClassroomIds.push(theUser.profile.classrooms[index]._id);
                }
                for (pindex; pindex < theUser.profile.groups.length; pindex++) {
                    if (user.profile.sharedgroups) {
                        for (p; p < user.profile.sharedgroups.length; p++) {
                            if (theUser.profile.groups[pindex]._id === user.profile.sharedgroups[p]._id) {
                                userSharedGroupIds = theUser.profile.groups[pindex].students.map(function (studentId) {
                                    return studentId
                                });
                            }
                        }
                    }
                    userGroupStudentIds = theUser.profile.groups[pindex].students.map(function(studentId){return studentId});
                }

                //if (user.profile.classrooms) {
                //    userClassroomIds = user.profile.classrooms.map(function(e) { return e._id; });
                //}
                //
                //// Get list of user's shared group ids
                //if (user.profile.sharedgroups) {
                //    userSharedGroupIds = user.profile.sharedgroups.map(function(e) { return e._id; });
                //}
                //
                //// Get list of user group and group student ids
                //if (user.profile.groups) {
                //    for (var i = 0; i < user.profile.groups.length; i++) {
                //        userGroupStudentIds.push.apply(userGroupStudentIds, user.profile.groups[i].students);
                //    }
                //}
                //
                //// If there are shared groups we need to find the user that the group belongs to
                //// and get the list of students for each of the shared groups
                //if (user.profile.sharedgroups) {
                //    var usersArray = users.fetch();
                //
                //    for (var i = 0; i < userSharedGroupIds.length; i++) {
                //        var currentId = userSharedGroupIds[i];
                //
                //        for (var j = 0; j < usersArray.length; j++) {
                //            if (usersArray[j].profile.groups) {
                //                for (var k = 0; k < usersArray[j].profile.groups.length; k++) {
                //                    if (usersArray[j].profile.groups[k]._id === currentId) {
                //                        userSharedGroupStudentIds.push.apply(userSharedGroupStudentIds, usersArray[j].profile.groups[k].students);
                //
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}

                // return students
                return Students.find({
                    $or: [
                        {classroom_id: {$in: userClassroomIds}},
                        {classroom_id: {$in: userSharedClassroomIds}},
                        {_id: {$in: userGroupStudentIds}},
                        {_id: {$in: userSharedGroupStudentIds}}
                    ]
                });
            }
        }
    ]
});

// Get the logged in user data for the current user and any users
// the logged in user shares with
Meteor.publish('usersStudents', function () {
    if (this.userId) {

        var user = Meteor.users.findOne({ _id: this.userId }),
            userClassroomIds = [],
            userGroupIds = [],
            userGroupStudentIds = [],
            userSharedGroupIds = [],
            userSharedGroupStudentIds = [];

        // Get list of user's classroom ids
        if (user.profile.classrooms) {
            userClassroomIds = user.profile.classrooms.map(function(e) { return e._id; });
        }

        // Get list of user's shared group ids
        if (user.profile.sharedgroups) {
            userSharedGroupIds = user.profile.sharedgroups.map(function(e) { return e._id; });
        }

        // Get list of user group and group student ids
        if (user.profile.groups) {
            userGroupIds = user.profile.groups.map(function(e) { return e._id; });

            for (var i = 0; i < user.profile.groups.length; i++) {
                userGroupStudentIds.push.apply(userGroupStudentIds, user.profile.groups[i].students);
            }
        }

        var users = Meteor.users.find({
            $or: [
                {_id: this.userId},
                {'profile.groups': { $elemMatch: { _id: {$in: userSharedGroupIds} } }},
                {'profile.sharedgroups': { $elemMatch: { _id: {$in: userGroupIds} } }}
            ]
        }, { fields: {profile: 1} });

        // If there are shared groups we need to find the user that the group belongs to
        // and get the list of students for each of the shared groups
        if (user.profile.sharedgroups) {
            var usersArray = users.fetch();

            for (var i = 0; i < userSharedGroupIds.length; i++) {
                var currentId = userSharedGroupIds[i];

                for (var j = 0; j < usersArray.length; j++) {
                    if (usersArray[j].profile.groups) {
                        for (var k = 0; k < usersArray[j].profile.groups.length; k++) {
                            if (usersArray[j].profile.groups[k]._id === currentId) {
                                userSharedGroupStudentIds.push.apply(userSharedGroupStudentIds, usersArray[j].profile.groups[k].students);

                                break;
                            }
                        }
                    }
                }
            }
        }

        var students = Students.find({
            $or: [
                {classroom_id: {$in: userClassroomIds}},
                {_id: {$in: userGroupStudentIds}},
                {_id: {$in: userSharedGroupStudentIds}}
            ]
        });

        return [
            users,
            students
        ];
    }
});

// Publish performance tasks by grade selected
Meteor.publish('performanceTasksByGrade', function (grade) {
    if (this.userId && grade != null) {
        check(grade, String);
        return PerformanceTasks.find({grade: grade});
    }
});

Meteor.publish('performanceTasksByGradeNames', function (grade) {
    if (this.userId && grade != null) {
        check(grade, String);
        return PerformanceTasks.find({grade: grade}, {name:true, subject:true, commonCoreStateStandards: true});
    }
});

// Publish all performance tasks
Meteor.publish('performanceTasks', function () {
    if (this.userId) {
        return PerformanceTasks.find();
    }
});



// Publish common core standards by grade selected
Meteor.publish('commonCoreStandardsByGrade', function (grade) {
    if (this.userId && grade != null) {
        check(grade, String);

        return CommonCoreStateStandards.find({grade: grade});
    }
});

// Publish all common core standards
Meteor.publish('commonCoreStandards', function () {
    if (this.userId) {
        return CommonCoreStateStandards.find();
    }
});




// Publish assessments by grade
Meteor.publish('assessmentsByGrade', function (grade) {
    if (this.userId) {
        return Assessments.find({grade: grade});
    }
});

// Publish all assessments
Meteor.publish('assessments', function () {
    if (this.userId) {
        return Assessments.find();
    }
});



// Publish all glossary terms
Meteor.publish('glossary', function () {
    if (this.userId) {
        return Glossary.find();
    }
});

// Only publish the current user
Meteor.publish("currentUserExtendedData", function () {
    if (this.userId) {
        return Meteor.users.find({_id: this.userId}, { fields: {emails: 1, profile: 1, services: 1} });
    }
});
