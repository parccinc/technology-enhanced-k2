/**
 * Created by sgi-dev-27 on 7/31/15.
 */

var adminUsernames = ["teacherk@yahoo.com",
    "teacherk@k.com",
    "teacherg1@yahoo.com",
    "teacherg2@yahoo.com",
    "edccm@yahoo.com",
    "centercm@yahoo.com",
    "parcccm@yahoo.com",
    "k2contentadmin1@yahoo.com",
    "k2contentadmin2@yahoo.com"];


isAdmin = function () {
    return (adminUsernames.indexOf(Meteor.user().emails[0].address) != -1 || Roles.userIsInRole(Meteor.user(), "Content Administrator (Curator)"))
};
