if (!Accounts.saml) {
    Accounts.saml = {};
}

var Fiber = Meteor.npmRequire('fibers');
var connect = Meteor.npmRequire('connect');
//RoutePolicy.declare('/_saml/', 'network');

Accounts.registerLoginHandler(function(loginRequest) {
    if(!loginRequest.saml || !loginRequest.credentialToken) {
        return undefined;
    }
    var loginResult = Accounts.saml.retrieveCredential(loginRequest.credentialToken);

    if(loginResult && loginResult.profile && loginResult.profile.staffid){
        var array_roles = loginResult.profile.roles.split(','),
            user = Meteor.users.findOne({'profile.saml.staffId': loginResult.profile.staffid});

        if (!isUserInValidRole(array_roles)) {
            return {
                error: new Meteor.Error(403, "Invalid_User")
            };
        } else {

            // If the user does not exist yet in the db then add it
            if (!user) {
                var options = {
                    email: loginResult.profile.mail,
                    profile: {
                        firstName: loginResult.profile.firstname,
                        lastName: loginResult.profile.lastname,
                        saml : {
                            staffId: loginResult.profile.staffid
                        }
                    }
                },
                userId = Accounts.createUser(options);

                // Add user role
                Roles.addUsersToRoles(userId, [loginResult.profile.roles]);

                user = Meteor.users.findOne({_id: userId});
            }

            if (!user) {
                throw new Error("Could not find an existing user with supplied staff id " + loginResult.profile.staffid);
            } else {
                // Check to see if role was modified
                if (!user.roles) {
                    Roles.addUsersToRoles(user._id, array_roles);
                } else if (!Roles.userIsInRole(user._id, array_roles)) {
                    Roles.removeUsersFromRoles(user._id, [user.roles]);
                    Roles.setUserRoles(user._id, array_roles);
                }
            }

            //creating the token and adding to the user
            var stampedToken = Accounts._generateStampedLoginToken();
            var hashStampedToken = Accounts._hashStampedToken(stampedToken);

            Meteor.users.update({_id: user._id},
                { $push: {'services.resume.loginTokens': hashStampedToken},
                  $set: {'samlSessionId': loginResult.profile.sessionIndex,
                         'samlNameId': loginResult.profile.nameID,
                         'profile.firstName': loginResult.profile.firstname,
                         'profile.lastName': loginResult.profile.lastname,
                         'emails.0.address': loginResult.profile.mail
                        }
                }
            );

            //sending token along with the userId
            return {
                userId: user._id,
                token: stampedToken.token
            };
        }

    }else{
        throw new Error("SAML Profile did not contain a staff id");
    }
});

Accounts.saml._loginResultForCredentialToken = {};

Accounts.saml.hasCredential = function(credentialToken) {
    return _.has(Accounts.saml._loginResultForCredentialToken, credentialToken);
}

Accounts.saml.retrieveCredential = function(credentialToken) {
    var result = Accounts.saml._loginResultForCredentialToken[credentialToken];
    delete Accounts.saml._loginResultForCredentialToken[credentialToken];
    return result;
}

// Listen to incoming OAuth http requests
WebApp.connectHandlers.use(connect.bodyParser()).use(function(req, res, next) {
    // Need to create a Fiber since we're using synchronous http calls and nothing
    // else is wrapping this in a fiber automatically
    Fiber(function () {
        middleware(req, res, next);
    }).run();
});

middleware = function (req, res, next) {
    // Make sure to catch any exceptions because otherwise we'd crash
    // the runner
    console.log(req.url);

    try {
        var samlObject = samlUrlToObject(req.url);
        if(!samlObject || !samlObject.serviceName){
            next();
            return;
        }

        if(!samlObject.actionName)
            throw new Error("Missing SAML action");

        var service = _.find([Meteor.settings.public.saml], function(samlSetting){
            return samlSetting.provider === samlObject.serviceName;
        });

        // Skip everything if there's no service set by the saml middleware
        if (!service)
            throw new Error("Unexpected SAML service " + samlObject.serviceName + " " + Meteor.settings.public.saml.provider);

        if(samlObject.actionName === "authorize"){
            service.callbackUrl = Meteor.absoluteUrl("_saml/validate/"+service.provider+"/");
            service.id = samlObject.credentialToken;

            _saml = new SAML(service);
            _saml.getAuthorizeUrl(req, function (err, url) {
                if(err)
                    throw new Error("Unable to generate authorize url");
                res.writeHead(302, {'Location': url});
                res.end();
            });
        }else if (samlObject.actionName === "validate"){
            _saml = new SAML(service);
            if (req.body.SAMLResponse) {
                _saml.validateResponse(req.body.SAMLResponse, 'body', function (err, profile, loggedOut) {

                    if(err)
                        throw new Error("Unable to validate response url");
                    if (!loggedOut) {
                        var credentialToken = profile.inResponseToId || profile.InResponseTo || service.id;

                        if(!credentialToken)
                            throw new Error("Unable to determine credentialToken");

                        Accounts.saml._loginResultForCredentialToken[credentialToken] = {
                            profile: profile
                        };

                    }

                    closePopup(res);
                });
            } else if (req.query.SAMLResponse) {
                    _saml.validateResponse(req.query.SAMLResponse, 'query', function (err, profile, loggedOut) {
                        if(err)
                            throw new Error("Unable to validate response url");

                        if (!loggedOut) {
                            var credentialToken = profile.inResponseToId || profile.InResponseTo || service.id;

                            if(!credentialToken)
                                throw new Error("Unable to determine credentialToken");

                            Accounts.saml._loginResultForCredentialToken[credentialToken] = {
                                profile: profile
                            };
                        }

                       closePopup(res);
                    });
            }
        } else if(samlObject.actionName === "logout"){
            _saml = new SAML(service);

            var currentUser = Meteor.users.findOne({"_id": samlObject.credentialToken});

            if (currentUser) {
                req.sessionIndex = currentUser.samlSessionId;
                req.nameId = currentUser.samlNameId;

                _saml.getLogoutUrl(req, function (err, url) {
                    if(err)
                        throw new Error("Unable to logout");
                    res.writeHead(302, {'Location': url});
                    res.end();
                });

            } else {
                throw new Error("User not logged in.");
            }
        }else {
            throw new Error("Unexpected SAML action " + samlObject.actionName);
        }
    } catch (err) {
        closePopup(res, err);
    }
};

var samlUrlToObject = function (url) {
    // req.url will be "/_saml/<action>/<service name>/<credentialToken>"
    if(!url)
        return null;

    var splitPath = url.split('/');

    // Any non-saml request will continue down the default
    // middlewares.
    if (splitPath[1] !== '_saml')
        return null;

    return {
        actionName:splitPath[2],
        serviceName:splitPath[3],
        credentialToken:splitPath[4]
    };
};

var closePopup = function(res, err) {
    res.writeHead(200, {'Content-Type': 'text/html'});

    var content =
        '<html><head><script>window.close()</script></head></html>';

    if(err)
        content = '<html><body><h2>Sorry, an error occurred</h2><div>'+err+'</div><a onclick="window.close();">Close Window</a></body></html>';

    res.end(content, 'utf-8');
};

var isUserInValidRole = function (roles) {
    if (roles.indexOf("Content Administrator (Curator)") !== -1 || roles.indexOf("PARCC-Member Educator") !== -1) {
        return true;
    }

    return false;
};