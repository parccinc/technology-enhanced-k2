// if the database is empty on server start, create some sample assessment data.

/*
_id: ObjectId
grade: String (K, 1, or 2)
subject: String (ela or math)
type: String (rubric or checklist)
name: String (RI1 Comprehension Rubric)
standard: String (RI1)
assessValues: Array of assessment possible values {_id, value, description}
*/

Meteor.startup(function () {
    if (Assessments.find().count() === 0) {

        var data = [
            {_id: "RL1",
                grade: "K",
                subject: "ela",
                type: "rubric",
                name: "RL1 Comprehension Rubric",
                standard: "RL.K.1",
                assessValues: [
                    {
                        _id: "1",
                        value: "Meeting",
                        description: "The student response demonstrates a full and accurate comprehension of the ideas and details expressed in the text(s)."
                    },
                    {
                        _id: "2",
                        value: "Approaching",
                        description: "The student response demonstrates accurate comprehension of the ideas and details expressed in the text(s)."
                    },
                    {
                        _id: "3",
                        value: "Developing",
                        description: "The student response demonstrates limited comprehension of the ideas and details expressed in the text(s). "
                    },
                    {
                        _id: "4",
                        value: "Emerging",
                        description: "The student response does not demonstrate comprehension of the ideas and details expressed in the text(s)."
                    }
                ]}
        ];

        for (var i = 0; i < data.length; i++) {
            Assessments.insert(data[i]);
        }
    }
});