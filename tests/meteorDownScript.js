/*Meteor.startup(function () {
    if (Meteor.users.find().count() === 0) {
        var i;
        for (i = 0; i < 100; i++) {
            Accounts.createUser({
                username: i,
                email: String(i) + "@test.com",
                password: 'Password1'
            });
        }
    }
});*/

/*Meteor.methods({
    mdAdd: function (x, y) {
        return x + y;
    },

    mdGetUser: function () {
        return Meteor.user();
    },

    mdGetSessionVar: function () {
        Session.get('selected_group');
    },

    mdGetPerformanceTask: function () {
        PerformanceTasks.find({"grade": selected_grade, "subject": selected_subject});
    },

    mdTimeout: function () {
        setTimeout(function(){ return true }, 3000);
    }
});*/

meteorDown.init(function (Meteor) {
    //console.log(Meteor.userId());
    var returnedSubscriptions = 0;
    // Subscribe to the media files data
    Meteor.subscribe("mediaData", function () {
        done();
    });

    // Subscribe to the media
    Meteor.subscribe("media", function () {
        done();
    });

    // Subscribe to the students
    Meteor.subscribe("classroomStudents", function () {
        done();
    });

    // Subscribe to the performance tasks
    Meteor.subscribe("performanceTasks", function () {
        done();
    });

    // Subscribe to the assessments
    Meteor.subscribe("assessments", function () {
        done();
    });

    // Subscribe to the common core standards
    Meteor.subscribe("commonCoreStandards", function () {
        done();
    });

    // Subscribe to all users so classrooms can be shared.
    Meteor.subscribe('allUsers', function () {
        done();
    });

    // Subscribe to all students.
    Meteor.subscribe('allStudents', function () {
        done();
    });

    // Subscribe to the glossary terms
    Meteor.subscribe("glossary", function () {
        done();
    });

    function done() {
        returnedSubscriptions++;
        if (returnedSubscriptions === 9) {
            //console.log('subscriptions ready for: ' + Meteor.userId());
            Meteor.kill();
        }
    }

});

meteorDown.run({
    concurrency: 200,
    url: "http://192.168.2.111:3000",
    key: 'TEST',
    auth: {userIds: ['weaE54GsExZpXJ5cT', 'gnjMHzwbriNiLrXmr']}
});

//auth: {userIds: ['weaE54GsExZpXJ5cT']}
