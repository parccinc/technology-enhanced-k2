getSubs = function (Meteor, callback) {
    //console.log(Meteor.userId());
    var returnedSubscriptions = 0;
    // Subscribe to the media files data
    Meteor.subscribe("mediaData", function () {
        done();
    });

    // Subscribe to the media
    Meteor.subscribe("media", function () {
        done();
    });

    // Subscribe to the students
    Meteor.subscribe("classroomStudents", function () {
        done();
    });

    // Subscribe to the performance tasks
    Meteor.subscribe("performanceTasks", function () {
        done();
    });

    // Subscribe to the assessments
    Meteor.subscribe("assessments", function () {
        done();
    });

    // Subscribe to the common core standards
    Meteor.subscribe("commonCoreStandards", function () {
        done();
    });

    // Subscribe to all users so classrooms can be shared.
    Meteor.subscribe('allUsers', function () {
        done();
    });

    // Subscribe to all students.
    Meteor.subscribe('allStudents', function () {
        done();
    });

    // Subscribe to the glossary terms
    Meteor.subscribe("glossary", function () {
        done();
    });

    function done() {
        returnedSubscriptions++;
        if (returnedSubscriptions === 9) {
            callback();
        }
    }
};

changeName = function (Meteor, callback) {
    Meteor.call("updateFirstName", Meteor.user(), "test first name", function (result) {
        if(result){
            callback();
        }
    });
};

annotate = function (Meteor, callback) {
    var annotation = {"_id":"4ed6a14e7bbd4b50cc7da85b","performance_task_id":"xnCuxknn4K7BhP5Dd","annotation_date":"2015-05-20T20:17:24.557Z","annotation":"dfghjkl;';l","instruction":"plan"};

    Meteor.call('addUserAnnotationEntry', annotation, function (res) {
        var annotation = res[1];
        if (annotation) {
            callback();
        } else {
            console.log(res);
            callback();
        }
    });
};

getPerformanceTask = function (Meteor, callback) {
    Meteor.call('getPerformanceTask', "xnCuxknn4K7BhP5Dd", function(res) {
        var task = res[1];
        if (task !== null && task.assessmentSchedule.length > 0) {
            callback();
        } else {
            console.log(res);
            callback();
        }
    });
};

executeRandom = function (Meteor, callback) {
    setTimeout(function () {
        var random = Math.floor(Math.random() * 100);
        if (random < 80) {
            getPerformanceTask(Meteor, function () {
                //console.log('select');
                callback();
            });
        } else if (random >= 80 && random < 85) {
            changeName(Meteor, function () {
                //console.log('change name');
                callback();
            })
        } else if (random >= 85 && random < 99) {
            annotate(Meteor, function () {
                //console.log('annotate');
                callback();
            });
        } else {
            getSubs(Meteor, function () {
                //console.log('get subs');
                callback();
            });
        }
    }, getTimeout())
};

getTimeout = function () {
    //return Math.floor((Math.random() * 5000) + 2000);   // wait between 2 and 5 seconds
    return Math.floor((Math.random() * 15000) + 5000);   // wait between 5 and 15 seconds
    //return Math.floor((Math.random() * 40000) + 20000);   // wait between 20 and 40 seconds
};

meteorDown.init(function (Meteor) {
    executeRandom(Meteor, function () { // call a random function
        executeRandom(Meteor, function () { // call a random function
            executeRandom(Meteor, function () { // call a random function
                Meteor.kill();                  // disconnect
            });
        });
    });
});

meteorDown.run({
    concurrency: 1300,
    url: "http://192.168.2.111:3000",
    key: 'TEST',
    auth: {userIds: ['weaE54GsExZpXJ5cT', 'gnjMHzwbriNiLrXmr', 'smLJX9aP4PgxJtP4c', 'oYPmRunYPfmApswP3', 'kBybmKH3ogNNr72iP', 'k76QGGYoG6zvYXy3M']}
});
