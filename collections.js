// Mongo collections used by the application

Assessments = new Meteor.Collection("assessments");
CommonCoreStateStandards = new Meteor.Collection("commoncorestatestandards");
Media = new Meteor.Collection('media');
PerformanceTasks = new Meteor.Collection("performancetasks");
Students = new Meteor.Collection("students");
Glossary = new Meteor.Collection("glossary");

var mediaStore = new FS.Store.FileSystem('mediaData', {
    path: '~/uploads/media',
    transformWrite: function(fileObj, readStream, writeStream) {
        // Transform the image into a 10x10px thumbnail
        gm(readStream, fileObj.name()).resize('500', '375').stream().pipe(writeStream);
    }
});

MediaData = new FS.Collection('mediaData', {
    stores: [mediaStore]
});

MediaData.allow({
    insert: function () {
        return true;
    },
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    },
    download: function () {
        return true;
    }
});